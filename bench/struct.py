from bench import bench

print(bench(10, 'class Foo(): pass', '''
for _ in range(100000):
  foo = Foo()
  foo.bar = 40
  foo.baz = foo.bar + 1
  foo.qux = foo.baz + 1
'''))
