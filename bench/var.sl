10 bench: (
  100000 times: {
    42 let: 'foo
    @foo let: 'bar
    @bar let: 'baz
  }
) say