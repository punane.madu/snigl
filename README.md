<script src="https://liberapay.com/sifoo/widgets/button.js"></script>

> The majority believes that everything hard to comprehend must be very profound.<br/>
> This is incorrect. What is hard to understand is what is immature, unclear and often false.<br/>
> The highest wisdom is simple and passes through the brain directly into the heart.
>
> ~ Viktor Shauberger

### [sni`gel]
Snigl is a dynamic language aiming for simplicity with a functional touch.<br/>
While usable on its own, the primary use case is to run embedded in `C`.

```
  "abc" &(int ++ char) map str

["bcd"]
```

### Core Features

#### Flexibility
Snigl goes to great lengths to support mixing and matching exactly the features needed without dragging additional complexity along. This idea runs like a thread through the language, but to name an example: all scopes are explicit; meaning that unless a stack and/or variable scope is explicitly asked for, all code runs in the same dynamic scope within a task.

#### Integration
Snigl is designed as a complement to `C` rather than a replacement for anything. The combination allows simultaneously playing the strengths of both languages to nail the right compromise between performance, flexibility and convenience.

#### Gradual Types
While most dynamic languages don't really offer much of a type system, Snigl follows the trail blazed by `Common Lisp` and allows code to be exactly as specific as it feels like regarding types.

#### Multiple Dispatch
Snigl uses multiple dispatch with support for pattern matching for all (including built-in) named functions.

#### Cooperative Multitasking
Snigl is designed from the ground up to support fast and convenient cooperative multitasking.

#### Opt Out Async
When multitasking, any operation that might block is deferred to a background thread by default while the current task yields until a result is available.

#### Native Executables
Snigl supports emitting `C` code for an interpreter initialized with the result of compiling and tracing a script. The resulting file may be embedded in separate applications or compiled standalone with a `main` stub.

### Status
Snigl currently weighs in at 11 kloc including standard library; and depends on `CMake`, a `C11` compiler and `pthreads`. It often runs faster, sometimes over 20x, than `Python3`; as far as the current [suite](https://gitlab.com/sifoo/snigl/tree/master/bench) of benchmarks go. The type hierarchy and standard library are works in progress, but extending from `C` is trivial. Code from this document, [tests](https://gitlab.com/sifoo/snigl/blob/master/test/all.sl) and benchmarks should run clean in `Valgrind`.

### Bootstrap
Snigl may be compiled from source by issuing the following commands.

```
$ git clone https://gitlab.com/sifoo/snigl.git
$ cd snigl
$ mkdir build
$ cd build
$ cmake ..
$ sudo make install
$ snigl ../test/all.sl
.............
$ rlwrap snigl

Snigl v0.8.5

Press Return in empty row to eval.

  1 2 +

[3]
```

### Evaluation
Snigl evaluates forms left to right, pushing arguments on the stack and calling functions and macros in order of appearance. The simplicity affords useful features, like the ability to manipulate the stack.

```
  1 2 3 swap

[1 3 2]

  dup
  
[1 3 2 2]

  drop
  
[1 3 2]

  rotl
  
[2 1 3]

  rotr
  
[1 3 2]

  swap
  
[1 2 3]
```

### Expressions
Expressions consist of forms; literals, including types; function and macros, which is anything that's not a type; and variables, like `@foo`. Sub expressions may be created by enclosing forms in `()` or prefixing with `,`.

```
  [3 times: 'a 'b] str

["aaab"]
```
```
  [3 times: ('a 'b)] str

["ababab"]
```
```
  [3 times:, 'a 'b] str

["ababab"]
```

### Equality & Identity
`=` may be used to check if two values are equal, while `==` makes sure they are the same value; the difference only applies to references. Both comparisons may be negated by prefixing with `!`.

```
  [1 2 3] [1 2 3] =

[t]
```
```
  [1 2 3] [1 2 3] !==

[t]
```
```
  42 42 ==

[t]
```

### Functions
Named functions support multiple/value dispatch, arguments may be specified as either types or literals.

```
  func: is-42? (42) t
  func: is-42? (Int) f
  1 is-42?
  42 is-42?

[f t]
```

Anonymous functions may be created by prefixing sexprs and scopes with `&`.

```
  &(42)

[(Lambda 1 0)]

  call
  
[42]
```
Scoped lambdas capture their defining environment.

```
  {42 let: 'foo &{@foo}}

[(Lambda 1 13)]

  call
  
[42]
```

Named functions may be referenced the same way.

```
  1 2 &+
  
[1 2 (Func +)]

  call

[3]
```

Functions may be called tail-recursively using `recall` and `recall:`; the first variant jumps to the start of the currently executing function, the second delegates to the specified function.

```
  func: fib-tail (0 Int Int) (rotr drop2)
  func: fib-tail (1 Int Int) (rotl drop2)
  func: fib-tail (Int Int Int) (rotr -- rotl dup rotl + recall: fib-tail)
  20 fib-tail

[6765]
```

Function argument lists are fully evaluated at compile time.

```
  func: is-3? (1 2 +) t
  func: is-3? (3 type) f
  1 is-3?
  3 is-3?

[f t]
```

### Macros
Macros transform expressions into sequences of VM operations at compile time.

```
  42 let: ('compiling say 'foo)

compiling
[]

  @foo

[42]
```

Macros are first class, and may be referenced like functions.

```
  &dup
  
[(Macro 1 0)]
```

Compile time arguments are captured at the reference site.

```
  &let: 'bar
  42 swap call
  @bar
  
[42]
```

### Variables
Names may be bound once per scope using `let:`. Using variables, the final case for `fib-tail` could be rewritten as follows.

```
func: fib-tail (Int Int Int) {
  let: ('n 'a 'b)
  @n -- @b dup @a + recall: fib-tail
}
```

Pairs may be used to inline values at compile time.

```
  2 let: ('foo :: 1 'bar 'baz :: 3)
  @foo @bar @baz

[1 2 3]
```

### Numbers
Snigl supports signed 64-bit integers and decimal fixed point numbers. Fixed point literals use the number of decimals to determine scale.

Fixed points are automatically down-scaled when added/subtracted.

```
.25 .5 +

[.7]
```
```
.25 .50 -

[-.25]
```

And re-scaled using the operation when multiplied/divided.

```
  1.00 4.0 /
[.2]
```

```
  .20 4.0 *
[.800]
```

### Characters
Character literals are prefixed with `#`.

```
  #a type

[Char]
```
While special characters use `\`.

```
  \n int

[10]
```

### Strings
Strings are mutable, reference counted, fixed capacity sequences of characters.

```
  ["foo" 42 'bar] #, join

["foo,42,bar"]

  dup pop! dup last
  
["foo,42,ba" #a]

  drop len

[9]
```

String literals support interpolation of arbitrary expressions using `%`. Each expression pops the last value from the stack after evaluation.

```
  'bar
  42 let: 'baz
  "foo %_ %(@baz)"

["foo bar 42"]
```

###  Symbols
Symbols are immutable singleton strings with support for fast equality, they may be created by prefixing any identifier with `'`.

```
  'abc type

[Sym]
```

```
  'abc 'abc ==

[t]
```

### Sequences
Sequences are values that may be iterated; integers (0..N-1), pairs, strings and lists are all sequences.

#### Lists
Lists may be created by enclosing forms in brackets.

```
  [42 \n]
  
[[42 \n]]

  dup 'foo push
  
[[42 \n 'foo]]

  pop
  
['foo']
```

Lists are reference counted; `dup` copies the reference, not the value.

```
  [1 2 3] dup pop!

[[1 2]]
```

A deep copy may be obtained by calling `clone`.

```
  [1 2 3] clone pop!

[[1 2 3]]
```

The `list` constructor allows creating a new list from any sequence.

```
  [1 2 3] dup list pop!

[[1 2 3]]
```

```
  7 list

[[0 1 2 3 4 5 6]]
```

Lists support deep comparisons.

```
  [1 2 2] [1 2 3] <

[t]
```

Expressions may be evaluated in the containing stack by prefixing with `%`. Each expression pops the last value from the stack after evaluation.

```
  41 ['foo %(++) 'bar]

[['foo 42 'bar]]
```
```
  42 ['foo [%_ 'bar %%_].. 'baz]
  
[['foo 'bar 42 'baz]]
```

#### Iterators
`iter` may be called on any sequence to get a new iterator. `next` pushes the next value, or nil; while `next!` discards it and `skip` discards the specified number of values.

```
  3 iter

[(IntIter 0x3100c38)]

  dup next

[(IntIter 0x3100c38) 0]

  swap dup next! dup next swap

[0 2 (IntIter 0x3100c38)]

  next

[0 2 nil]
```
```
  "foobar" iter dup 3 skip
  
[(StrIter 0x1666318)]

  next
  
['b]
```

List iterators additionally support inserting and deleting values at the current position.

```
  [1 2 3] dup iter

[[1 2 3] (ListIter 0x2100b38)]

  dup next! dup next! dup delete!

[[1 3] (ListIter 0x2100b38)]

  42 insert

[1 42 3]
```

Iterators may be further processed using `map` and `filter`, both return new iterators.

```
  "abcabcabc" &(int ++ char) map str

["bcdbcdbcd"]
```
```
  "abcabcabc" &('b !=) filter str

["acacac"]
```

`map` skips `nil` results, which allows filtering while mapping. With that in mind, previous examples could be combined as follows:

```
  "abcabcabc" &(int ++ char dup #b = if: (drop nil) _) map str

["cdcdcd"]
```

Since list literals are fully evaluated, putting a `for:`-loop with the condition inside accomplishes the mostly same thing slightly faster, in return for being less composable and requiring additional memory to store the list.

```
  ["abcabcabc" for:, int ++ char dup #b = if: drop _] str
````

`for:` may be used to consume, or reduce, any iterable value.

```
  7 list

[[0 1 2 3 4 5 6]]

  0 swap for: +

[21]
```

### Pairs
Pairs may be formed using `::`; queried using `first` and `last`; and split up using `..`.

```
  'foo :: 42

['foo::42]

  dup last 42 =

['foo::42 t]

  drop ..

['foo 42]
```

Pairs support deep comparisons.

```
  42 :: 'a 42 :: 'b <

[t]
```

### Loops
Besides recursion and iterators; `times:` may be used to iterate an expression a fixed number of, well, times.

```
  0 3 times: ++

[3]
```

And `while:` to repeat an expression until it returns false.

```
  0 while: (++ dup 42 <)

[42]
```

### Types
Typenames start with uppercase letters. All types, including `Nil`; are derived from the root type, `A`. All types except `Nil` are additionally derived from `T`.

```
  42 T type-sub?

[t]
```
```
  42 Nil type-sub?
  
[f]
```
```
  nil Nil type-sub?  
[t]
```

Types may be combined using `|`.

```
  42 Int|Char type-sub?

[t]
```
```
  Int|Fix Num sub?

[t]
````

`type:` creates a new type derived from specified target, the expression is fully evaluated at compile time.

```
  type: IntList List
  func: sum (IntList) (0 swap for: +)
  [1 2 3] sum

Error in row 1, col 8: Fimp not applicable: sum (IntList)

  [1 2 3] int-list sum

[6]
```
```
  IntList List sub?
  List IntList sub?

[t f]
```

#### Conversions
Where applicable, values may be converted by calling a constructor named after the target type.

```
  "foo" list
  
[['f 'o 'o]]

  dup "bar" push

[['f 'o 'o "bar"]]

  str
["foobar"]
```

#### Structures
Structures contain a fixed number of named and typed fields, any number of parent types may be specified as long as field names are unique.

```
  struct: Peer _ ('name Str)
  struct: Message _ ('from Peer 'body Str)
  struct: EMail Message ('subject Str)
```

`new` may be used to create new instances. `put:` works much like `let:`, besides taking a receiver as stack argument.

```
  Peer new let: 'p
  @p put: ('name :: "Me")
  @p

[(Peer "Me")]
```

Fields may be referenced using symbols, which are used to look up fields; or using optionally typed field specifiers that know enough to index the instance.

```
  @p .name
  
["Me"]

  @p .Peer/name
  
["Me"]

```

Uninitialized fields return `nil`.

```
  EMail new let: 'm
  m .from
  
[nil]
```

`put:` accepts any kind of field reference.

```
  @p "Test" "Body..."
  @m put: ('.EMail/from '.subject 'body)
  @m

[(EMail (Peer "Me") "Test" "Body...")]
```

Instances default to comparing fields.

```
  Peer new dup put: ('.name :: "foo")
  let: 'p1
  Peer new dup put: ('.name :: "foo")
  let: 'p2  
  @p1 @p2 = say

[t]
```
```
  @p2 put: ('.name :: "bar")
  @p1 @p2 = say
  
[f]
```
The behavior may be customized by overriding `=`.
```
  func: = (Peer Peer) 42
  @p1 @p2 = say
  
[42]
```

Structures may be turned into sequences by deriving `Seq` and implementing `iter`

```
struct: Foobar Seq _
func: iter Foobar (drop "foobar" iter)
Foobar new #, join

["f,o,o,b,a,r"]
```

### Tasks
Snigl was designed from the ground up with fast cooperative multi-tasking in mind; as a result, [tasks](https://gitlab.com/sifoo/snigl/blob/master/bench/yield.sl) currently run at around 20x the speed of `Python3` [generators](https://gitlab.com/sifoo/snigl/blob/master/bench/yield.py). New tasks may be started using `task:`, while calling `yield` transfers control to the next. `idle:` may be used to finish all tasks, the expression is run before each round. The following example runs two cooperative tasks pushing results to a shared list and waits for both to finish.

```
[] let: 'out
task: (2 times:, @out 'bar push yield)
task: (2 times:, @out 'baz push yield)
idle: (@out 'foo push)
@out
  
[['foo 'bar 'baz 'foo 'bar 'baz 'foo 'foo]]
```

### IO
Snigl automatically switches between asynchronous and blocking IO depending on the number of tasks. In asynchronous mode, blocking calls are run in a separate thread behind the scenes while the task yields until the call returns. In tight loops like the following example, it still makes sense to ```yield``` manually for optimal concurrency. As of this writing; asynchronous file IO is around 5 times slower, which is still slightly faster than blocking file IO in Python3.

test.txt
```
foo
bar
baz
```
```
  3 for: {
    let: 'n

    task: {
      "test.txt" fopen
      [@n " fopen"] say
    
      lines for: {
        [@n " read"] say
        drop yield
      }
    }
  }

  idle: ('idle say)

idle
1 fopen
0 fopen
2 fopen
idle
2 read
0 read
1 read
idle
1 read
idle
1 read
2 read
0 read
idle
0 read
2 read
idle
```

#### Program Arguments
`argv` returns the list of arguments passed to `snigl` when starting the script.

test.sl
```
argv for: say
```
```
$ snigl test.sl foo bar baz
foo
bar
baz
```

#### Printing
`say` pretty-prints the specified value followed by newline to `stdout`; while `dump` prints the value without formatting to `stderr`. Lists may be used to output multiple values.

```
  [42 \n foo] say

42
foo
[]
```
```
  [42 \n foo] dump

[42 \n 'foo]
[]
```

### Precompilation
`pre:` may be used to evaluate code at compile time, any results are pushed each time the code is evaluated.

```
  &((pre:, 'compiling say 1 2 +) 'running say)

compiling
[(Lambda 1 0)]

  call

running
[3]
```

### Failures
When operations fail, it's often useful to unwind to a point where the failure can be dealt with properly. In those situations; `throw` may be used to unwind to the nearest `try:`, and `catch` to retrieve the thrown value. 

`try:` returns nil on success.

```
  try: _

[nil]
```

And an error containing the value otherwise.

```
  try: (42 throw)

[(Error 42)]

  catch

[42]
```

### Plugins
`link:` may be used to pull shared libraries into the runtime. A plugin is simply a shared library containing a function with the signature `bool sgl_plugin_init(struct sgl *, struct sgl_pos *)`.

```
  link: "/usr/local/lib/libsnigl_sqlite.so"
  use: sqlite ('open-db)
  "test.dat" open-db

[(DB 0x1684798)]
```

### Tracing
Snigl traces code before running to replace as many runtime checks as possible with static guarantees. Operations that are no longer needed are replaced with `nop`.

```
$ snigl compile bench/str.sl

0       nop next: 1
1       push 10 next: 2
2       bench next: 3
3       scope-beg next: 4
4       push [] next: 7
5       nop next: 7
6       nop next: 7
7       times 100000 next: 8
8       dup next: 9
9       push ["foo" "bar" 42] next: 14
10      nop next: 14
11      nop next: 14
12      nop next: 14
13      nop next: 14
14      push #, next: 15
15      fimp-call join(List T) next: 16
16      fimp-call push(List T) next: 17
17      count next: 18
18      push #; next: 19
19      dispatch join next: 20
20      drop next: 21
21      scope-end next: 22
22      count next: 23
23      dispatch say(A) next: 24
24      nop next: 24

Traced 4 rounds in 0ms

nop: 6 (25%)
fimp-call/dispatch: 2 / 2 (50%)

$ snigl bench/str.sl
610
```

Unless otherwise instructed, Snigl keeps tracing until no more improvements can be found. The number of rounds may be limited by passing `--trace N` on the command line. Disabling tracing entirely results in a 7% slow down in this case.

```
$ snigl compile --trace 0 bench/str.sl

0       nop next: 1
1       push 10 next: 2
2       bench next: 3
3       scope-beg next: 4
4       list-beg next: 5
5       list-end next: 6
6       push 100000 next: 7
7       times next: 8
8       dup next: 9
9       list-beg next: 10
10      push "foo" next: 11
11      push "bar" next: 12
12      push 42 next: 13
13      list-end next: 14
14      push #, next: 15
15      dispatch join next: 16
16      dispatch push(List T) next: 17
17      count next: 18
18      push #; next: 19
19      dispatch join next: 20
20      drop next: 21
21      scope-end next: 22
22      count next: 23
23      dispatch say(A) next: 24
24      nop next: 24

fimp-call/dispatch: 0 / 4 (0%)

$ snigl --trace 0 bench/str.sl
656
```

### Native Executables
Passing `--cemit` on the command line emits the `C` code for an interpreter initialized with the result of compiling and tracing a script. The resulting file may be embedded in separate applications or compiled standalone with a `main` stub. Native executables start faster and embed the results of compile time expressions, which simplifies distribution further. Native executables are a work in progress, it's still quite common to run into unmapped features; the good news is that the situation is rapidly improving.

test.sl
```
func: fib-rec (0) _
func: fib-rec (1) _
func: fib-rec (Int) (-- dup fib-rec swap -- fib-rec +)

20 fib-rec say
```

make_test
```
rm -f test
snigl compile --cemit test.sl > test.c
gcc test.c -lsnigl -lpthread -ldl -o test -O2 -g
```

```
$ ./make_test
$ ./test
6765
```

### Performance
Included below are fairly recent results from running the [benchmarks](https://gitlab.com/sifoo/snigl/blob/master/bench/run) on my aging HP desktop.

```
catch
473
104

fib_rec
394
253

fib_tail
531
364

fix
698
52
261
n/a

func
114
82

io
412
n/a
79
403

loop
270
46

seq
64
199
137
188

stack
613
487

str
601
598

str_vars
462
106

struct
479
381

try
130
59

var
143
44

yield
556
34
```

### Support
Evolving Snigl takes a lot of time and effort, enough to qualify as a full time project. I'm mostly scraping by and finding ways, but do consider a donation if you're interested in seeing the language reach its full potential. I'm currently running an aging crap desktop that smells like burnt electronics, getting to the point where I can replace that with a modern laptop would help a lot for example.

<a href="https://liberapay.com/sifoo/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>