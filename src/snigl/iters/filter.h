#ifndef SNIGL_ITERS_FILTER_H
#define SNIGL_ITERS_FILTER_H

#include "snigl/iter.h"

struct sgl_filter_iter {
  struct sgl_iter iter;
  struct sgl_iter *in;
  struct sgl_val *fn;
};

struct sgl_filter_iter *sgl_filter_iter_new(struct sgl *sgl,
                                            struct sgl_iter *in,
                                            struct sgl_val *fn);

#endif
