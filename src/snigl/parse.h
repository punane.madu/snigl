#ifndef SNIGL_PARSE_H
#define SNIGL_PARSE_H

#include <stdbool.h>

struct sgl;
struct sgl_ls;
struct sgl_pos;

bool sgl_parse(const char *in, struct sgl *sgl, struct sgl_ls *out);

const char *sgl_parse_form(const char *in,
                           struct sgl *sgl,
                           struct sgl_pos *pos,
                           char end_char,
                           struct sgl_ls *out);

const char *sgl_parse_end(const char *in,
                          struct sgl *sgl,
                          struct sgl_pos *pos,
                          char end_char,
                          struct sgl_ls *out);

const char *sgl_parse_char(const char *in,
                           struct sgl *sgl,
                           struct sgl_pos *pos,
                           struct sgl_ls *out);

const char *sgl_parse_special_char(const char *in,
                                   struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_ls *out);

const char *sgl_parse_id(const char *in,
                         struct sgl *sgl,
                         struct sgl_pos *pos,
                         char end_char,
                         bool quote,
                         struct sgl_ls *out);

const char *sgl_parse_inter(const char *in,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_ls *out);

const char *sgl_parse_list(const char *in,
                           struct sgl *sgl,
                           struct sgl_pos *pos,
                           struct sgl_ls *out);

const char *sgl_parse_num(const char *in,
                          struct sgl *sgl,
                          struct sgl_pos *pos,
                          bool neg,
                          struct sgl_ls *out);

const char *sgl_parse_ref(const char *in,
                          struct sgl *sgl,
                          struct sgl_pos *pos,
                          char end_char,
                          struct sgl_ls *out);

const char *sgl_parse_rest(const char *in,
                           struct sgl *sgl,
                           struct sgl_pos *pos,
                           char end_char,
                           struct sgl_ls *out);

const char *sgl_parse_scope(const char *in,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_ls *out);

const char *sgl_parse_sexpr(const char *in,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_ls *out);

const char *sgl_parse_str(const char *in,
                          struct sgl *sgl,
                          struct sgl_pos *pos,
                          struct sgl_ls *out);

#endif
