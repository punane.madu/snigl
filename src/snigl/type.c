#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "snigl/buf.h"
#include "snigl/cemit.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/meta.h"

struct sgl_def_type SGL_DEF_TYPE;

static enum sgl_cmp children_cmp(const void *lhs, const void *rhs, void *_) {
  return sgl_ptr_cmp(*(struct sgl_type **)lhs, *(struct sgl_type **)rhs);
}

struct sgl_type *sgl_type_init(struct sgl_type *t,
                               struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_lib *lib,
                               struct sgl_sym *id,
                               struct sgl_type *parents[]) {
  assert(sgl->type_tag < SGL_MAX_TYPES);
  sgl_def_init(&t->def, sgl, pos, lib, &SGL_DEF_TYPE, id);
  sgl_lib_add(lib, sgl, pos, &t->def);
  
  t->meta = NULL;
  t->is_meta = t->is_ref = t->is_trait = false;
  
  t->bool_val = NULL;
  t->call_val = NULL;
  t->cemit_val = NULL;
  t->clone_val = NULL;
  t->cmp_val = NULL;
  t->deinit_val = NULL;
  t->dump_val = NULL;
  t->dup_val = NULL;
  t->eq_val = NULL;
  t->is_val = NULL;
  t->iter_val = NULL;
  t->print_val = NULL;
  t->free = NULL;
  
  t->cemit_id = -1;
  t->tag = sgl->type_tag++;
  
  t->parent_offs =
    (ptrdiff_t)((struct sgl_type **)offsetof(struct sgl_type, parents) + t->tag);

  memset(&t->parents, 0, sizeof(t->parents));
  *(struct sgl_type **)((unsigned char *)t + t->parent_offs) = t;
  sgl_vset_init(&t->children, sizeof(struct sgl_type *), children_cmp, 0);
  struct sgl_type *pt = NULL;
  while ((pt = *parents++)) { sgl_derive(t, pt); }
  return t;
}

struct sgl_type *sgl_type_new(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_lib *lib,
                              struct sgl_sym *id,
                              struct sgl_type *parents[]) {
  return sgl_type_init(sgl_malloc(&sgl->type_pool), sgl, pos, lib, id, parents);
}

struct sgl_type *sgl_trait_new(struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_lib *lib,
                               struct sgl_sym *id,
                               struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, parents);
  t->is_trait = true;
  return t;
}

void sgl_type_free(struct sgl_type *t, struct sgl *sgl) {
  sgl_vset_deinit(&t->children);
  if (t->free) {
    t->free(t, sgl);
  } else {
    sgl_free(&sgl->type_pool, t);
  }
}

struct sgl_sym *sgl_type_id(struct sgl_type *t) { return t->def.id; }

sgl_int_t sgl_type_cemit_id(struct sgl_type *t,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_cemit *out) {
  if (t->cemit_id != -1) { return true; }
  t->cemit_id = out->type_id++;
  sgl_int_t sym_id = sgl_sym_cemit_id(sgl_type_id(t), sgl, out);
  
  sgl_cemit_line(out,
                 "struct sgl_type *type%" SGL_INT " = "
                 "sgl_get_type(sgl, sgl_pos_init(&p, %" SGL_INT ", %" SGL_INT "), "
                 "sym%" SGL_INT ");",
                 t->cemit_id, pos->row, pos->col, sym_id);

  sgl_cemit_line(out, "if (!type%" SGL_INT ") { return false; }", t->cemit_id);
  return t->cemit_id;
}

struct sgl_type *sgl_type_meta(struct sgl_type *t, struct sgl *sgl) {
  if (t->is_meta) { return t; }

  if (!t->meta) {
    char *n = sgl_sprintf("%sType", t->def.id->id);

    t->meta = sgl_meta_type_new(sgl, &t->def.pos, t->def.lib,
                                sgl_sym(sgl, n),
                                sgl_types(sgl->Meta));

    free(n);
  }
  
  return t->meta;
}

void sgl_derive(struct sgl_type *type, struct sgl_type *parent) {
  bool ok = false;
  sgl_int_t i = sgl_vset_find(&parent->children, &parent, NULL, &ok);
  if (!ok) { *(struct sgl_type **)sgl_vec_insert(&parent->children.vals, i) = type; }
  int n = 0;
  
  for (struct sgl_type **i = type->parents, **pi = parent->parents;
       n < SGL_MAX_TYPES;
       i++, pi++, n++) {
    struct sgl_type *pt = *pi;
    
    if (pt) {
      *i = pt;
      if (pt->bool_val) { type->bool_val = pt->bool_val; }
      if (pt->call_val) { type->call_val = pt->call_val; }
      if (pt->cemit_val) { type->cemit_val = pt->cemit_val; }
      if (pt->clone_val) { type->clone_val = pt->clone_val; }
      if (pt->cmp_val) { type->cmp_val = pt->cmp_val; }
      if (pt->deinit_val) { type->deinit_val = pt->deinit_val; }
      if (pt->dump_val) { type->dump_val = pt->dump_val; }
      if (pt->dup_val) { type->dup_val = pt->dup_val; }
      if (pt->eq_val) { type->eq_val = pt->eq_val; }
      if (pt->is_val) { type->is_val = pt->is_val; }
      if (pt->iter_val) { type->iter_val = pt->iter_val; }
      if (pt->print_val) { type->print_val = pt->print_val; }
    }
  }
}

bool sgl_derived(struct sgl_type *type, struct sgl_type *parent) {
  return *(struct sgl_type **)((unsigned char *)type+parent->parent_offs);
}

struct sgl_type *_sgl_type_union(struct sgl *sgl,
                                 struct sgl_pos *pos,
                                 struct sgl_type *types[]) {
  struct sgl_buf id;
  sgl_buf_init(&id);
  
  for (struct sgl_type **t = types; *t; t++) {
    if (id.len) { sgl_buf_putc(&id, '|'); }
    sgl_buf_putcs(&id, sgl_type_id(*t)->id);
  }
  
  struct sgl_sym *ids = sgl_sym(sgl, sgl_buf_cs(&id));
  sgl_buf_deinit(&id);  
  struct sgl_type *u = sgl_find_type(sgl, pos, ids);
  if (!u) { u = sgl_type_union_new(sgl, pos, sgl_lib(sgl), ids, types); }
  return u;
}

struct sgl_type *sgl_type_union_new(struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_lib *lib,
                                    struct sgl_sym *id,
                                    struct sgl_type *types[]) {
  struct sgl_type *u = sgl_type_new(sgl, pos, lib, id, sgl_types(NULL));
  sgl_int_t nparents = 0;
  
  for (struct sgl_type **ps = types[0]->parents, **p = ps;
       p < ps + SGL_MAX_TYPES;
       p++) {
    if (*p) { nparents++; }
  }

  struct sgl_type *parents[nparents];

  for (struct sgl_type **ps = types[0]->parents, **p = ps, **dst = parents;
       p < ps + SGL_MAX_TYPES;
       p++) {
    if (*p) { *dst++ = *p; }
  }
  
  for (struct sgl_type **t = types; *t; t++) {
    for (struct sgl_type **p = parents; p < parents + nparents; p++) {
      bool ok = *(struct sgl_type **)((unsigned char *)(*t)+(*p)->parent_offs);
      if (*p && !ok) { *p = NULL; }
    }
    
    sgl_derive(*t, u);
  }

  for (struct sgl_type **p = parents; p < parents + nparents; p++) {
    if (*p) { sgl_derive(u, *p); }
  }
  
  return u;
}

static void free_def(struct sgl_def *def, struct sgl *sgl) {
  sgl_type_free(sgl_baseof(struct sgl_type, def, def), sgl);
}

void sgl_setup_types() {
  sgl_def_type_init(&SGL_DEF_TYPE, "type");
  SGL_DEF_TYPE.free_def = free_def;
}
