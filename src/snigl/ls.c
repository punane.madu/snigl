#include <stddef.h>

#include "snigl/ls.h"

struct sgl_ls *sgl_ls_init(struct sgl_ls *ls) {
  ls->prev = ls->next = ls;
  return ls;
}

bool sgl_ls_null(struct sgl_ls *ls) { return ls->next == ls; }

void sgl_ls_append(struct sgl_ls *ls, struct sgl_ls *next) {
  next->prev = ls;
  next->next = ls->next;
  ls->next = next->next->prev = next;
}

void sgl_ls_insert(struct sgl_ls *ls, struct sgl_ls *prev) {
  prev->next = ls;
  prev->prev = ls->prev;
  ls->prev = prev->prev->next = prev;
}

void sgl_ls_delete(struct sgl_ls *ls) {
  ls->prev->next = ls->next;
  ls->next->prev = ls->prev;
}

struct sgl_ls *sgl_ls_peek(struct sgl_ls *root) {
  struct sgl_ls *top = root->prev;
  return (top == root) ? NULL : top;
}

struct sgl_ls *sgl_ls_pop(struct sgl_ls *root) {
  struct sgl_ls *top = root->prev;
  if (top == root) { return NULL; }
  sgl_ls_delete(top);
  return top;
}
