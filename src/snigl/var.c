#include <stddef.h>

#include "snigl/form.h"
#include "snigl/pool.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/util.h"
#include "snigl/val.h"
#include "snigl/var.h"
#include "snigl/vset.h"

enum sgl_cmp sgl_var_cmp(struct sgl_ls *lhs, const void *rhs, void *_) {
  struct sgl_var *lv = sgl_baseof(struct sgl_var, ls, lhs);
  return sgl_sym_cmp(lv->id, rhs);
}

struct sgl_var *sgl_var_init(struct sgl_var *v,
                             struct sgl_sym *id,
                             struct sgl_val *val) {
  v->id = id;
  v->val = val;
  return v;
}

struct sgl_var *sgl_var_deinit(struct sgl_var *v, struct sgl *sgl) {
  sgl_val_free(v->val, sgl);
  return v;
}

struct sgl_var *sgl_var_new(struct sgl *sgl,
                            struct sgl_sym *id,
                            struct sgl_val *val) {
  return sgl_var_init(sgl_malloc(&sgl->var_pool), id, val);
}

void sgl_var_free(struct sgl_var *v, struct sgl *sgl) {
  sgl_free(&sgl->var_pool, sgl_var_deinit(v, sgl));
}
