#ifndef SNIGL_CEMIT_H
#define SNIGL_CEMIT_H

#include "snigl/buf.h"

#define sgl_cemit_scope(c, spec, ...)                         \
  sgl_cemit_line(c, spec " {", ##__VA_ARGS__);                \
  c->indent++;                                                \
  for (bool _done = false;                                    \
       !_done;                                                \
       _done = true, c->indent--, sgl_cemit_line(c, "}"))     \
     

struct sgl_cemit {
  sgl_int_t indent, fimp_id, form_id, func_id, pc, sym_id, type_id, tab;
  struct sgl_buf buf;
};

struct sgl_cemit *sgl_cemit_init(struct sgl_cemit *c);
struct sgl_cemit *sgl_cemit_deinit(struct sgl_cemit *c);
void sgl_cemit_incl(struct sgl_cemit *c, const char *path);
void sgl_cemit_sincl(struct sgl_cemit *c, const char *path);
void sgl_cemit_indent(struct sgl_cemit *c);
void sgl_cemit_line(struct sgl_cemit *c, const char *spec, ...);
  
#endif
