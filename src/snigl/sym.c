#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "snigl/buf.h"
#include "snigl/cemit.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/util.h"

struct sgl_sym *sgl_sym_init(struct sgl_sym *s, const char *id, sgl_int_t len) {
  s->id = sgl_strndup(id, len);
  s->cemit_id = -1;
  s->len = len;
  return s;
}

struct sgl_sym *sgl_sym_deinit(struct sgl_sym *s) {
  free(s->id);
  return s;
}

sgl_int_t sgl_sym_cemit_id(struct sgl_sym *s,
                           struct sgl *sgl,
                           struct sgl_cemit *out) {
  if (s->cemit_id != -1) { return s->cemit_id; }
  s->cemit_id = out->sym_id++;

  sgl_cemit_line(out,
                 "struct sgl_sym *sym%" SGL_INT " = sgl_sym(sgl, \"%s\");",
                 s->cemit_id, s->id);
  
  return s->cemit_id;
}

struct sgl_sym *sgl_sym_lcase(struct sgl_sym *s, struct sgl *sgl) {
  struct sgl_buf out;
  sgl_buf_init(&out);

  for (char *in = s->id; *in; in++) {
    char c = *in;
    
    if (c == '|') {
      sgl_buf_putc(&out, '-');
    } else if (isupper(c)) {
      if (in > s->id) { sgl_buf_putc(&out, '-'); }
      sgl_buf_putc(&out, tolower(c));
    } else {
      sgl_buf_putc(&out, c);
    }
  }
  
  struct sgl_sym *os = sgl_sym(sgl, sgl_buf_cs(&out));
  sgl_buf_deinit(&out);
  return os;
}

enum sgl_cmp sgl_sym_cmp(const struct sgl_sym *lhs, const struct sgl_sym *rhs) {
  return sgl_ptr_cmp(lhs, rhs);
}

