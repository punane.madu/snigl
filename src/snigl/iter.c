#include <assert.h>
#include <stddef.h>

#include "snigl/iter.h"

struct sgl_iter *sgl_iter_init(struct sgl_iter *i) {
  i->next_val = NULL;
  i->skip_vals = NULL;
  i->free = NULL;
  i->nrefs = 1;
  return i;
}

void sgl_iter_deref(struct sgl_iter *i, struct sgl *sgl) {
  assert(i->nrefs > 0);  
  if (!--i->nrefs) { i->free(i, sgl); }
}

struct sgl_val *sgl_iter_next_val(struct sgl_iter *i,
                                  struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_ls *root) {
  assert(i->next_val);
  return i->next_val(i, sgl, pos, root);
}

bool sgl_iter_skip_vals(struct sgl_iter *i,
                        struct sgl *sgl,
                        struct sgl_pos *pos,
                        sgl_int_t nvals) {
  assert(i->skip_vals);
  return i->skip_vals(i, sgl, pos, nvals);
}
