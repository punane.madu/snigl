#ifndef SNABL_CONFIG_H
#define SNABL_CONFIG_H

#define SGL_MAX_ARGS 8
#define SGL_MAX_RETS SGL_MAX_ARGS
#define SGL_MAX_TYPES 64
#define SGL_MIN_SLAB 4
#define SGL_MAX_CHAR 255
#define SGL_MAX_STR_LEN 8
#define SGL_MIN_VEC_LEN 4
#define SGL_MAX_IO_ARGS 4
#define SGL_MAX_IO_THREADS 4
#define SGL_USE_POOL
#define SGL_USE_DL

#endif
