#include <assert.h>

#include "snigl/call.h"
#include "snigl/sgl.h"
#include "snigl/state.h"
#include "snigl/task.h"
#include "snigl/try.h"

struct sgl_state *sgl_state_init(struct sgl_state *state, struct sgl *sgl) {
  struct sgl_task *task = sgl->task;
  state->call = task->calls.prev;
  state->lib = task->lib;
  state->reg_stackp = task->reg_stack.prev;
  state->scope = task->scopes.prev;
  state->try = task->tries.prev;
  return state;
}

static void call_deinit(struct sgl_ls *ls, struct sgl *sgl) {
  sgl_call_free(sgl_baseof(struct sgl_call, ls, ls), sgl);  
}

static void scope_deinit(struct sgl_ls *ls, struct sgl *sgl) {
  sgl_scope_deref(sgl_baseof(struct sgl_scope, ls, ls), sgl);  
}

static void try_deinit(struct sgl_ls *ls, struct sgl *sgl) {
  sgl_try_free(sgl_baseof(struct sgl_try, ls, ls), sgl);  
}

static void val_deinit(struct sgl_ls *ls, struct sgl *sgl) {
  sgl_val_free(sgl_baseof(struct sgl_val, ls, ls), sgl);  
}

static void restore_ls(struct sgl *sgl,
                         struct sgl_task *task,
                         struct sgl_ls *root,
                         struct sgl_ls *last,
                         void (*deinit)(struct sgl_ls *, struct sgl *)) {
  for (struct sgl_ls *i = root->prev; i != last;) {
    assert(i != root);
    struct sgl_ls *tmp = i;
    i = i->prev;
    sgl_ls_delete(tmp);
    deinit(tmp, sgl);
  }
}

void sgl_state_restore(struct sgl_state *state, struct sgl *sgl) {
  struct sgl_task *task = sgl->task;
  sgl->task->lib = state->lib;
  restore_ls(sgl, task, &task->reg_stack, state->reg_stackp, val_deinit);
  restore_ls(sgl, task, &task->scopes, state->scope, scope_deinit);
  restore_ls(sgl, task, &task->tries, state->try, try_deinit);
}

void sgl_state_restore_calls(struct sgl_state *state, struct sgl *sgl) {
  struct sgl_task *task = sgl->task;
  restore_ls(sgl, task, &task->calls, state->call, call_deinit);
}
