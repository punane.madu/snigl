#ifndef SNIGL_TRACE_H
#define SNIGL_TRACE_H

#include "snigl/int.h"
#include "snigl/ls.h"
#include "snigl/vec.h"

struct sgl;

struct sgl_trace {
  struct sgl_ls ls, stack;
  sgl_int_t len;
};

struct sgl_trace *sgl_trace_new(struct sgl *sgl);
void sgl_trace_free(struct sgl_trace *t, struct sgl *sgl);

sgl_int_t sgl_trace_len(struct sgl_trace *t);
void sgl_trace_undef(struct sgl_trace *t, struct sgl *sgl, sgl_int_t n);
bool sgl_trace_is_undef(struct sgl_trace *t, struct sgl *sgl, sgl_int_t n);
void sgl_trace_sync(struct sgl_trace *lhs, struct sgl *sgl, struct sgl_trace *rhs);
                  
void sgl_trace_copy(struct sgl_trace *src,
                    struct sgl *sgl,
                    struct sgl_trace *dst);

struct sgl_val *sgl_trace_push(struct sgl_trace *t,
                               struct sgl *sgl,
                               struct sgl_op *op,
                               struct sgl_val *val);

struct sgl_val *sgl_trace_push_new(struct sgl_trace *t,
                                   struct sgl *sgl,
                                   struct sgl_op *op,
                                   struct sgl_type *type);

void sgl_trace_push_undef(struct sgl_trace *t,
                          struct sgl *sgl,
                          struct sgl_type *type);

struct sgl_val *sgl_trace_pop(struct sgl_trace *t, struct sgl *sgl);
void sgl_trace_drop(struct sgl_trace *t, struct sgl *sgl, sgl_int_t nvals);
void sgl_trace_dup(struct sgl_trace *t, struct sgl *sgl, struct sgl_op *op);
void sgl_trace_swap(struct sgl_trace *t, struct sgl *sgl);
void sgl_trace_rotl(struct sgl_trace *t, struct sgl *sgl);
void sgl_trace_rotr(struct sgl_trace *t, struct sgl *sgl);
void sgl_trace_move(struct sgl_trace *src, struct sgl_trace *dst);
void sgl_trace_move_ls(struct sgl_trace *src, struct sgl_ls *dst);
void sgl_trace_clear(struct sgl_trace *t, struct sgl *sgl);

#endif
