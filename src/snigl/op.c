#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "snigl/buf.h"
#include "snigl/call.h"
#include "snigl/cemit.h"
#include "snigl/form.h"
#include "snigl/list.h"
#include "snigl/op.h"
#include "snigl/sgl.h"
#include "snigl/stack.h"
#include "snigl/str.h"
#include "snigl/struct.h"
#include "snigl/sym.h"
#include "snigl/trace.h"
#include "snigl/timer.h"
#include "snigl/try.h"
#include "snigl/types/struct.h"
#include "snigl/val.h"

struct sgl_op_type SGL_NOP,
  SGL_OP_BENCH,
  SGL_OP_CALL, SGL_OP_COUNT,
  SGL_OP_DISPATCH, SGL_OP_DROP, SGL_OP_DUP,
  SGL_OP_EVAL_BEG, SGL_OP_EVAL_END, SGL_OP_FIMP, SGL_OP_FIMP_CALL, SGL_OP_FOR,
  SGL_OP_GET_FIELD, SGL_OP_GET_VAR,
  SGL_OP_IDLE, SGL_OP_IF, SGL_OP_ITER,
  SGL_OP_JUMP,
  SGL_OP_LAMBDA, SGL_OP_LET_VAR, SGL_OP_LIST_BEG, SGL_OP_LIST_END,
  SGL_OP_PAIR, SGL_OP_PUSH, SGL_OP_PUT_FIELD,
  SGL_OP_RECALL, SGL_OP_RETURN, SGL_OP_ROTL, SGL_OP_ROTR,
  SGL_OP_SCOPE_BEG, SGL_OP_SCOPE_END, SGL_OP_STACK_SWITCH, SGL_OP_STR_BEG,
  SGL_OP_STR_END, SGL_OP_STR_PUT, SGL_OP_SWAP,
  SGL_OP_TASK_BEG, SGL_OP_TASK_END, SGL_OP_THROW, SGL_OP_TIMES, SGL_OP_TRY_BEG,
  SGL_OP_TRY_END, SGL_OP_TYPE,
  SGL_OP_YIELD;

static bool const_op(struct sgl_op *op) { return true; }

static bool trace_op(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_clear(t, sgl);
  return false;
}

struct sgl_op_type *sgl_op_type_init(struct sgl_op_type *type, const char *id) {
  type->id = id;
  type->const_op = const_op;
  type->deinit_op = NULL;
  type->deval_op = NULL;
  type->dump_op = NULL;
  type->trace_op = trace_op;
  return type;
}

struct sgl_op *sgl_op_init(struct sgl_op *op,
                           struct sgl *sgl,
                           struct sgl_form *form,
                           struct sgl_op_type *type) {
  op->form = form;
  if (form) { form->nrefs++; }

  op->type = type;
  op->run = NULL;
  op->pc = 0;
  op->cemit_pc = -1;
  op->prev = op->next = NULL;
  
  if (sgl->ops.prev->prev != &sgl->ops) {
    struct sgl_op *prev_op = sgl_baseof(struct sgl_op, ls, sgl->ops.prev->prev);
    prev_op->next = op;
    op->prev = prev_op;
    op->pc = prev_op->pc+1;
  }

  if (sgl->ops.prev != &sgl->ops) {
    op->next = sgl->end_pc;
    sgl->end_pc->prev = op;
    sgl->end_pc->pc++;
  }

  sgl_ls_push(sgl->ops.prev, &op->ls);
  return op;
}

struct sgl_op *sgl_op_deinit(struct sgl_op *op, struct sgl *sgl) {
  if (op->type->deinit_op) { op->type->deinit_op(op, sgl); }
  if (op->form) { sgl_form_deref(op->form, sgl); }
  return op;
}

struct sgl_op *sgl_op_new(struct sgl *sgl,
                          struct sgl_form *form,
                          struct sgl_op_type *type) {
  return sgl_op_init(sgl_malloc(&sgl->op_pool), sgl, form, type);
}

void sgl_op_free(struct sgl_op *op, struct sgl *sgl) {
  sgl_free(&sgl->op_pool, sgl_op_deinit(op, sgl));
}

bool sgl_op_is_const(struct sgl_op *op) { return op->type->const_op(op); }

struct sgl_op *sgl_op_cinit(struct sgl_op *op,
                            struct sgl *sgl,
                            struct sgl_cemit *out) {
  op->cemit_pc = out->pc++;
  if (sgl_form_cemit_id(op->form, sgl, out) == -1) { return sgl->end_pc; }

  if (!op->type->cinit_op) { 
    sgl_error(sgl, &op->form->pos,
              sgl_sprintf("cinit not implemented by op: %s", op->type->id));
    
    return sgl->end_pc;
  }

  return op->type->cinit_op(op, sgl, out);
}

struct sgl_op *sgl_op_cemit(struct sgl_op *op,
                            struct sgl *sgl,
                            struct sgl_cemit *out) {  
  if (!op->type->cemit_op) { return op->next; }
  return op->type->cemit_op(op, sgl, out);
}

void sgl_op_dump(struct sgl_op *op, struct sgl_buf *out) {
  sgl_buf_printf(out, "%" SGL_INT "\t%s", op->pc, op->type->id);
  if (op->type->dump_op) { op->type->dump_op(op, out); }
  sgl_buf_printf(out, " next: %" SGL_INT "\n", op->next->pc);
}

bool sgl_op_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  return op->type->trace_op(op, sgl, t);
}

bool sgl_ops_recalls(struct sgl_op *start) {
  for (struct sgl_op *op = start; op->type != &SGL_OP_RETURN; op = op->next) {
    if (op->type == &SGL_OP_RECALL) { return true; }
    
    if (op->type == &SGL_OP_FIMP) {
      op = op->as_fimp.fimp->imp.end_pc->next;
    } else if (op->type == &SGL_OP_LAMBDA) {
      op = op->as_lambda.end_pc->next;
    }
  }

  return false;
}

static bool fuse_jumps(struct sgl_op **pc, struct sgl *sgl, struct sgl_trace *t) {
  bool changed = false;
  
  for (struct sgl_op *op = (*pc)->next; op != sgl->end_pc; op = op->next) {
    if (op->type == &SGL_OP_JUMP) {
      op = *pc = op->as_jump.pc;
      changed = true;
    } else if (op->type != &SGL_NOP) {
      break;
    }
  }

  return changed;
}

static struct sgl_op *nop_cinit(struct sgl_op *op,
                                struct sgl *sgl,
                                struct sgl_cemit *out) { return op->next; }

static bool nop_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  return false;
}

static struct sgl_op *nop_run(struct sgl_op *op, struct sgl *sgl) { return op->next; }

void sgl_op_deval(struct sgl_op *op, struct sgl *sgl) {
  if (op->type->deval_op) {
    op->type->deval_op(op, sgl);
  } else {
    sgl_nop(op, sgl);
  }
}

void sgl_nop(struct sgl_op *op, struct sgl *sgl) {
  if (op->type->deinit_op) { op->type->deinit_op(op, sgl); }
  op->type = &SGL_NOP;
  op->run = nop_run;
  
  struct sgl_op *last = op;
  while (last->next != sgl->end_pc && last->type == &SGL_NOP) { last = last->next; }
  
  struct sgl_op *first = op;
  while (first != sgl->start_pc) {
    first = first->prev;
    if (first->type != &SGL_NOP) { break; }
  }
  
  for (op = last->prev; ; op = op->prev) {
    op->next = last;
    if (op == first) { break; }
  }
}

struct sgl_op *sgl_nop_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_NOP);
  op->run = nop_run;
  return op;  
}

static struct sgl_op *bench_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *reps = sgl_pop(sgl);
  if (!reps) { return sgl->end_pc; }

  if (reps->type != sgl->Int) {
    sgl_error(sgl, &op->form->pos,
              sgl_sprintf("Invalid reps: %s", reps->type->def.id->id));
    
    return sgl->end_pc;
  }

  struct sgl_op
    *start_op = op->next,
    *end_op = op->as_bench.end_pc->next;

  struct sgl_task *task = sgl->task;
  struct sgl_ls *stack = sgl_reg_stack(sgl);
  sgl_val_dup(reps, sgl, stack);
  if (!sgl_run_task(sgl, task, start_op, end_op)) { return sgl->end_pc; }

  struct sgl_timer t;
  sgl_ls_push(stack, &reps->ls);
  sgl_timer_init(&t);
  if (!sgl_run_task(sgl, task, start_op, end_op)) { return sgl->end_pc; }
  sgl_push(sgl, sgl->Int)->as_int = sgl_timer_ms(&t);
  return op->as_bench.end_pc->next;
}

struct sgl_op *sgl_op_bench_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_BENCH);
  op->run = bench_run;
  op->as_bench.end_pc = NULL;
  return op;
}

static void call_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = op->as_call.val;
  if (v) { sgl_val_free(v, sgl); }
}

static void call_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_val *v = op->as_call.val;

  if (v) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(v, out);
  }
}

static bool call_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_call *c = &op->as_call;
  bool changed = false;
  if (c->val) { goto exit; }
  struct sgl_val *v = c->val = sgl_trace_pop(t, sgl);

  if (v) {
    assert(v->op);
    sgl_op_deval(v->op, sgl);
    changed = true;
  }
 exit:
  sgl_trace_clear(t, sgl);
  return changed;
}

static struct sgl_op *call_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = op->as_call.val;
  if (!v && !(v = sgl_pop(sgl))) { return NULL; }
  return sgl_val_call(v, sgl, &op->form->pos, op, false);
}

void sgl_op_call_reuse(struct sgl_op *op, struct sgl *sgl) {
  if (op->type->deinit_op) { op->type->deinit_op(op, sgl); }
  op->type = &SGL_OP_CALL;
  op->run = call_run;
  op->as_call.val = NULL;
}

static struct sgl_op *count_cinit(struct sgl_op *op,
                                  struct sgl *sgl,
                                  struct sgl_cemit *out) {
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_count_new(sgl, f%" SGL_INT ", op%" SGL_INT ");",
                 op->cemit_pc, form_id, op->as_count.pc->cemit_pc);
  
  return op->next;
}

static bool count_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_clear(t, sgl);
  return fuse_jumps(&op->as_count.pc, sgl, t);
}

static struct sgl_op *count_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = sgl_peek_reg(sgl);
  
  if (!--v->as_int) {
    sgl_ls_delete(&v->ls);
    sgl_val_free(v, sgl);
    return op->next;
  }
  
  return op->as_count.pc->next;
}

struct sgl_op *sgl_op_count_new(struct sgl *sgl,
                                struct sgl_form *form,
                                struct sgl_op *pc) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_COUNT);
  op->run = count_run;
  op->as_count.pc = pc;
  return op;
}

static struct sgl_op *dispatch_cinit(struct sgl_op *op,
                                     struct sgl *sgl,
                                     struct sgl_cemit *out) {
  struct sgl_pos *pos = &op->form->pos;
  struct sgl_op_dispatch *d = &op->as_dispatch;
  
  sgl_int_t func_id = sgl_func_cemit_id(d->func, sgl, pos, out);
  if (func_id == -1) { return sgl->end_pc; }

  sgl_int_t fimp_id = d->fimp
    ? sgl_fimp_cemit_id(d->fimp, sgl, pos, out)
    : out->fimp_id++;

  if (fimp_id == -1) { return sgl->end_pc; }
  sgl_int_t form_id = op->form->cemit_id;

  if (!d->fimp) {
    sgl_cemit_line(out, "struct sgl_fimp *fimp%" SGL_INT " = NULL;", fimp_id);
  }
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_dispatch_new(sgl, f%" SGL_INT ", "
                 "func%" SGL_INT ", "
                 "fimp%" SGL_INT");",
                 op->cemit_pc, form_id, func_id, fimp_id);
  
  return op->next;
}

static void dispatch_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_dispatch *d = &op->as_dispatch;
  sgl_buf_printf(out, " %s", d->func->def.id->id);
  if (d->fimp) { sgl_buf_printf(out, "(%s)", d->fimp->id->id); }
}

static bool dispatch_const(struct sgl_op *op) { return false; }

static bool dispatch_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_dispatch *d = &op->as_dispatch;
  struct sgl_fimp *fimp = d->fimp;

  if (t->len >= d->func->nargs) {
    if (sgl_trace_is_undef(t, sgl, d->func->nargs)) {
      if (fimp) {
        if (!sgl_fimp_match_types(fimp, sgl, &t->stack)) { goto fail; }
      } else if (!(fimp = sgl_func_dispatch_types(d->func, sgl, &t->stack))) {
        goto fail;
      }
    } else {
      if (fimp) {
        if (!sgl_fimp_match(d->fimp, sgl, &t->stack)) { goto fail; }
      } else if (!(fimp = sgl_func_dispatch(d->func, sgl, &t->stack))) {
        goto fail;
      }
    }
  } else {
    goto fail;
  }

  if (!sgl_fimp_compile(fimp, sgl, &op->form->pos)) {
    sgl_trace_clear(t, sgl);
    return false;
  }

  sgl_op_fimp_call_reuse(op, sgl, fimp);
  sgl_op_trace(op, sgl, t);
  return true;

 fail:
  sgl_trace_drop(t, sgl, d->func->nargs);

  if (fimp) {
    for (struct sgl_type **r = fimp->rets; r < fimp->rets + fimp->nrets; r++) {
      sgl_trace_push_undef(t, sgl, *r);
    }
  }

  return false; 
}

static struct sgl_op *dispatch_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_ls *stack = sgl_stack(sgl);
  struct sgl_op_dispatch *d = &op->as_dispatch;
  struct sgl_fimp *fimp = d->fimp;
  
  if (fimp) {
    if (!sgl_fimp_match(fimp, sgl, stack)) {
      sgl_error(sgl, &op->form->pos,
                sgl_sprintf("Fimp not applicable: %s (%s)",
                            d->func->def.id->id, fimp->id->id));
      
      return sgl->end_pc;
    }
  } else {
    fimp = sgl_func_dispatch(d->func, sgl, stack);
    
    if (!fimp) {
      sgl_error(sgl, &op->form->pos,
                sgl_sprintf("Func not applicable: %s", d->func->def.id->id));
      
      return sgl->end_pc;
    }
  }
    
  struct sgl_op *pc = sgl_fimp_call(fimp, sgl, &op->form->pos, op);
  return pc ? pc : sgl->end_pc;
}

struct sgl_op *sgl_op_dispatch_new(struct sgl *sgl,
                                   struct sgl_form *form,
                                   struct sgl_func *func,
                                   struct sgl_fimp *fimp) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_DISPATCH);
  op->run = dispatch_run;
  op->as_dispatch = (struct sgl_op_dispatch){.func = func, .fimp = fimp};
  return op;
}

static bool drop_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_drop *d = &op->as_drop;
  bool changed = false;
  
  while (d->nvals) {
    struct sgl_val *v = sgl_trace_pop(t, sgl);
    if (!v) { break; }
    sgl_op_deval(v->op, sgl);
    d->nvals--;
    changed = true;
  }

  if (d->nvals) {
    sgl_trace_drop(t, sgl, d->nvals);
  } else {
    sgl_nop(op, sgl);
    changed = true;
  }
  
  return changed;
}

static struct sgl_op *drop_run(struct sgl_op *op, struct sgl *sgl) {
  sgl_int_t nvals = op->as_drop.nvals;
  
  return (sgl_stack_drop(sgl_stack(sgl), sgl, nvals, false) == nvals)
    ? op->next
    : sgl->end_pc;
}

struct sgl_op *sgl_op_drop_new(struct sgl *sgl,
                               struct sgl_form *form,
                               sgl_int_t nvals) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_DROP);
  op->run = drop_run;
  op->as_drop.nvals = nvals;
  return op;
}

static struct sgl_op *dup_cinit(struct sgl_op *op,
                                struct sgl *sgl,
                                struct sgl_cemit *out) {
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_dup_new(sgl, f%" SGL_INT ");",
                 op->cemit_pc, form_id);
  
  return op->next;
}

static bool dup_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_dup(t, sgl, op);
  return false;
}

static struct sgl_op *dup_run(struct sgl_op *op, struct sgl *sgl) {
  return sgl_stack_dup(sgl_stack(sgl), sgl, false) ? op->next : sgl->end_pc;
}

struct sgl_op *sgl_op_dup_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_DUP);
  op->run = dup_run;
  return op;
}

static void eval_beg_dump(struct sgl_op *op, struct sgl_buf *out) {
  sgl_buf_printf(out, " end: %" SGL_INT, op->as_eval_beg.end_pc->pc);
}

static bool eval_beg_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_eval_beg *fb = &op->as_eval_beg;
  struct sgl_op *start_pc = op;
  bool nop = true, changed = false;
  
  for (op = start_pc->next; op != fb->end_pc; op = op->next) {
    nop &=
      op->type != &SGL_OP_FIMP &&
      op->type != &SGL_OP_LAMBDA &&
      op->type != &SGL_OP_TASK_BEG;
  }

  if (nop) {
    for (op = start_pc; op != fb->end_pc->next; op = op->next) { sgl_nop(op, sgl); }
    changed = true;
  } else {
    for (op = fb->end_pc->next; t->len && op != sgl->end_pc; op = op->next) {
      changed |= sgl_op_trace(op, sgl, t);
    }
    
    sgl_trace_clear(t, sgl);
  }
  
  return changed;
}

static struct sgl_op *eval_beg_run(struct sgl_op *op, struct sgl *sgl) {
  return op->as_eval_beg.end_pc->next;
}

struct sgl_op *sgl_op_eval_beg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_EVAL_BEG);
  op->run = eval_beg_run;
  op->as_eval_beg.end_pc = NULL;
  return op;
}

static struct sgl_op *eval_end_run(struct sgl_op *op, struct sgl *sgl) {
  return op->next;
}

struct sgl_op *sgl_op_eval_end_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_EVAL_END);
  op->run = eval_end_run;
  return op;
}

static struct sgl_op *fimp_cinit(struct sgl_op *op,
                                 struct sgl *sgl,
                                 struct sgl_cemit *out) {
  struct sgl_fimp *f = op->as_fimp.fimp;
  struct sgl_pos *pos = &op->form->pos;
  sgl_int_t form_id = op->form->cemit_id;
  sgl_int_t fimp_id = sgl_fimp_cemit_id(f, sgl, pos, out);
  if (fimp_id == -1) { return sgl->end_pc; }

  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_fimp_new(sgl, f%" SGL_INT ", fimp%" SGL_INT ");",
                 op->cemit_pc, form_id, fimp_id);
  
  sgl_cemit_line(out,
                 "fimp%" SGL_INT "->imp.start_pc = op%" SGL_INT ";",
                 fimp_id, op->cemit_pc);

  if (f->imp.end_pc == f->imp.start_pc) {
    sgl_cemit_line(out,
                   "fimp%" SGL_INT "->imp.end_pc = op%" SGL_INT ";",
                   fimp_id, op->cemit_pc);

    return op->next;
  }

  for (op = op->next;
       op != f->imp.end_pc && op != sgl->end_pc;
       op = sgl_op_cinit(op, sgl, out));
  
  if (op == sgl->end_pc) { return sgl->end_pc; }
  struct sgl_op *end_pc = op;
  op = sgl_op_cinit(op, sgl, out);
  if (op == sgl->end_pc) { return sgl->end_pc; }
  
  sgl_cemit_line(out,
                 "fimp%" SGL_INT "->imp.end_pc = op%" SGL_INT ";",
                 fimp_id, end_pc->cemit_pc);

  return op;
}

static void fimp_dump(struct sgl_op *op, struct sgl_buf *out) {
  sgl_buf_printf(out, " end: %" SGL_INT, op->as_fimp.fimp->imp.end_pc->pc);
}

static bool fimp_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_fimp *f = op->as_fimp.fimp;
  bool changed = false;
  
  if (t->len) {
    for (op = f->imp.end_pc->next;
         t->len && op != sgl->end_pc;
         op = op->next) { changed |= sgl_op_trace(op, sgl, t); }

    sgl_trace_clear(t, sgl);
  }

  return changed;
}

static struct sgl_op *fimp_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_fimp *f = op->as_fimp.fimp;
  struct sgl_op *next = op->next;
  
  if (next->type == &SGL_OP_SCOPE_BEG && !next->as_scope_beg.parent) {
    struct sgl_scope *s = sgl_scope(sgl);
    next->as_scope_beg.parent = s;
    s->nrefs++;
  }

  return f->imp.end_pc->next;
}

struct sgl_op *sgl_op_fimp_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_fimp *fimp) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_FIMP);
  op->run = fimp_run;
  op->as_fimp.fimp = fimp;
  return op;
}

static struct sgl_op *fimp_call_cinit(struct sgl_op *op,
                                      struct sgl *sgl,
                                      struct sgl_cemit *out) {
  struct sgl_pos *pos = &op->form->pos;
  sgl_int_t fimp_id = sgl_fimp_cemit_id(op->as_fimp_call.fimp, sgl, pos, out);
  if (fimp_id == -1) { return sgl->end_pc; }
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_fimp_call_new(sgl, f%" SGL_INT ", fimp%" SGL_INT ");",
                 op->cemit_pc, form_id, fimp_id);
  
  return op->next;
}

static bool fimp_call_const(struct sgl_op *op) { return false; }

static void fimp_call_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_fimp *f = op->as_fimp_call.fimp;
  sgl_buf_printf(out, " %s(%s)", f->func->def.id->id, f->id->id);
}

static bool fimp_call_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_fimp_call *c = &op->as_fimp_call;
  struct sgl_fimp *f = c->fimp;

  if (f == sgl->call_fimp) {
    sgl_op_call_reuse(op, sgl);
    sgl_op_trace(op, sgl, t);
    return true;
  }
  
  struct sgl_imp *i = &f->imp;
  f->trace_depth++;
  bool changed = false;
  
  if (f->cimp || i->end_pc == i->start_pc) {
    sgl_trace_drop(t, sgl, f->func->nargs);

    if (f->nrets == -1) {
      sgl_trace_clear(t, sgl);
    } else {
      for (struct sgl_type **r = f->rets; r < f->rets + f->nrets; r++) {
        sgl_trace_push_undef(t, sgl, *r);
      }
    }
  } else {
    for (struct sgl_arg *a = f->args + t->len; a < f->args + f->func->nargs; a++) {
      sgl_trace_push_undef(t, sgl, a->type); 
    }

    if (f->trace_depth == 1) {  
      for (op = i->start_pc->next;
           op != i->end_pc;
           op = op->next) { changed |= sgl_op_trace(op, sgl, t); }

      sgl_trace_undef(t, sgl, (f->nrets == -1) ? t->len : f->nrets);
    } else {
      sgl_trace_clear(t, sgl);
    }

    for (struct sgl_type **r = f->rets + t->len; r < f->rets + f->nrets; r++) {
      sgl_trace_push_undef(t, sgl, *r);
    }
  }
  
  f->trace_depth--;
  return changed;
}

static struct sgl_op *fimp_call_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op *pc = sgl_fimp_call(op->as_fimp_call.fimp, sgl, &op->form->pos, op);
  return pc ? pc : sgl->end_pc;
}

struct sgl_op *sgl_op_fimp_call_new(struct sgl *sgl,
                                    struct sgl_form *form,
                                    struct sgl_fimp *fimp) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_FIMP_CALL);
  op->run = fimp_call_run;
  op->as_fimp_call = (struct sgl_op_fimp_call){.fimp = fimp};
  return op;
}

void sgl_op_fimp_call_reuse(struct sgl_op *op,
                            struct sgl *sgl,
                            struct sgl_fimp *fimp) {
  if (op->type->deinit_op) { op->type->deinit_op(op, sgl); }
  op->type = &SGL_OP_FIMP_CALL;
  op->run = fimp_call_run;
  op->as_fimp_call = (struct sgl_op_fimp_call){.fimp = fimp};
}

static struct sgl_op *for_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *iv = sgl_pop(sgl);
  if (!iv) { return sgl->end_pc; }
  struct sgl_type *it = NULL;
  struct sgl_iter *i = sgl_val_iter(iv, sgl, &it);
  if (!i) { return sgl->end_pc; }

  sgl_val_reuse(iv, sgl, iv->type, true)->as_iter = i;
  sgl_ls_push(sgl_reg_stack(sgl), &iv->ls);

  if (!sgl_iter_next_val(i, sgl, &op->form->pos, sgl_stack(sgl))) {
    sgl_ls_delete(&iv->ls);
    sgl_val_free(iv, sgl);
    return op->as_for.end_pc->next;
  }
  
  return op->next;
}

struct sgl_op *sgl_op_for_new(struct sgl *sgl,
                              struct sgl_form *form,
                              struct sgl_op *end_pc) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_FOR);
  op->run = for_run;
  op->as_for.end_pc = end_pc;
  return op;
}

static bool init_field(struct sgl_op_field *f,
                       struct sgl *sgl,
                       struct sgl_pos *pos,
                       struct sgl_sym *id) {
  char
    *start = id->id,
    *div = strchr(start, '/');
  
  if (div) {
    struct sgl_sym *tid = sgl_nsym(sgl, start, div - start);
    f->id = sgl_sym(sgl, div+1);
    
    f->struct_type = sgl_find_type(sgl, pos, tid);
    
    if (!f->struct_type) {
      sgl_error(sgl, pos, sgl_sprintf("Unknown type: %s", tid->id));
      return false;
    }
    
    struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, f->struct_type);
    struct sgl_struct_type *st = sgl_baseof(struct sgl_struct_type, ref_type, rt);
    f->field = sgl_struct_type_find(st, f->id);
    
    if (!f->field) {
      sgl_error(sgl, pos, sgl_sprintf("Unknown field: %s", f->id->id));
      return false;
    }
  } else {
    f->struct_type = NULL;
    f->field = NULL;
    f->id = id;
  }

  f->val = NULL;
  return true;
}

static void field_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_field *f = &op->as_field;
  
  if (f->field) {
    sgl_buf_printf(out, " %s.%s",
                   sgl_type_id(f->struct_type)->id,
                   f->field->id->id);
  } else {
    sgl_buf_printf(out, " %s", f->id->id);
  }

  if (f->val) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(f->val, out);
  }
}

static bool get_field_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_field *f = op->as_field.field;
  sgl_trace_push_undef(t, sgl, f ? f->type : sgl->A);
  return false;
}

static struct sgl_op *get_field_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_field *gf = &op->as_field;
  struct sgl_val *sv = sgl_pop(sgl);
  if (!sv) { return sgl->end_pc; }
  struct sgl_type *t = sv->type, *st = gf->struct_type;
  struct sgl_field *f = gf->field;
  
  if (!f) {
    if (!sgl_derived(t, sgl->Struct)) {
      sgl_error(sgl, &op->form->pos,
                sgl_sprintf("Expected struct, was: %s", t->def.id->id));
      
      return sgl->end_pc;
    }


    struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, t);
    struct sgl_struct_type *st = sgl_baseof(struct sgl_struct_type, ref_type, rt);
    struct sgl_sym *id = gf->id;
    f = sgl_struct_type_find(st, id);
    
    if (!f) {
      sgl_error(sgl, &op->form->pos, sgl_sprintf("Unknown field: %s", id->id));
      return sgl->end_pc;
    }
  } else if (t != st) {
    sgl_error(sgl, &op->form->pos,
              sgl_sprintf("Expected %s, was: %s", st->def.id->id, t->def.id->id));
    
    return sgl->end_pc;
  }

  struct sgl_val **v = sv->as_struct->fields + f->i;

  if (*v) {
    sgl_val_dup(*v, sgl, sgl_stack(sgl));
  } else {
    sgl_push(sgl, sgl->Nil);
  }
  
  sgl_val_free(sv, sgl);
  return op->next;
}

struct sgl_op *sgl_op_get_field_new(struct sgl *sgl,
                                struct sgl_form *form,
                                struct sgl_sym *id) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_GET_FIELD);
  op->run = get_field_run;

  if (!init_field(&op->as_field, sgl, &form->pos, id)) {
    sgl_op_free(op, sgl);
    return NULL;
  }
  
  return op;
}

static void get_var_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_get_var *gv = &op->as_get_var;
  sgl_buf_printf(out, " %s", gv->id->id);

  if (gv->val) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(gv->val, out);
  }
}

static bool get_var_const(struct sgl_op *op) { return false; }

static bool get_var_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_get_var *gv = &op->as_get_var;
  struct sgl_val *v = gv->val;
  
  if (v) {
    sgl_trace_push(t, sgl, op, gv->val);
  } else {
    sgl_trace_push_undef(t, sgl, sgl->T);
  }
  
  return false;
}

static struct sgl_op *get_var_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_get_var *gv = &op->as_get_var;
  struct sgl_val *v = gv->val;

  if (!v) {
    v = sgl_get_var(sgl, gv->id);    
  
    if (!v) {
      sgl_error(sgl, &op->form->pos, sgl_sprintf("Unknown var: %s", gv->id->id));
      return sgl->end_pc;
    }
  }

  sgl_val_dup(v, sgl, sgl_stack(sgl));
  return op->next;
}

struct sgl_op *sgl_op_get_var_new(struct sgl *sgl,
                                  struct sgl_form *form,
                                  struct sgl_sym *id) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_GET_VAR);
  op->run = get_var_run;
  op->as_get_var = (struct sgl_op_get_var){.id = id, .val = NULL};
  return op;
}

static bool iter_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_clear(t, sgl);
  return fuse_jumps(&op->as_iter.pc, sgl, t);
}

static struct sgl_op *iter_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = sgl_peek_reg(sgl);

  if (!v) {
    sgl_error(sgl, &op->form->pos, sgl_strdup("Missing iter"));
    return sgl->end_pc;
  }
  
  if (!sgl_iter_next_val(v->as_iter, sgl, &op->form->pos, sgl_stack(sgl))) {
    sgl_ls_delete(&v->ls);
    sgl_val_free(v, sgl);
    return op->next;
  }
  
  return op->as_iter.pc->next;
}

struct sgl_op *sgl_op_iter_new(struct sgl *sgl,
                                struct sgl_form *form,
                                struct sgl_op *pc) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_ITER);
  op->run = iter_run;
  op->as_iter.pc = pc;
  return op;
}

static struct sgl_op *idle_run(struct sgl_op *op, struct sgl *sgl) {
  return (sgl->ntasks == 1) ? op->as_idle.end_pc->next : op->next;
}

struct sgl_op *sgl_op_idle_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_IDLE);
  op->run = idle_run;
  op->as_idle.end_pc = NULL;
  return op;
}

static void if_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_if *i = &op->as_if;
  sgl_buf_printf(out, " end: %" SGL_INT, i->pc->pc);
  if (i->neg) { sgl_buf_putcs(out, " neg"); }
}

static bool if_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op *start_op = op;
  bool changed = fuse_jumps(&op->as_if.pc, sgl, t);
  if (!t->len) { return changed; }
  struct sgl_trace *t_if = sgl_trace_new(sgl);
  sgl_trace_move(t, t_if);
    
  for (op = start_op->next; t_if->len && op != sgl->end_pc; op = op->next) {
    if (op == start_op->as_if.pc->next) {
      sgl_trace_sync(t, sgl, t_if);
      continue;
    }
    
    changed |= sgl_op_trace(op, sgl, t_if);
  }

  sgl_trace_free(t_if, sgl);
  return changed;
}

static struct sgl_op *if_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *cond = sgl_pop(sgl);
  if (!cond) { return sgl->end_pc; }
  struct sgl_op_if *i = &op->as_if;
  bool ok = sgl_val_bool(cond);
  if (i->neg) { ok = !ok; }
  struct sgl_op *pc = ok ? i->pc->next : op->next;
  sgl_val_free(cond, sgl);
  return pc;
}

struct sgl_op *sgl_op_if_new(struct sgl *sgl,
                             struct sgl_form *form,
                             struct sgl_op *pc) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_IF);
  op->run = if_run;
  op->as_if = (struct sgl_op_if){.neg = false, .pc = pc};
  return op;
}

static void jump_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_jump *j = &op->as_jump;
  sgl_buf_printf(out, " %" SGL_INT, j->pc->next->pc);
}

static bool jump_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  bool changed = fuse_jumps(&op->as_jump.pc, sgl, t);
  
  if (t->len) {
    for (op = op->as_jump.pc->next; t->len && op != sgl->end_pc; op = op->next) {
      changed |= sgl_op_trace(op, sgl, t);
    }
  }

  sgl_trace_clear(t, sgl);
  return changed;
}

static struct sgl_op *jump_run(struct sgl_op *op, struct sgl *sgl) {
  return op->as_jump.pc->next;
}

struct sgl_op *sgl_op_jump_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_op *pc) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_JUMP);
  op->run = jump_run;
  op->as_jump.pc = pc;
  return op;
}

static void let_var_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_let_var *lv = &op->as_let_var;
  sgl_buf_printf(out, " %s", lv->id->id);

  if (lv->val) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(lv->val, out);
  }
}

static bool let_var_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_let_var *lv = &op->as_let_var;
  struct sgl_val *v = lv->val;
  bool changed = false;
  
  if (!v) {
    v = sgl_trace_pop(t, sgl);
    if (!v) { return false; }
    assert(v->op);
    sgl_op_deval(v->op, sgl);
    op->as_let_var.val = v;
    changed = true;
  }

  if (!v) { return changed; }  
  sgl_int_t scope_depth = 1;
  
  for (op = op->next; scope_depth && op != sgl->end_pc; op = op->next) {
    if (op->type == &SGL_OP_LET_VAR) {
      if (op->as_let_var.id == lv->id) {
        if (scope_depth == 1) {
          sgl_error(sgl, &op->form->pos, sgl_sprintf("Dup var: %s", lv->id->id));
          return false;
        } else {
          break;
        }
      }
    } else if (op->type == &SGL_OP_GET_VAR) {
      struct sgl_op_get_var *gv = &op->as_get_var;
      
      if (gv->id == lv->id && !gv->val) {
        gv->val = sgl_val_dup(v, sgl, NULL);
        changed = true;
      }
    } else if (op->type == &SGL_OP_SCOPE_BEG) {
      scope_depth++;
    } else if (op->type == &SGL_OP_SCOPE_END) {
      scope_depth--;
    } else if (op->type == &SGL_OP_TASK_BEG) {
      op = op->as_task_beg.end_pc;
    } else if (op->type == &SGL_OP_FIMP) {
      op = op->as_fimp.fimp->imp.end_pc;
    } else if (op->type == &SGL_OP_LAMBDA) {
      op = op->as_lambda.end_pc;
    } else if (op->type == &SGL_OP_JUMP) {
      op = op->as_jump.pc;
    } else if (op->type == &SGL_OP_CALL ||
               op->type == &SGL_OP_DISPATCH ||
               op->type == &SGL_OP_FIMP_CALL ||
               op->type == &SGL_OP_RETURN ||
               op->type == &SGL_OP_TASK_END) {
      break;
    }  
  }
  
  return changed;
}

static struct sgl_op *let_var_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_let_var *sv = &op->as_let_var;
  struct sgl_val *val = sv->val ? sgl_val_dup(sv->val, sgl, NULL) : NULL;
  
  if (!val) {
    val = sgl_pop(sgl);  
    if (!val) { return sgl->end_pc; }
  }

  return sgl_let_var(sgl, &op->form->pos, sv->id, val) ? op->next : sgl->end_pc;
}

static void let_var_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *val = op->as_let_var.val;
  if (val) { sgl_val_free(val, sgl); }
}

struct sgl_op *sgl_op_let_var_new(struct sgl *sgl,
                                  struct sgl_form *form,
                                  struct sgl_sym *id) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_LET_VAR);
  op->run = let_var_run;
  op->as_let_var = (struct sgl_op_let_var){.id = id, .val = NULL};
  return op;
}

static void lambda_deval(struct sgl_op *op, struct sgl *sgl) {
  op->as_lambda.push_val = false;
}

static void lambda_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_lambda *l = &op->as_lambda;
  sgl_buf_printf(out, " end: %" SGL_INT, l->end_pc->pc);
  if (l->push_val) { sgl_buf_putcs(out, " push"); }
}

static bool lambda_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_lambda *l = &op->as_lambda;
  bool changed = false;

  if (l->push_val) {
    sgl_trace_push_new(t, sgl, op, sgl->Lambda)->as_lambda = l;
    
    for (op = l->end_pc->next; t->len && op != sgl->end_pc; op = op->next) {
      changed |= sgl_op_trace(op, sgl, t);
    }
  }
  
  sgl_trace_clear(t, sgl);
  return changed;
}

static struct sgl_op *lambda_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_lambda *l = &op->as_lambda;
  if (l->push_val) { sgl_push(sgl, l->type)->as_lambda = l; }
  struct sgl_op *next = op->next;  

  if (next->type == &SGL_OP_SCOPE_BEG && !next->as_scope_beg.parent) {
    struct sgl_scope *s = sgl_scope(sgl);
    next->as_scope_beg.parent = s;
    s->nrefs++;
  }
  
  return l->end_pc->next;
}

struct sgl_op *sgl_op_lambda_new(struct sgl *sgl,
                                 struct sgl_form *form,
                                 bool is_nil) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_LAMBDA);
  op->run = lambda_run;
  
  op->as_lambda = (struct sgl_op_lambda){.start_pc = is_nil ? NULL : op,
                                         .end_pc = is_nil ? op : NULL,
                                         .type = sgl->Lambda,
                                         .push_val = true};

  return op;
}

static bool list_beg_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_trace *t_list = sgl_trace_new(sgl);
  struct sgl_op *start_pc = op;
  bool changed = false;
  sgl_int_t depth = 1;
  
  for (op = start_pc->next; depth && op != sgl->end_pc; op = op->next) {
    changed |= op->type != &SGL_OP_LIST_END && sgl_op_trace(op, sgl, t_list);

    if (!t_list->len && (op != start_pc->next || op->type != &SGL_OP_LIST_END)) {
      goto exit;
    }
               
    if (op->type == &SGL_OP_LIST_BEG) {
      depth++;
    } else if (op->type == &SGL_OP_LIST_END) {
      depth--;
    } else if (!sgl_op_is_const(op)) {
      break;
    }
  }
  
  if (depth) { goto exit; }
  struct sgl_op *end_pc = op->prev;

  if (!sgl_trace_is_undef(t, sgl, t->len)) {
    struct sgl_val *v = sgl_val_new(sgl, sgl->List, NULL);
    struct sgl_list *l = v->as_list = sgl_list_new(sgl);
    sgl_trace_move_ls(t_list, &l->root);
  
    sgl_ls_do(&l->root, struct sgl_val, ls, lv) {
      assert(lv->op);
      sgl_op_deval(lv->op, sgl);
    }

    sgl_op_push_reuse(start_pc, sgl, v);
    sgl_nop(end_pc, sgl);
    changed = true;
  }

 exit:
  sgl_trace_free(t_list, sgl);
  return changed;
}

static struct sgl_op *list_beg_run(struct sgl_op *op, struct sgl *sgl) {
  sgl_beg_stack(sgl);
  return op->next;
}

struct sgl_op *sgl_op_list_beg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_LIST_BEG);
  op->run = list_beg_run;
  return op;
}

static bool list_end_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_push_undef(t, sgl, sgl->List);
  return false;
}

static struct sgl_op *list_end_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_list *l = sgl_end_stack(sgl);
  sgl_push(sgl, sgl->List)->as_list = l;
  return op->next;
}

struct sgl_op *sgl_op_list_end_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_LIST_END);
  op->run = list_end_run;
  return op;
}

static bool pair_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_val *lst = sgl_trace_pop(t, sgl), *fst = sgl_trace_pop(t, sgl);

  if (!fst || !lst) {
    sgl_trace_push_undef(t, sgl, sgl->Pair);
    return false;
  }

  assert(fst->op);
  sgl_op_deval(fst->op, sgl);

  assert(lst->op);
  sgl_op_deval(lst->op, sgl);

  struct sgl_val *v = sgl_val_new(sgl, sgl->Pair, NULL);
  sgl_pair_init(&v->as_pair, fst, lst);
  sgl_op_push_reuse(op, sgl, v);
  sgl_op_trace(op, sgl, t);
  return true;
}

static struct sgl_op *pair_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *last = sgl_pop(sgl);
  if (!last) { return sgl->end_pc; }
  struct sgl_val *first = sgl_pop(sgl);
  if (!first) { return sgl->end_pc; }
  sgl_pair_init(&sgl_push(sgl, sgl->Pair)->as_pair, first, last); 
  return op->next;
}

struct sgl_op *sgl_op_pair_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_PAIR);
  op->run = pair_run;
  return op;
}

static void push_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_push *p = &op->as_push;
  sgl_buf_putc(out, ' ');
  sgl_val_dump(p->val, out);
}

static bool push_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_push(t, sgl, op, op->as_push.val);
  return false;
}

static struct sgl_op *push_cinit(struct sgl_op *op,
                                 struct sgl *sgl,
                                 struct sgl_cemit *out) {
  sgl_int_t fid = op->form->cemit_id;

  if (op->form->type == &SGL_FORM_LIT) {
    sgl_cemit_line(out,
                   "v = sgl_val_dup(f%" SGL_INT "->as_lit.val, sgl, NULL);",
                   fid);
  } else {
    sgl_val_cemit(op->as_push.val, sgl, &op->form->pos, "v", "NULL", out);
  }

  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_push_new(sgl, f%" SGL_INT ", v);",
                 op->cemit_pc, fid);

  return op->next;
}

static void push_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *val = op->as_push.val;
  if (val) { sgl_val_free(val, sgl); }
}

static struct sgl_op *push_run(struct sgl_op *op, struct sgl *sgl) {
  return sgl_val_clone(op->as_push.val, sgl, &op->form->pos, sgl_stack(sgl))
    ? op->next
    : sgl->end_pc;
}

struct sgl_op *sgl_op_push_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_val *val) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_PUSH);
  op->run = push_run;
  op->as_push.val = val;
  return op;
}

void sgl_op_push_reuse(struct sgl_op *op, struct sgl *sgl, struct sgl_val *val) {
  if (op->type->deinit_op) { op->type->deinit_op(op, sgl); }
  op->type = &SGL_OP_PUSH;
  op->run = push_run;
  op->as_push.val = val;
}

static struct sgl_op *put_field_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_field *pf = &op->as_field;

  struct sgl_val
    *v = pf->val ? sgl_val_dup(pf->val, sgl, NULL) : sgl_pop(sgl),
    *sv = sgl_pop(sgl);

  if (!v || !sv) { return sgl->end_pc; }
  
  struct sgl_type *t = sv->type, *st = pf->struct_type;
  struct sgl_field *f = pf->field;
  
  if (!f) {
    if (!sgl_derived(t, sgl->Struct)) {
      sgl_error(sgl, &op->form->pos,
                sgl_sprintf("Expected struct, was: %s", t->def.id->id));
      
      return sgl->end_pc;
    }

    struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, t);
    struct sgl_struct_type *st = sgl_baseof(struct sgl_struct_type, ref_type, rt);
    struct sgl_sym *id = pf->id;    
    f = sgl_struct_type_find(st, id);
    
    if (!f) {
      sgl_error(sgl, &op->form->pos, sgl_sprintf("Unknown field: %s", id->id));
      return sgl->end_pc;
    }
  } else if (t != st) {
    sgl_error(sgl, &op->form->pos,
              sgl_sprintf("Expected %s, was: %s", st->def.id->id, t->def.id->id));
    
    return sgl->end_pc;
  }

  if (!pf->val && !sgl_derived(v->type, f->type)) {
    sgl_error(sgl, &op->form->pos,
              sgl_sprintf("Expected %s, was: %s",
                          f->type->def.id->id, v->type->def.id->id));
    
    return sgl->end_pc;
  }

  struct sgl_val **fv = sv->as_struct->fields + f->i;
  if (*fv) { sgl_val_free(*fv, sgl); }
  *fv = v;
  sgl_val_free(sv, sgl);
  return op->next;
}

struct sgl_op *sgl_op_put_field_new(struct sgl *sgl,
                                    struct sgl_form *form,
                                    struct sgl_sym *id,
                                    struct sgl_val *val) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_PUT_FIELD);
  op->run = put_field_run;

  if (!init_field(&op->as_field, sgl, &form->pos, id)) {
    sgl_op_free(op, sgl);
    return NULL;
  }

  op->as_field.val = val;
  return op;
}

static struct sgl_op *recall_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_pos *pos = &op->form->pos;
  struct sgl_ls *cs = &sgl->task->calls;
  struct sgl_call *call = NULL;
  
  for (struct sgl_ls *cl = cs->prev; cl != cs; cl = cl->prev) {
    struct sgl_call *c = sgl_baseof(struct sgl_call, ls, cl);

    if (c->start_pc) {
      call = c;
      break;
    }
  }
  
  if (!call) {
    sgl_error(sgl, pos, sgl_strdup("No call in progress"));
    return sgl->end_pc;
  }
  
  sgl_state_restore(&call->state, sgl);
  struct sgl_func *func = op->as_recall.func;

  if (func) {
    struct sgl_fimp *fimp = sgl_func_dispatch(func, sgl, sgl_stack(sgl));
    
    if (!fimp) {
      sgl_error(sgl, pos, sgl_sprintf("Func not applicable: %s", func->def.id->id));
      return sgl->end_pc;
    }

    struct sgl_imp *i = &fimp->imp;
    if (!sgl_fimp_compile(fimp, sgl, pos)) { return sgl->end_pc; }
    sgl_call_init(call, sgl, pos, i->recalls ? i->start_pc : NULL, call->return_pc);
    return i->start_pc->next;
  }
  
  return call->start_pc->next;
}

struct sgl_op *sgl_op_recall_new(struct sgl *sgl,
                                 struct sgl_form *form,
                                 struct sgl_func *func) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_RECALL);
  op->run = recall_run;
  op->as_recall.func = func;
  return op;
}

static struct sgl_op *return_cinit(struct sgl_op *op,
                                   struct sgl *sgl,
                                   struct sgl_cemit *out) {
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_return_new(sgl, f%" SGL_INT ");",
                 op->cemit_pc, form_id);
  
  return op->next;
}

static struct sgl_op *return_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_call *call = sgl_call(sgl);
  
  if (!call) {
    sgl_error(sgl, &op->form->pos, sgl_strdup("No calls in progress"));
    return sgl->end_pc;
  }
  
  struct sgl_op *return_pc = call->return_pc->next;
  sgl_call_free(call, sgl);
  return return_pc;
}

struct sgl_op *sgl_op_return_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_RETURN);
  op->run = return_run;
  return op;
}

static bool rotl_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_rotl(t, sgl);
  return false;
}

static struct sgl_op *rotl_run(struct sgl_op *op, struct sgl *sgl) {
  return sgl_stack_rotl(sgl_stack(sgl), sgl, false) ? op->next : sgl->end_pc;
}

struct sgl_op *sgl_op_rotl_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_ROTL);
  op->run = rotl_run;
  return op;
}

static bool rotr_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_rotr(t, sgl);
  return false;
}

static struct sgl_op *rotr_run(struct sgl_op *op, struct sgl *sgl) {
  return sgl_stack_rotr(sgl_stack(sgl), sgl, false) ? op->next : sgl->end_pc;
}

struct sgl_op *sgl_op_rotr_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_ROTR);
  op->run = rotr_run;
  return op;
}

static void scope_beg_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_scope_beg *sb = &op->as_scope_beg;  
  struct sgl_scope *ps = sb->parent;
  if (ps) { sgl_scope_deref(ps, sgl); }
}

static struct sgl_op *scope_beg_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_scope_beg *sb = &op->as_scope_beg;
  struct sgl_scope *ps = sb->parent;
  sgl_beg_scope(sgl, ps ? ps : sgl_scope(sgl));
  return op->next;
}

struct sgl_op *sgl_op_scope_beg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_SCOPE_BEG);
  op->run = scope_beg_run;
  op->as_scope_beg = (struct sgl_op_scope_beg){.parent = NULL};
  return op;
}

static struct sgl_op *scope_end_run(struct sgl_op *op, struct sgl *sgl) {
  sgl_end_scope(sgl);
  return op->next;
}

struct sgl_op *sgl_op_scope_end_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_SCOPE_END);
  op->run = scope_end_run;
  return op;
}

static void stack_switch_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_stack_switch *ss = &op->as_stack_switch;
  sgl_buf_printf(out, " n: %" SGL_INT " %s", ss->n, ss->pop ? "pop" : "");
}

static struct sgl_op *stack_switch_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_stack_switch *ss = &op->as_stack_switch;
  struct sgl_val *v = ss->pop ? sgl_peek(sgl) : NULL;
  struct sgl_list *s = sgl_switch_stack(sgl, op->as_stack_switch.n);
  
  if (!s) {
    if (v) {
      sgl_ls_delete(&v->ls);
      sgl_val_free(v, sgl);
    }
    
    return sgl->end_pc;
  }

  if (v) {
    sgl_ls_delete(&v->ls);
    sgl_ls_push(&s->root, &v->ls);
  }
  
  return op->next;
}

struct sgl_op *sgl_op_stack_switch_new(struct sgl *sgl,
                                       struct sgl_form *form,
                                       sgl_int_t n,
                                       bool pop) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_STACK_SWITCH);
  op->run = stack_switch_run;
  op->as_stack_switch = (struct sgl_op_stack_switch){.n = n, .pop = pop};
  return op;
}

static bool str_beg_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op *start_pc = op;
  bool changed = false;
  sgl_int_t depth = 1;
  
  for (op = start_pc->next; depth && op != sgl->end_pc; op = op->next) {
    changed |= op->type != &SGL_OP_STR_END && sgl_op_trace(op, sgl, t);

    if (op->type == &SGL_OP_STR_BEG) {
      depth++;
    } else if (op->type == &SGL_OP_STR_END) {
      depth--;
    } else if ((op->type == &SGL_OP_STR_PUT && !op->as_str_put.val) ||
               !sgl_op_is_const(op)) {
      break;
    }
  }

  sgl_trace_clear(t, sgl);
  if (depth) { return changed; }
  struct sgl_op *end_pc = op->prev;
  struct sgl_buf buf;
  sgl_buf_init(&buf);
  
  for (op = start_pc->next; op != end_pc; op = op->next) {
    if (op->type == &SGL_OP_STR_PUT) {
      struct sgl_val *v = op->as_str_put.val;
      assert(v);
      sgl_val_print(v, sgl, &op->form->pos, &buf);
      sgl_nop(op, sgl);
    }
  }
  
  bool is_short = buf.len < SGL_MAX_STR_LEN;

  struct sgl_str *s =
    sgl_str_new(sgl, is_short ? sgl_buf_cs(&buf) : NULL, buf.len);
  
  if (!is_short) {
    char *d = (char *)buf.data;
    d[buf.len] = 0;
    s->as_long = d;
    buf.data = NULL;
  }
  
  struct sgl_val *v = sgl_val_new(sgl, sgl->Str, NULL);
  v->as_str = s;
  sgl_op_push_reuse(start_pc, sgl, v);
  sgl_nop(end_pc, sgl);
  sgl_buf_deinit(&buf);
  return true;
}

static struct sgl_op *str_beg_run(struct sgl_op *op, struct sgl *sgl) {
  sgl_push_reg(sgl, sgl->Buf)->as_buf = sgl_buf_new(sgl);
  return op->next;
}

struct sgl_op *sgl_op_str_beg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_STR_BEG);
  op->run = str_beg_run;
  return op;
}

static bool str_end_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_push_undef(t, sgl, sgl->Str);
  return false;
}

static struct sgl_op *str_end_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = sgl_pop_reg(sgl);
  struct sgl_buf *b = v->as_buf;
  bool is_short = b->len < SGL_MAX_STR_LEN;
  struct sgl_str *s = sgl_str_new(sgl, is_short ? sgl_buf_cs(b) : NULL, b->len);

  if (!is_short) {
    s->as_long = (char *)b->data;
    b->data = NULL;
  }

  sgl_val_init(v, sgl, sgl->Str, sgl_stack(sgl))->as_str = s;
  sgl_buf_deref(b, sgl);
  return op->next;
}

struct sgl_op *sgl_op_str_end_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_STR_END);
  op->run = str_end_run;
  return op;
}

static bool str_put_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  if (op->as_str_put.val) { return false; }
  struct sgl_val *v = sgl_trace_pop(t, sgl);
  if (!v) { return false; }
  op->as_str_put.val = v;
  assert(v->op);
  sgl_op_deval(v->op, sgl);
  return true;
}

static void str_put_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *val = op->as_str_put.val;
  if (val) { sgl_val_free(val, sgl); }
}

static struct sgl_op *str_put_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_str_put *sp = &op->as_str_put;
  struct sgl_val *v = sp->val;
  if (!v && !(v = sgl_pop(sgl))) { return sgl->end_pc; }
  sgl_val_print(v, sgl, &op->form->pos, sgl_peek_reg(sgl)->as_buf);
  if (!sp->val) { sgl_val_free(v, sgl); }
  return op->next;
}

struct sgl_op *sgl_op_str_put_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_STR_PUT);
  op->run = str_put_run;
  op->as_str_put.val = NULL;
  return op;
}

static struct sgl_op *swap_cinit(struct sgl_op *op,
                                 struct sgl *sgl,
                                 struct sgl_cemit *out) {
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_swap_new(sgl, f%" SGL_INT ");",
                 op->cemit_pc, form_id);
  
  return op->next;
}

static bool swap_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_swap(t, sgl);
  return false;
}

static struct sgl_op *swap_run(struct sgl_op *op, struct sgl *sgl) {
  return sgl_stack_swap(sgl_stack(sgl), sgl, false) ? op->next : sgl->end_pc;
}

struct sgl_op *sgl_op_swap_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_SWAP);
  op->run = swap_run;
  return op;
}

static void task_beg_dump(struct sgl_op *op, struct sgl_buf *out) {
  sgl_buf_printf(out, " end: %" SGL_INT, op->as_task_beg.end_pc->pc);
}

static bool task_beg_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  bool changed = false;
  
  if (t->len) {
    for (op = op->as_task_beg.end_pc->next;
         t->len && op != sgl->end_pc;
         op = op->next) { changed |= sgl_op_trace(op, sgl, t); }

    sgl_trace_clear(t, sgl);
  }

  return changed;
}

static struct sgl_op *task_beg_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op *end_pc = op->as_task_beg.end_pc;
  struct sgl_op *next = op->next;
    
  if (next->type == &SGL_OP_SCOPE_BEG) {
    op->as_task_beg.is_scope = true;
    sgl_nop(next, sgl);
  }
  
  struct sgl_scope *ps = op->as_task_beg.is_scope ? sgl_scope(sgl) : NULL;
  sgl_task_new(sgl, sgl_lib(sgl), ps, op, end_pc);
  return end_pc->next;
}

struct sgl_op *sgl_op_task_beg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_TASK_BEG);
  op->run = task_beg_run;
  op->as_task_beg = (struct sgl_op_task_beg){.end_pc = NULL, .is_scope = false};
  return op;
}

static struct sgl_op *task_end_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_task *t = sgl->task;
  sgl_ls_delete(&t->ls);
  sgl->ntasks--;
  struct sgl_op *pc = sgl_yield(sgl);
  sgl_task_free(t, sgl);
  return pc->next;
}

struct sgl_op *sgl_op_task_end_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_TASK_END);
  op->run = task_end_run;
  return op;
}

static void throw_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_val *v = op->as_throw.val;

  if (v) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(v, out);
  }
}

static bool throw_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  bool changed = false;
  
  if (!op->as_throw.val) {
    struct sgl_val *v = op->as_throw.val = sgl_trace_pop(t, sgl);

    if (v) {
      assert(v->op);
      sgl_op_deval(v->op, sgl);
      changed = true;
    }
  }

  sgl_trace_clear(t, sgl);
  return changed;
}

static void throw_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = op->as_throw.val;
  if (v) { sgl_val_free(v, sgl); }
}

static struct sgl_op *throw_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = op->as_throw.val;
  
  return sgl_throw(sgl, &op->form->pos, NULL,
                   v ? sgl_val_dup(v, sgl, NULL) : sgl_pop(sgl));
}

struct sgl_op *sgl_op_throw_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_THROW);
  op->run = throw_run;
  op->as_throw.val = NULL;
  return op;
}

static struct sgl_op *times_cinit(struct sgl_op *op,
                                  struct sgl *sgl,
                                  struct sgl_cemit *out) {
  sgl_int_t form_id = op->form->cemit_id;
  struct sgl_val *reps = op->as_times.reps;
  if (reps) { sgl_val_cemit(reps, sgl, &op->form->pos, "v", NULL, out); }
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "                
                 "sgl_op_times_new(sgl, f%" SGL_INT ", %s);",
                 op->cemit_pc, form_id, reps ? "v" : "NULL");
  
  return op->next;
}

static void times_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_val *reps = op->as_times.reps;
  if (reps) { sgl_buf_printf(out, " %" SGL_INT, reps->as_int); }
}

static bool times_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op *start_op = op;
  struct sgl_val *v = start_op->as_times.reps ? NULL : sgl_trace_pop(t, sgl);
  bool changed = false;

  if (t->len) {
    struct sgl_trace *t_iter = sgl_trace_new(sgl);
    sgl_trace_copy(t, sgl, t_iter);
    bool ok = false;
    
    for (op = start_op->next;
         (!ok || t_iter->len) && op != sgl->end_pc;
         op = op->next) {
      if (op->type == &SGL_OP_COUNT && op->as_count.pc == start_op) {
        sgl_trace_sync(t, sgl, t_iter);
        sgl_trace_undef(t_iter, sgl, t_iter->len);
        ok = true;
        continue;
      }
      
      changed |= sgl_op_trace(op, sgl, t_iter);
    }

    sgl_trace_free(t_iter, sgl);
  }

  if (start_op->as_times.reps) { return changed; }
  
  if (v) {
    if (v->type != sgl->Int) {
      sgl_error(sgl, &start_op->form->pos,
                sgl_sprintf("Invalid reps: %s", v->type->def.id->id));
      
      return false;
    }

    start_op->as_times.reps = v;
    assert(v->op);
    sgl_op_deval(v->op, sgl);
  }
  
  return true;
}

static struct sgl_op *times_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_times *t = &op->as_times;
  struct sgl_val *reps = t->reps ? sgl_val_dup(t->reps, sgl, NULL) : NULL;

  if (!reps) {
    reps = sgl_pop(sgl);
    if (!reps) { return sgl->end_pc; }

    if (reps->type != sgl->Int) {
      sgl_error(sgl, &op->form->pos,
                sgl_sprintf("Invalid reps: %s", reps->type->def.id->id));
      
      return sgl->end_pc;
    }
  }

  sgl_ls_push(sgl_reg_stack(sgl), &reps->ls);
  return op->next;
}

struct sgl_op *sgl_op_times_new(struct sgl *sgl,
                                struct sgl_form *form,
                                struct sgl_val *reps) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_TIMES);
  op->run = times_run;
  op->as_times.reps = reps;
  return op;
}

static bool try_beg_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  return false;
}

static struct sgl_op *try_beg_run(struct sgl_op *op, struct sgl *sgl) {
  sgl_try_new(sgl, op->as_try_beg.end_pc);
  return op->next;
}

struct sgl_op *sgl_op_try_beg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_TRY_BEG);
  op->run = try_beg_run;
  op->as_try_beg.end_pc = NULL;
  return op;
}

static bool try_end_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_push_undef(t, sgl, sgl->Error);
  return false;
}

static struct sgl_op *try_end_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_ls *found = sgl_ls_pop(&sgl->task->tries);
  assert(found);
  struct sgl_try *try = sgl_baseof(struct sgl_try, ls, found);
  sgl_try_free(try, sgl);
  return op->next;
}

struct sgl_op *sgl_op_try_end_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_TRY_END);
  op->run = try_end_run;
  return op;
}

static bool type_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_val *v = sgl_trace_pop(t, sgl);
  if (!v) { return false; }
  struct sgl_type *vt = v->type;
  sgl_op_push_reuse(v->op, sgl, sgl_val_reuse(v, sgl, sgl_type_meta(vt, sgl), true));
  v->as_type = vt;
  sgl_nop(op, sgl);
  return true;
}

static struct sgl_op *type_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = sgl_peek(sgl);

  if (!v) {
    sgl_error(sgl, &op->form->pos, sgl_strdup("Missing value"));
    return sgl->end_pc;
  }

  struct sgl_type *t = v->type;
  sgl_val_reuse(v, sgl, sgl_type_meta(t, sgl), true)->as_type = t;
  return op->next;
}

struct sgl_op *sgl_op_type_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_TYPE);
  op->run = type_run;
  return op;
}

static bool yield_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  return false;
}

static struct sgl_op *yield_run(struct sgl_op *op, struct sgl *sgl) {
  return sgl_yield(sgl)->next;
}

struct sgl_op *sgl_op_yield_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_YIELD);
  op->run = yield_run;
  return op;
}

void sgl_setup_ops() {
  sgl_op_type_init(&SGL_NOP, "nop");
  SGL_NOP.cinit_op = nop_cinit;
  SGL_NOP.trace_op = nop_trace;
  
  sgl_op_type_init(&SGL_OP_BENCH, "bench");

  sgl_op_type_init(&SGL_OP_CALL, "call");
  SGL_OP_CALL.deinit_op = call_deinit;
  SGL_OP_CALL.dump_op = call_dump;
  SGL_OP_CALL.trace_op = call_trace;

  sgl_op_type_init(&SGL_OP_COUNT, "count");
  SGL_OP_COUNT.cinit_op = count_cinit;
  SGL_OP_COUNT.trace_op = count_trace;
  
  sgl_op_type_init(&SGL_OP_DISPATCH, "dispatch");
  SGL_OP_DISPATCH.cinit_op = dispatch_cinit;
  SGL_OP_DISPATCH.const_op = dispatch_const;
  SGL_OP_DISPATCH.dump_op = dispatch_dump;
  SGL_OP_DISPATCH.trace_op = dispatch_trace;
  
  sgl_op_type_init(&SGL_OP_DROP, "drop");
  SGL_OP_DROP.trace_op = drop_trace;

  sgl_op_type_init(&SGL_OP_DUP, "dup");
  SGL_OP_DUP.cinit_op = dup_cinit;
  SGL_OP_DUP.trace_op = dup_trace;

  sgl_op_type_init(&SGL_OP_EVAL_BEG, "eval-beg");
  SGL_OP_EVAL_BEG.dump_op = eval_beg_dump;
  SGL_OP_EVAL_BEG.trace_op = eval_beg_trace;

  sgl_op_type_init(&SGL_OP_EVAL_END, "eval-end");

  sgl_op_type_init(&SGL_OP_FIMP, "fimp");
  SGL_OP_FIMP.cinit_op = fimp_cinit;
  SGL_OP_FIMP.dump_op = fimp_dump;
  SGL_OP_FIMP.trace_op = fimp_trace;

  sgl_op_type_init(&SGL_OP_FIMP_CALL, "fimp-call");
  SGL_OP_FIMP_CALL.cinit_op = fimp_call_cinit;
  SGL_OP_FIMP_CALL.const_op = fimp_call_const;
  SGL_OP_FIMP_CALL.dump_op = fimp_call_dump;
  SGL_OP_FIMP_CALL.trace_op = fimp_call_trace;

  sgl_op_type_init(&SGL_OP_FOR, "for");

  sgl_op_type_init(&SGL_OP_GET_FIELD, "get-field");
  SGL_OP_GET_FIELD.dump_op = field_dump;
  SGL_OP_GET_FIELD.trace_op = get_field_trace;
  
  sgl_op_type_init(&SGL_OP_GET_VAR, "get-var");
  SGL_OP_GET_VAR.dump_op = get_var_dump;
  SGL_OP_GET_VAR.const_op = get_var_const;
  SGL_OP_GET_VAR.trace_op = get_var_trace;
  
  sgl_op_type_init(&SGL_OP_IDLE, "idle");

  sgl_op_type_init(&SGL_OP_IF, "if");
  SGL_OP_IF.dump_op = if_dump;
  SGL_OP_IF.trace_op = if_trace;

  sgl_op_type_init(&SGL_OP_ITER, "iter");
  SGL_OP_ITER.trace_op = iter_trace;
  
  sgl_op_type_init(&SGL_OP_JUMP, "jump");
  SGL_OP_JUMP.dump_op = jump_dump;
  SGL_OP_JUMP.trace_op = jump_trace;
  
  sgl_op_type_init(&SGL_OP_LET_VAR, "let-var");
  SGL_OP_LET_VAR.dump_op = let_var_dump;
  SGL_OP_LET_VAR.trace_op = let_var_trace;
  SGL_OP_LET_VAR.deinit_op = let_var_deinit;

  sgl_op_type_init(&SGL_OP_LAMBDA, "lambda");
  SGL_OP_LAMBDA.deval_op = lambda_deval;
  SGL_OP_LAMBDA.dump_op = lambda_dump;
  SGL_OP_LAMBDA.trace_op = lambda_trace;
  
  sgl_op_type_init(&SGL_OP_LIST_BEG, "list-beg");
  SGL_OP_LIST_BEG.trace_op = list_beg_trace;

  sgl_op_type_init(&SGL_OP_LIST_END, "list-end");
  SGL_OP_LIST_END.trace_op = list_end_trace;
  
  sgl_op_type_init(&SGL_OP_PAIR, "pair");
  SGL_OP_PAIR.trace_op = pair_trace;
  
  sgl_op_type_init(&SGL_OP_PUSH, "push");
  SGL_OP_PUSH.cinit_op = push_cinit;
  SGL_OP_PUSH.dump_op = push_dump;
  SGL_OP_PUSH.trace_op = push_trace;
  SGL_OP_PUSH.deinit_op = push_deinit;

  sgl_op_type_init(&SGL_OP_PUT_FIELD, "put-field");
  SGL_OP_PUT_FIELD.dump_op = field_dump;

  sgl_op_type_init(&SGL_OP_RECALL, "recall");

  sgl_op_type_init(&SGL_OP_RETURN, "return");
  SGL_OP_RETURN.cinit_op = return_cinit;
  
  sgl_op_type_init(&SGL_OP_ROTL, "rotl");
  SGL_OP_ROTL.trace_op = rotl_trace;
  
  sgl_op_type_init(&SGL_OP_ROTR, "rotr");
  SGL_OP_ROTR.trace_op = rotr_trace;
  
  sgl_op_type_init(&SGL_OP_SCOPE_BEG, "scope-beg");
  SGL_OP_SCOPE_BEG.deinit_op = scope_beg_deinit;
  SGL_OP_SCOPE_BEG.trace_op = nop_trace;
  
  sgl_op_type_init(&SGL_OP_SCOPE_END, "scope-end");
  SGL_OP_SCOPE_END.trace_op = nop_trace;
  
  sgl_op_type_init(&SGL_OP_STACK_SWITCH, "stack-switch");
  SGL_OP_STACK_SWITCH.dump_op = stack_switch_dump;
  
  sgl_op_type_init(&SGL_OP_STR_BEG, "str-beg");
  SGL_OP_STR_BEG.trace_op = str_beg_trace;
  
  sgl_op_type_init(&SGL_OP_STR_END, "str-end");
  SGL_OP_STR_END.trace_op = str_end_trace;
  
  sgl_op_type_init(&SGL_OP_STR_PUT, "str-put");
  SGL_OP_STR_PUT.trace_op = str_put_trace;
  SGL_OP_STR_PUT.deinit_op = str_put_deinit;
  
  sgl_op_type_init(&SGL_OP_SWAP, "swap");
  SGL_OP_SWAP.cinit_op = swap_cinit;
  SGL_OP_SWAP.trace_op = swap_trace;
  
  sgl_op_type_init(&SGL_OP_TASK_BEG, "task-beg");
  SGL_OP_TASK_BEG.dump_op = task_beg_dump;
  SGL_OP_TASK_BEG.trace_op = task_beg_trace;

  sgl_op_type_init(&SGL_OP_TASK_END, "task-end");

  sgl_op_type_init(&SGL_OP_THROW, "throw");
  SGL_OP_THROW.dump_op = throw_dump;
  SGL_OP_THROW.trace_op = throw_trace;
  SGL_OP_THROW.deinit_op = throw_deinit;
  
  sgl_op_type_init(&SGL_OP_TIMES, "times");
  SGL_OP_TIMES.cinit_op = times_cinit;
  SGL_OP_TIMES.dump_op = times_dump;
  SGL_OP_TIMES.trace_op = times_trace;
  
  sgl_op_type_init(&SGL_OP_TRY_BEG, "try-beg");
  SGL_OP_TRY_BEG.trace_op = try_beg_trace;

  sgl_op_type_init(&SGL_OP_TRY_END, "try-end");
  SGL_OP_TRY_END.trace_op = try_end_trace;
  
  sgl_op_type_init(&SGL_OP_TYPE, "type");
  SGL_OP_TYPE.trace_op = type_trace;

  sgl_op_type_init(&SGL_OP_YIELD, "yield");
  SGL_OP_YIELD.trace_op = yield_trace;
}
