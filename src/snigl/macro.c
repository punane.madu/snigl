#include <stdlib.h>

#include "snigl/form.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/macro.h"

struct sgl_def_type SGL_DEF_MACRO;

struct sgl_macro *sgl_macro_init(struct sgl_macro *macro,
                                 struct sgl *sgl,
                                 struct sgl_pos *pos,
                                 struct sgl_lib *lib,
                                 struct sgl_sym *id,
                                 sgl_int_t nargs,
                                 sgl_macro_imp_t imp) {
  sgl_def_init(&macro->def, sgl, pos, lib, &SGL_DEF_MACRO, id);
  macro->nargs = nargs;
  macro->imp = imp;
  macro->deinit = NULL;
  sgl_ls_init(&macro->data);
  return macro;
}

struct sgl_macro *sgl_macro_new(struct sgl *sgl,
                                struct sgl_pos *pos,
                                struct sgl_lib *lib,
                                struct sgl_sym *id,
                                sgl_int_t nargs,
                                sgl_macro_imp_t imp) {
  return sgl_macro_init(sgl_malloc(&sgl->macro_pool), sgl, pos, lib, id, nargs, imp);
}

struct sgl_ls *sgl_macro_call(struct sgl_macro *m,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  if (m->nargs) {
    struct sgl_ls *f = &form->ls;
    for (sgl_int_t i = 0; i < m->nargs && f != root; i++, f = f->next);

    if (f == root) {
      sgl_error(sgl, &form->pos,
                sgl_sprintf("Macro not applicable: %s", m->def.id->id));

      return NULL;
    }
  }

  return m->imp(m, form, root, sgl);
}

static void free_def(struct sgl_def *def, struct sgl *sgl) {
  struct sgl_macro *m = sgl_baseof(struct sgl_macro, def, def);
  if (m->deinit) { m->deinit(m, sgl); }
  sgl_free(&sgl->macro_pool, m);
}

void sgl_setup_macros() {
  sgl_def_type_init(&SGL_DEF_MACRO, "macro");
  SGL_DEF_MACRO.free_def = free_def;
}
