#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "snigl/buf.h"
#include "snigl/error.h"
#include "snigl/file.h"
#include "snigl/form.h"
#include "snigl/io.h"
#include "snigl/iters/filter.h"
#include "snigl/iters/int.h"
#include "snigl/iters/map.h"
#include "snigl/lib.h"
#include "snigl/libs/abc.h"
#include "snigl/macro.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/str.h"
#include "snigl/struct.h"
#include "snigl/sym.h"
#include "snigl/types/bool.h"
#include "snigl/types/buf.h"
#include "snigl/types/char.h"
#include "snigl/types/error.h"
#include "snigl/types/file.h"
#include "snigl/types/fix.h"
#include "snigl/types/func.h"
#include "snigl/types/int.h"
#include "snigl/types/iter.h"
#include "snigl/types/lambda.h"
#include "snigl/types/list.h"
#include "snigl/types/meta.h"
#include "snigl/types/nil.h"
#include "snigl/types/pair.h"
#include "snigl/types/str.h"
#include "snigl/types/struct.h"
#include "snigl/types/sym.h"
#include "snigl/types/undef.h"

static struct sgl_ls *pair_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {  
  struct sgl_form *last = sgl_baseof(struct sgl_form, ls, form->ls.next);
  if (!sgl_form_compile(last, sgl)) { return NULL; }
  sgl_op_pair_new(sgl, form);
  return last->ls.next;
}

static struct sgl_ls *bench_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  struct sgl_op *op = sgl_op_bench_new(sgl, form);
  struct sgl_ls *arg = form->ls.next;
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, arg);
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  op->as_bench.end_pc = sgl_op_count_new(sgl, form, op);
  return arg->next;
}

static struct sgl_ls *debug_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  sgl->debug = true;
  return form->ls.next;
}

static struct sgl_ls *drop_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  sgl_op_drop_new(sgl, form, 1);
  return form->ls.next;
}

static struct sgl_ls *drop2_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  sgl_op_drop_new(sgl, form, 2);
  return form->ls.next;
}

static struct sgl_ls *dup_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  sgl_op_dup_new(sgl, form);
  return form->ls.next;
}

static struct sgl_ls *for_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  struct sgl_op *op = sgl_op_for_new(sgl, form, NULL);
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_op *start_pc = sgl->end_pc->prev;
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  sgl_op_iter_new(sgl, form, start_pc);
  op->as_for.end_pc = sgl->end_pc->prev;
  return body->ls.next;
}

static struct sgl_ls *func_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  struct sgl_form *id_form = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_form *args_form = sgl_baseof(struct sgl_form, ls, id_form->ls.next);
  
  struct sgl_list *arg_list = sgl_form_eval(args_form, sgl);
  if (!arg_list) { return NULL; }
  sgl_int_t nargs = sgl_list_len(arg_list);
  struct sgl_arg args[nargs];
  struct sgl_ls *vl = arg_list->root.next;
  
  for (struct sgl_arg *a = args;
       a < args + nargs && vl != &arg_list->root;
       a++, vl = vl->next) {
    struct sgl_val *v = sgl_baseof(struct sgl_val, ls, vl);

    if (sgl_derived(v->type, sgl->Meta)) {
      sgl_arg_init(a, v->as_type, NULL);
    } else {
      sgl_arg_init(a, NULL, v);
    }
  }

  sgl_ls_init(&arg_list->root);
  sgl_list_deref(arg_list, sgl);
  struct sgl_fimp *fimp = _sgl_lib_add_func(sgl_lib(sgl), sgl, &form->pos,
                                            id_form->as_id.val,
                                            nargs, args, NULL);

  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, args_form->ls.next);  
  sgl_imp_init(&fimp->imp, body);
  if (!sgl_fimp_compile(fimp, sgl, &form->pos)) { return NULL; }
  return body->ls.next;
}

static struct sgl_ls *idle_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  struct sgl_op *op = sgl_op_idle_new(sgl, form);
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  sgl_op_yield_new(sgl, form);
  op->as_idle.end_pc = sgl_op_jump_new(sgl, form, op->prev);
  return body->ls.next;
}

static struct sgl_ls *if_imp(struct sgl_macro *macro,
                             struct sgl_form *form,
                             struct sgl_ls *root,
                             struct sgl *sgl) {
  struct sgl_op *else_skip = sgl_op_if_new(sgl, form, NULL);
  else_skip->as_if.neg = true;
  struct sgl_ls *arg = form->ls.next;
  struct sgl_form *lhs = sgl_baseof(struct sgl_form, ls, arg);
  if (!sgl_form_compile(lhs, sgl)) { return NULL; }
  struct sgl_op *if_skip = sgl_op_jump_new(sgl, form, NULL);
  arg = arg->next;
  struct sgl_form *rhs = sgl_baseof(struct sgl_form, ls, arg);
  else_skip->as_if.pc = sgl->end_pc->prev;
  if (!sgl_form_compile(rhs, sgl)) { return NULL; }
  if_skip->as_jump.pc = sgl->end_pc->prev;
  return arg->next;
}

static struct sgl_ls *let_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  struct sgl_form *place = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_list *ids = sgl_form_eval(place, sgl);
  if (!ids) { return NULL; }

  for (struct sgl_ls *ls = ids->root.prev; ls != &ids->root; ls = ls->prev) {
    struct sgl_val *idv = sgl_baseof(struct sgl_val, ls, ls), *val = NULL;
    struct sgl_sym *id = NULL;

    if (idv->type == sgl->Pair) {
      val = sgl_val_dup(idv->as_pair.last, sgl, NULL);
      idv = idv->as_pair.first;
    }
    
    if (idv->type == sgl->Sym) {
      id = idv->as_sym;
    } else {
      sgl_error(sgl, &form->pos,
                sgl_sprintf("Invalid let place: %s", idv->type->def.id->id));
      
      return NULL;
    }
    
    sgl_op_let_var_new(sgl, form, id)->as_let_var.val = val;
  }
  
  sgl_list_deref(ids, sgl);
  return place->ls.next;
}

static struct sgl_ls *link_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_list *out = sgl_form_eval(body, sgl);
  if (!out) { return NULL; }
  
  sgl_ls_do(&out->root, struct sgl_val, ls, v) {
    if (v->type != sgl->Str) {
      sgl_error(sgl, pos,
                sgl_sprintf("Invalid link argument: %s", sgl_type_id(v->type)->id));

      sgl_list_deref(out, sgl);
      return NULL;
    }
    
    if (!sgl_link(sgl, pos, sgl_str_cs(v->as_str))) {
      sgl_list_deref(out, sgl);
      return NULL;
    }
  }
  
  sgl_list_deref(out, sgl);
  return body->ls.next;
}

static struct sgl_ls *or_imp(struct sgl_macro *macro,
                             struct sgl_form *form,
                             struct sgl_ls *root,
                             struct sgl *sgl) {
  struct sgl_form *rhs = sgl_baseof(struct sgl_form, ls, form->ls.next);
  sgl_op_dup_new(sgl, form);
  struct sgl_op *skip = sgl_op_if_new(sgl, form, NULL);
  sgl_op_drop_new(sgl, form, 1);
  if (!sgl_form_compile(rhs, sgl)) { return NULL; }
  skip->as_if.pc = sgl->end_pc->prev;
  return rhs->ls.next;
}

static struct sgl_ls *pre_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_list *out = sgl_form_eval(body, sgl);
  if (!out) { return NULL; }
  sgl_ls_do(&out->root, struct sgl_val, ls, v) { sgl_op_push_new(sgl, form, v); }
  sgl_ls_init(&out->root);
  sgl_list_deref(out, sgl);
  return body->ls.next;
}

static struct sgl_ls *put_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_form *place = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_list *ids = sgl_form_eval(place, sgl);
  if (!ids) { return NULL; }

  for (struct sgl_ls *ls = ids->root.prev; ls != &ids->root; ls = ls->prev) {
    struct sgl_val *idv = sgl_baseof(struct sgl_val, ls, ls), *val = NULL;

    if (idv->type == sgl->Pair) {
      val = sgl_val_dup(idv->as_pair.last, sgl, NULL);
      idv = idv->as_pair.first;
    }
    
    if (idv->type == sgl->Sym && idv->as_sym->id[0] == '.') {
      struct sgl_sym *id = idv->as_sym;

      if (ls->prev == &ids->root) {
        if (!val) { sgl_op_swap_new(sgl, form); }
      } else {
        sgl_op_dup_new(sgl, form);
        if (!val) { sgl_op_rotr_new(sgl, form); }
      }

      sgl_op_put_field_new(sgl, form, sgl_sym(sgl, id->id+1), val);
    } else {
      if (ls->prev != &ids->root) {
        sgl_op_dup_new(sgl, form);
        if (!val) { sgl_op_rotr_new(sgl, form); }
        sgl_op_push_new(sgl, form, sgl_val_dup(idv, sgl, NULL));

        if (val) {
          sgl_op_push_new(sgl, form, sgl_val_dup(val, sgl, NULL));
        } else {
          sgl_op_swap_new(sgl, form);
        }
      }
      
      struct sgl_func *func = sgl_find_func(sgl, pos, sgl_sym(sgl, "put"));
      assert(func);
      sgl_op_dispatch_new(sgl, form, func, NULL);
    }
  }
  
  sgl_list_deref(ids, sgl);
  return place->ls.next;
}

static struct sgl_ls *recall_arg_imp(struct sgl_macro *macro,
                                     struct sgl_form *form,
                                     struct sgl_ls *root,
                                     struct sgl *sgl) {
  struct sgl_ls *arg = form->ls.next;
  struct sgl_form *id_form = sgl_baseof(struct sgl_form, ls, arg);

  if (id_form->type != &SGL_FORM_ID) {
    sgl_error(sgl, &form->pos,
              sgl_sprintf("Invalid recall arg: %s", id_form->type->id));
    
    return NULL;
  }
  
  struct sgl_sym *id = id_form->as_id.val;
  struct sgl_func *func = sgl_find_func(sgl, &form->pos, id);

  if (!func) {
    sgl_error(sgl, &form->pos, sgl_sprintf("Unknown func: %s", id->id));
    return NULL;    
  }
  
  sgl_op_recall_new(sgl, form, func);
  return form->ls.next;
}

static struct sgl_ls *recall_imp(struct sgl_macro *macro,
                                 struct sgl_form *form,
                                 struct sgl_ls *root,
                                 struct sgl *sgl) {
  sgl_op_recall_new(sgl, form, NULL);
  return form->ls.next;
}

static struct sgl_ls *rotl_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  sgl_op_rotl_new(sgl, form);
  return form->ls.next;
}

static struct sgl_ls *rotr_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  sgl_op_rotr_new(sgl, form);
  return form->ls.next;
}

static struct sgl_ls *struct_imp(struct sgl_macro *macro,
                                 struct sgl_form *form,
                                 struct sgl_ls *root,
                                 struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  
  struct sgl_form
    *id_form = sgl_baseof(struct sgl_form, ls, form->ls.next),
    *parent_form = sgl_baseof(struct sgl_form, ls, id_form->ls.next),
    *field_form = sgl_baseof(struct sgl_form, ls, parent_form->ls.next);

  if (id_form->type != &SGL_FORM_ID) {
    sgl_error(sgl, pos, sgl_sprintf("Invalid struct id: %s", id_form->type->id));
    return NULL;
  }

  struct sgl_list *parent_list = sgl_form_eval(parent_form, sgl);
  if (!parent_list) { return NULL; }
  sgl_int_t nparents = sgl_list_len(parent_list);
  struct sgl_type *parents[nparents+2];
  parents[0] = sgl->Struct;
  parents[nparents+1] = NULL;
  struct sgl_ls *p = parent_list->root.next;
  
  for (sgl_int_t i = 1; i < nparents+1; i++, p = p->next) {
    struct sgl_val *v = sgl_baseof(struct sgl_val, ls, p);
    struct sgl_type *vt = v->as_type;
    
    if (!vt->is_trait && !sgl_derived(vt, sgl->Struct)) {
      sgl_error(sgl, pos, sgl_sprintf("Invalid struct parent: %s", vt->def.id->id));
      sgl_list_deref(parent_list, sgl);
      return NULL;
    }
    
    parents[i] = vt;
  }

  sgl_list_deref(parent_list, sgl);
  struct sgl_list *field_list = sgl_form_eval(field_form, sgl);
  if (!field_list) { return NULL; }  

  struct sgl_type *t = sgl_struct_type_new(sgl, pos,
                                           sgl_lib(sgl),
                                           id_form->as_id.val,
                                           parents);

  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, t);
  struct sgl_struct_type *st = sgl_baseof(struct sgl_struct_type, ref_type, rt);
  
  struct sgl_vec fids;
  sgl_vec_init(&fids, sizeof(struct sgl_sym *));
  struct sgl_ls *fr = &field_list->root;
  bool ok = false;
  
  for (struct sgl_ls *f = fr->next; f != fr; f = f->next) {
    struct sgl_val *fv = sgl_baseof(struct sgl_val, ls, f);

    if (fv->type == sgl->Sym) {
      *(struct sgl_sym **)sgl_vec_push(&fids) = fv->as_sym;
    } else if (sgl_derived(fv->type, sgl->Meta)) {
      if (!fids.len) {
        sgl_error(sgl, pos,
                  sgl_sprintf("Missing field name: %s", fv->type->def.id->id));

        goto exit;
      }

      sgl_vec_do(&fids, struct sgl_sym *, fid) {
        if (!sgl_struct_type_add(st, sgl, pos, *fid, fv->as_type)) { goto exit; }
      }

      fids.len = 0;
    }
  }

  sgl_vec_do(&fids, struct sgl_sym *, fid) {
    if (!sgl_struct_type_add(st, sgl, pos, *fid, sgl->A)) { goto exit; }
  }

  ok = true;
 exit:
  sgl_vec_deinit(&fids);
  sgl_list_deref(field_list, sgl);
  return ok ? field_form->ls.next : NULL;
}

static struct sgl_ls *task_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  struct sgl_ls *arg = form->ls.next;
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, arg);
  struct sgl_op *op = sgl_op_task_beg_new(sgl, form);
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  op->as_task_beg.end_pc = sgl_op_task_end_new(sgl, form);
  return arg->next;
}

static struct sgl_ls *throw_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  sgl_op_throw_new(sgl, form);
  return form->ls.next;
}

static struct sgl_ls *times_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  sgl_op_times_new(sgl, form, NULL);
  struct sgl_ls *arg = form->ls.next;
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, arg);
  struct sgl_op *start_pc = sgl->end_pc->prev;
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  sgl_op_count_new(sgl, form, start_pc);
  return arg->next;
}

static struct sgl_ls *try_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  struct sgl_op *op = sgl_op_try_beg_new(sgl, form);
  struct sgl_ls *arg = form->ls.next;
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, arg);
  if (!sgl_form_compile(body, sgl)) { return NULL; }  
  sgl_op_try_end_new(sgl, form);

  op->as_try_beg.end_pc =
    sgl_op_push_new(sgl, form, sgl_val_dup(&sgl->nil, sgl, NULL));
  
  return arg->next;
}

static struct sgl_ls *swap_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  sgl_op_swap_new(sgl, form);
  return form->ls.next;
}

static struct sgl_ls *type_get_imp(struct sgl_macro *macro,
                                   struct sgl_form *form,
                                   struct sgl_ls *root,
                                   struct sgl *sgl) {
  sgl_op_type_new(sgl, form);
  return form->ls.next;
}

bool type_conv_imp(struct sgl_fimp *fimp,
                   struct sgl *sgl,
                   struct sgl_op **return_pc) {
  sgl_val_reuse(sgl_peek(sgl), sgl, fimp->data, false);
  return true;
}

static struct sgl_ls *type_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_lib *lib = sgl_lib(sgl);
  struct sgl_form *id_form = sgl_baseof(struct sgl_form, ls, form->ls.next);

  if (id_form->type != &SGL_FORM_ID) {
    sgl_error(sgl, pos, sgl_sprintf("Invalid type id: %s", id_form->type->id));
    return NULL;    
  }

  struct sgl_form *imp_form = sgl_baseof(struct sgl_form, ls, id_form->ls.next);
  struct sgl_list *imp_stack = sgl_form_eval(imp_form, sgl);
  if (!imp_stack) { return NULL; }

  if (imp_stack->root.prev == &imp_stack->root) {
    sgl_error(sgl, pos, sgl_strdup("Missing type imp"));
    return NULL;
  }

  if (imp_stack->root.prev->prev != &imp_stack->root) {
    sgl_error(sgl, pos, sgl_strdup("Invalid type imp"));
    return NULL;
  }
  
  struct sgl_val *imp_val =
    sgl_baseof(struct sgl_val, ls, sgl_ls_pop(&imp_stack->root));
  
  if (!sgl_derived(imp_val->type, sgl->Meta)) {
    sgl_error(sgl, pos,
              sgl_sprintf("Invalid type imp: %s", imp_val->type->def.id->id));
    
    return NULL;
  }
  
  struct sgl_type *imp = imp_val->as_type;
  sgl_list_deref(imp_stack, sgl);
  struct sgl_sym *id = id_form->as_id.val;
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, sgl_types(sgl->U, imp));
  
  struct sgl_fimp *f = sgl_lib_add_cfunc(lib, sgl, pos,
                                         sgl_sym_lcase(id, sgl),
                                         type_conv_imp,
                                         sgl_rets(t),
                                         sgl_arg(imp));
  
  if (!f) { return NULL; }
  f->data = t;
  return imp_form->ls.next;
}

static struct sgl_ls *use_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_form *src_form = sgl_baseof(struct sgl_form, ls, form->ls.next);

  if (src_form->type != &SGL_FORM_ID) {
    sgl_error(sgl, pos, sgl_sprintf("Invalid lib id: %s", src_form->type->id));
    return NULL;
  }
  
  struct sgl_sym *src_id = src_form->as_id.val;
  struct sgl_lib *src = sgl_find_lib(sgl, pos, src_id);

  if (!src) {
    sgl_error(sgl, pos, sgl_sprintf("Unknown lib: %s", src_id->id));
    return NULL;
  }
                                    
  struct sgl_form *dids_form = sgl_baseof(struct sgl_form, ls, src_form->ls.next);
  struct sgl_list *dids = sgl_form_eval(dids_form, sgl);
  if (!dids) { return NULL; }
  sgl_int_t ndids = sgl_list_len(dids);
  struct sgl_sym *dids_arg[ndids + 1];
  dids_arg[ndids] = NULL;
  struct sgl_sym **arg = dids_arg;
  
  sgl_ls_do(&dids->root, struct sgl_val, ls, v) {
    if (v->type != sgl->Sym) {
      sgl_error(sgl, pos,
                sgl_sprintf("Invalid def id: %s", sgl_type_id(v->type)->id));

      sgl_list_deref(dids, sgl);
      return NULL;
    }
    
    *arg++ = v->as_sym;
  }

  sgl_list_deref(dids, sgl);
  if (!_sgl_use(sgl, pos, src, dids_arg)) { return NULL; }
  return dids_form->ls.next;
}


static struct sgl_ls *while_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_op *start_pc = sgl->end_pc->prev;
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  sgl_op_if_new(sgl, form, start_pc);
  return body->ls.next;
}

static struct sgl_ls *yield_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  sgl_op_yield_new(sgl, form);
  return form->ls.next;
}

static bool dump_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_buf buf;
  sgl_buf_init(&buf);
  sgl_val_dump(v, &buf);
  sgl_buf_putc(&buf, '\n');
  fputs(sgl_buf_cs(&buf), stderr);
  sgl_buf_deinit(&buf);
  sgl_val_free(v, sgl);
  return true;
}

static bool type_sub_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *t = sgl_pop(sgl), *v = sgl_peek(sgl);
  struct sgl_type *vt = v->type;
  sgl_val_reuse(v, sgl, sgl->Bool, true)->as_bool = sgl_derived(vt, t->as_type);
  sgl_val_free(t, sgl);
  return true;
}

static bool sub_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *y = sgl_pop(sgl), *x = sgl_peek(sgl);
  struct sgl_type *xt = x->as_type;
  sgl_val_reuse(x, sgl, sgl->Bool, false)->as_bool = sgl_derived(xt, y->as_type);
  return true;
}

static bool clone_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  return sgl_val_clone(v, sgl, sgl_pos(sgl), &v->ls);
}

static bool clone_drop_imp(struct sgl_fimp *fimp,
                           struct sgl *sgl,
                           struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  bool ok = sgl_val_clone(v, sgl, sgl_pos(sgl), sgl_stack(sgl));
  sgl_val_free(v, sgl);
  return ok;
}

static bool call_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  *rpc = sgl_val_call(v, sgl, sgl_pos(sgl), sgl->pc, true);
  sgl_val_free(v, sgl);
  return sgl->errors.next == &sgl->errors;
}

static bool catch_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_error *e = v->as_error;
  sgl_val_dup(e->val, sgl, sgl_stack(sgl));
  sgl_val_free(v, sgl);
  return true;
}

static bool bool_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  sgl_push(sgl, sgl->Bool)->as_bool = sgl_val_bool(v);
  sgl_val_free(v, sgl);
  return true;
}

static bool not_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  sgl_push(sgl, sgl->Bool)->as_bool = !sgl_val_bool(v);
  sgl_val_free(v, sgl);
  return true;
}

static bool is_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {  
  struct sgl_val *rhs = sgl_pop(sgl), *lhs = sgl_pop(sgl);
  sgl_push(sgl, sgl->Bool)->as_bool = sgl_val_is(lhs, rhs);
  return true;
}

static bool nis_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {  
  struct sgl_val *rhs = sgl_pop(sgl), *lhs = sgl_pop(sgl);
  sgl_push(sgl, sgl->Bool)->as_bool = !sgl_val_is(lhs, rhs);
  return true;
}

static bool eq_imp(struct sgl_fimp *fimp,
                   struct sgl *sgl,
                   struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_peek(sgl);

  bool ok = sgl_val_eq(lhs, sgl, rhs);
  sgl_val_reuse(lhs, sgl, sgl->Bool, true)->as_bool = ok;
  sgl_val_free(rhs, sgl);
  return true;
}

static bool ne_imp(struct sgl_fimp *fimp,
                   struct sgl *sgl,
                   struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_peek(sgl);

  bool ok = !sgl_val_eq(lhs, sgl, rhs);
  sgl_val_reuse(lhs, sgl, sgl->Bool, true)->as_bool = ok;
  sgl_val_free(rhs, sgl);
  return true;
}

static bool lt_imp(struct sgl_fimp *fimp,
                   struct sgl *sgl,
                   struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_peek(sgl);

  bool ok = sgl_val_cmp(lhs, rhs) == SGL_LT;
  sgl_val_reuse(lhs, sgl, sgl->Bool, true)->as_bool = ok;
  sgl_val_free(rhs, sgl);
  return true;
}

static bool lte_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_peek(sgl);

  bool ok = sgl_val_cmp(lhs, rhs) < SGL_GT;
  sgl_val_reuse(lhs, sgl, sgl->Bool, true)->as_bool = ok;
  sgl_val_free(rhs, sgl);
  return true;
}

static bool gt_imp(struct sgl_fimp *fimp,
                   struct sgl *sgl,
                   struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_peek(sgl);

  bool ok = sgl_val_cmp(lhs, rhs) == SGL_GT;
  sgl_val_reuse(lhs, sgl, sgl->Bool, true)->as_bool = ok;
  sgl_val_free(rhs, sgl);
  return true;
}

static bool gte_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_peek(sgl);

  bool ok = sgl_val_cmp(lhs, rhs) > SGL_LT;
  sgl_val_reuse(lhs, sgl, sgl->Bool, true)->as_bool = ok;
  sgl_val_free(rhs, sgl);
  return true;
}

static bool int_inc_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  sgl_peek(sgl)->as_int++;
  return true;
}

static bool int_dec_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  sgl_peek(sgl)->as_int--;
  return true;
}

static bool int_add_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_peek(sgl)->as_int += rhs->as_int;
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool int_sub_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_peek(sgl)->as_int -= rhs->as_int;
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool int_mul_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_peek(sgl)->as_int *= rhs->as_int;
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool int_div_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_peek(sgl)->as_int /= rhs->as_int;
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool int_mod_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_peek(sgl)->as_int %= rhs->as_int;
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool int_fix_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *scale = sgl_pop(sgl);
  struct sgl_val *v = sgl_peek(sgl);
  int64_t iv = v->as_int;
  sgl_val_reuse(v, sgl, sgl->Fix, false);
  sgl_fix_init(&v->as_fix, iv, 0, scale->as_int, iv < 0);
  sgl_free(&sgl->val_pool, scale);
  return true;
}

static bool fix_add_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_fix_add(&sgl_peek(sgl)->as_fix, &rhs->as_fix);
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool fix_sub_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_fix_sub(&sgl_peek(sgl)->as_fix, &rhs->as_fix);
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool fix_mul_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_fix_mul(&sgl_peek(sgl)->as_fix, &rhs->as_fix);
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool fix_int_mul_imp(struct sgl_fimp *fimp,
                            struct sgl *sgl,
                            struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_fix_int_mul(&sgl_peek(sgl)->as_fix, rhs->as_int);
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool fix_div_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_fix_div(&sgl_peek(sgl)->as_fix, &rhs->as_fix);
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool fix_int_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  sgl_int_t iv = sgl_fix_int(&v->as_fix);
  sgl_val_reuse(v, sgl, sgl->Int, false)->as_int = iv;
  return true;
}

static bool min_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl), *lhs = sgl_peek(sgl);

  if (rhs->type != lhs->type) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Expected type %s: %s",
                          lhs->type->def.id->id, rhs->type->def.id->id));

    return false;
  }
  
  if (sgl_val_cmp(lhs, rhs) == SGL_GT) {
    sgl_pop(sgl);
    sgl_ls_push(sgl_stack(sgl), &rhs->ls);
    sgl_val_free(lhs, sgl);
  } else {
    sgl_val_free(rhs, sgl);
  }
  
  return true;
}

static bool max_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl), *lhs = sgl_peek(sgl);

  if (rhs->type != lhs->type) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Expected type %s: %s",
                          lhs->type->def.id->id, rhs->type->def.id->id));

    return false;
  }
  
  if (sgl_val_cmp(lhs, rhs) == SGL_LT) {
    sgl_pop(sgl);
    sgl_ls_push(sgl_stack(sgl), &rhs->ls);
    sgl_val_free(lhs, sgl);
  } else {
    sgl_val_free(rhs, sgl);
  }
  
  return true;
}

static bool seq_iter_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *in = sgl_pop(sgl);
  struct sgl_type *t = NULL;
  struct sgl_iter *i = sgl_val_iter(in, sgl, &t);
  bool ok = false;
  if (!i) { goto exit; }
  sgl_push(sgl, t)->as_iter = i;
  ok = true;
 exit:
  sgl_val_free(in, sgl);
  return ok;
}

static bool seq_splat_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *in = sgl_pop(sgl);
  struct sgl_iter *i = sgl_val_iter(in, sgl, NULL);
  sgl_val_free(in, sgl);
  if (!i) { return false; }
  struct sgl_ls *s = sgl_stack(sgl);
  while (sgl_iter_next_val(i, sgl, sgl_pos(sgl), s));
  sgl_iter_deref(i, sgl);
  return true;
}

static bool iter_next_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *i = sgl_pop(sgl);
  struct sgl_val *v =
    sgl_iter_next_val(i->as_iter, sgl, sgl_pos(sgl), sgl_stack(sgl));
  if (!v) { sgl_push(sgl, sgl->Nil); } 
  sgl_val_free(i, sgl);
  return true;
}

static bool iter_next_drop_imp(struct sgl_fimp *fimp,
                               struct sgl *sgl,
                               struct sgl_op **rpc) {
  struct sgl_val *i = sgl_pop(sgl);
  sgl_iter_skip_vals(i->as_iter, sgl, sgl_pos(sgl), 1);
  sgl_val_free(i, sgl);
  return true;
}

static bool iter_skip_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *n = sgl_pop(sgl), *i = sgl_pop(sgl);
  bool ok = sgl_iter_skip_vals(i->as_iter, sgl, sgl_pos(sgl), n->as_int);
  sgl_val_free(n, sgl);
  sgl_val_free(i, sgl);
  return ok;
}

static bool iter_map_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *fn = sgl_pop(sgl), *in = sgl_pop(sgl);
  struct sgl_iter *i = sgl_val_iter(in, sgl, NULL);
  bool ok = false;
  
  if (!i) {
    sgl_val_free(fn, sgl);
    goto exit;
  }
  
  sgl_push(sgl, sgl->Iter)->as_iter = &sgl_map_iter_new(sgl, i, fn)->iter;
  ok = true;
 exit:
  sgl_val_free(in, sgl);
  return ok;
}

static bool iter_filter_imp(struct sgl_fimp *fimp,
                            struct sgl *sgl,
                            struct sgl_op **rpc) {
  struct sgl_val *fn = sgl_pop(sgl), *in = sgl_pop(sgl);
  struct sgl_iter *i = sgl_val_iter(in, sgl, NULL);
  bool ok = false;
  
  if (!i) {
    sgl_val_free(fn, sgl);
    goto exit;
  }

  sgl_push(sgl, sgl->Iter)->as_iter = &sgl_filter_iter_new(sgl, i, fn)->iter;
  ok = true;
 exit:
  sgl_val_free(in, sgl);
  return ok;
}

static bool int_char_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  sgl_int_t iv = v->as_int;
  
  if (iv < 0 || iv > SGL_MAX_CHAR) {
    sgl_error(sgl, sgl_pos(sgl), sgl_sprintf("Invalid char: %" SGL_INT, iv));
    sgl_val_free(sgl_pop(sgl), sgl);
    return false;
  }

  sgl_val_reuse(v, sgl, sgl->Char, false)->as_char = iv;
  return true;
}

static bool char_int_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  sgl_int_t iv = v->as_char;
  sgl_val_reuse(v, sgl, sgl->Int, false)->as_int = iv;
  return true;
}

static bool str_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  struct sgl_buf out;
  sgl_buf_init(&out);
  sgl_val_print(v, sgl, sgl_pos(sgl), &out);
  bool is_short = out.len < SGL_MAX_STR_LEN;
  struct sgl_str *s = sgl_str_new(sgl, is_short ? sgl_buf_cs(&out) : NULL, out.len);

  if (!is_short) {
    char *d = (char *)out.data;
    d[out.len] = 0;
    s->as_long = d;
    out.data = NULL;
  }

  sgl_val_reuse(v, sgl, sgl->Str, true)->as_str = s;
  sgl_buf_deinit(&out);
  return true;
}

static bool str_pop_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_str *s = v->as_str;
  if (!s->len) { sgl_error(sgl, sgl_pos(sgl), "Nothing to pop"); }
  s->len--;
  sgl_push(sgl, sgl->Char)->as_char = sgl_str_cs(s)[s->len];
  sgl_val_free(v, sgl);
  return true;
}

static bool str_pop_drop_imp(struct sgl_fimp *fimp,
                             struct sgl *sgl,
                             struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_str *s = v->as_str;
  if (!s->len) { sgl_error(sgl, sgl_pos(sgl), "Nothing to pop"); }
  s->len--;
  sgl_val_free(v, sgl);
  return true;
}

static bool str_first_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_str *s = v->as_str;

  if (s->len) {
    sgl_push(sgl, sgl->Char)->as_char = *sgl_str_cs(s);
  } else {
    sgl_push(sgl, sgl->Nil);
  }

  sgl_val_free(v, sgl);
  return true;
}

static bool str_last_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_str *s = v->as_str;

  if (s->len) {
    sgl_push(sgl, sgl->Char)->as_char = sgl_str_cs(s)[s->len-1];
  } else {
    sgl_push(sgl, sgl->Nil);
  }

  sgl_val_free(v, sgl);
  return true;
}

static bool str_len_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_str *s = v->as_str;
  sgl_push(sgl, sgl->Int)->as_int = s->len;
  sgl_val_free(v, sgl);
  return true;
}

static bool seq_join_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *sepv = sgl_pop(sgl), *in = sgl_pop(sgl);
  struct sgl_iter *i = sgl_val_iter(in, sgl, NULL);
  sgl_val_free(in, sgl);

  if (!i) {
    sgl_val_free(sepv, sgl);
    return false;
  }
  
  struct sgl_buf out;
  sgl_buf_init(&out);
  struct sgl_val *v = NULL, *sep = NULL;
  struct sgl_pos *pos = sgl_pos(sgl);
  
  while ((v = sgl_iter_next_val(i, sgl, pos, NULL))) {
    if (sep) { sgl_val_print(sep, sgl, pos, &out); }
    sgl_val_print(v, sgl, pos, &out);
    sgl_val_free(v, sgl);
    sep = sepv;
  }

  sgl_iter_deref(i, sgl);
  sgl_val_free(sep, sgl);
  bool is_short = out.len < SGL_MAX_STR_LEN;
  struct sgl_str *s = sgl_str_new(sgl, is_short ? sgl_buf_cs(&out) : NULL, out.len);

  if (!is_short) {
    char *d = (char *)out.data;
    d[out.len] = 0;
    s->as_long = d;
    out.data = NULL;
  }

  sgl_buf_deinit(&out);
  sgl_push(sgl, sgl->Str)->as_str = s;
  return true;
}

static bool list_join_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *sep = sgl_pop(sgl), *in = sgl_pop(sgl);
  struct sgl_list *l = in->as_list;

  struct sgl_buf out;
  sgl_buf_init(&out);
  struct sgl_pos *pos = sgl_pos(sgl);
  sgl_int_t len = 0, sep_len = (sep->type == sgl->Str) ? sep->as_str->len : 1;
  struct sgl_ls *fst = l->root.next;

  sgl_ls_do(&l->root, struct sgl_val, ls, v) {
    if (&v->ls != fst) { len += sep_len; }

    if (v->type == sgl->Str) {
      len += v->as_str->len;
    } else {
      len++;
    }
  }

  sgl_buf_grow(&out, len);
  
  sgl_ls_do(&l->root, struct sgl_val, ls, v) {
    if (&v->ls != fst) { sgl_val_print(sep, sgl, pos, &out); }
    sgl_val_print(v, sgl, pos, &out);
  }

  sgl_val_free(sep, sgl);
  sgl_val_free(in, sgl);
  
  bool is_short = out.len < SGL_MAX_STR_LEN;
  struct sgl_str *s = sgl_str_new(sgl, is_short ? sgl_buf_cs(&out) : NULL, out.len);

  if (!is_short) {
    char *d = (char *)out.data;
    d[out.len] = 0;
    s->as_long = d;
    out.data = NULL;
  }

  sgl_buf_deinit(&out);
  sgl_push(sgl, sgl->Str)->as_str = s;
  return true;
}

static bool str_int_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_str *s = v->as_str;
  sgl_push(sgl, sgl->Int)->as_int = strtoimax(sgl_str_cs(s), NULL, 10);
  sgl_val_free(v, sgl);
  return true;
}

static bool str_iter_int_imp(struct sgl_fimp *fimp,
                             struct sgl *sgl,
                             struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_str_iter *i = sgl_baseof(struct sgl_str_iter, iter, v->as_iter);
  char *s = i->pos+1, *end = NULL;
  sgl_push(sgl, sgl->Int)->as_int = strtoimax(s, &end, 10);
  if (end > i->pos) { i->pos = end-1; }
  sgl_val_free(v, sgl);
  return true;
}

static bool pair_first_imp(struct sgl_fimp *fimp,
                           struct sgl *sgl,
                           struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_pair *p = &v->as_pair;
  sgl_val_dup(p->first, sgl, sgl_stack(sgl));
  sgl_val_free(v, sgl);
  return true;
}

static bool pair_last_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_pair *p = &v->as_pair;
  sgl_val_dup(p->last, sgl, sgl_stack(sgl));
  sgl_val_free(v, sgl);
  return true;
}

static bool pair_splat_imp(struct sgl_fimp *fimp,
                           struct sgl *sgl,
                           struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_pair *p = &v->as_pair;
  struct sgl_ls *s = sgl_stack(sgl);
  sgl_ls_push(s, &p->first->ls);
  sgl_ls_push(s, &p->last->ls);
  sgl_free(&sgl->val_pool, v);
  return true;
}

static bool list_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *in = sgl_peek(sgl);
  struct sgl_iter *i = sgl_val_iter(in, sgl, NULL);

  if (!i) {
    sgl_ls_delete(&in->ls);
    sgl_val_free(in, sgl);
    return false;
  }
  
  struct sgl_list *l = sgl_list_new(sgl);
  struct sgl_pos *pos = sgl_pos(sgl);
  while (sgl_iter_next_val(i, sgl, pos, &l->root));
  sgl_iter_deref(i, sgl);
  sgl_val_reuse(in, sgl, sgl->List, true)->as_list = l;
  return true;
}

static bool list_push_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl), *l = sgl_pop(sgl);
  sgl_ls_push(&l->as_list->root, &v->ls);
  sgl_val_free(l, sgl);
  return true;
}

static bool list_peek_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *l = sgl_pop(sgl);
  struct sgl_ls *v = sgl_ls_peek(&l->as_list->root);

  if (v) {
    sgl_val_dup(sgl_baseof(struct sgl_val, ls, v), sgl, sgl_stack(sgl));
  } else {
    sgl_push(sgl, sgl->Nil);
  }

  sgl_val_free(l, sgl);
  return true;
}

static bool list_pop_drop_imp(struct sgl_fimp *fimp,
                              struct sgl *sgl,
                              struct sgl_op **rpc) {
  struct sgl_val *l = sgl_pop(sgl);
  struct sgl_ls *v = sgl_ls_pop(&l->as_list->root);
  
  if (v) {
    sgl_val_free(sgl_baseof(struct sgl_val, ls, v), sgl);
    sgl_val_free(l, sgl);
    return true;
  }
  
  sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Nothing to pop"));
  sgl_val_free(l, sgl);
  return false;
}

static bool list_pop_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *l = sgl_pop(sgl);
  struct sgl_ls *v = sgl_ls_pop(&l->as_list->root);
  
  if (v) {
    sgl_ls_push(sgl_stack(sgl), v);
    sgl_val_free(l, sgl);
    return true;
  } else {
    sgl_push(sgl, sgl->Nil);
  }

  sgl_val_free(l, sgl);
  return false;
}

static bool list_len_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  struct sgl_list *l = v->as_list;
  sgl_val_reuse(v, sgl, sgl->Int, false)->as_int = sgl_list_len(l);
  sgl_list_deref(l, sgl);
  return true;
}

static bool list_delete_imp(struct sgl_fimp *fimp,
                            struct sgl *sgl,
                            struct sgl_op **rpc) {
  struct sgl_val *i = sgl_pop(sgl);
  struct sgl_list_iter *li = sgl_baseof(struct sgl_list_iter, iter, i->as_iter);

  if (li->pos == &li->list->root) {
    sgl_push(sgl, sgl->Nil);
  } else {
    struct sgl_val *v = sgl_baseof(struct sgl_val, ls, li->pos);
    li->pos = li->pos->next;
    sgl_ls_delete(&v->ls);
    sgl_ls_push(sgl_stack(sgl), &v->ls);
  }

  sgl_val_free(i, sgl);
  return true;
}

static bool list_delete_drop_imp(struct sgl_fimp *fimp,
                                 struct sgl *sgl,
                                 struct sgl_op **rpc) {
  struct sgl_val *i = sgl_pop(sgl);
  struct sgl_list_iter *li = sgl_baseof(struct sgl_list_iter, iter, i->as_iter);
  bool ok = false;
  
  if (li->pos == &li->list->root) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Iter eof"));
    goto exit;
  }

  struct sgl_val *v = sgl_baseof(struct sgl_val, ls, li->pos);
  li->pos = li->pos->next;
  sgl_ls_delete(&v->ls);
  sgl_val_free(v, sgl);
  ok = true;
 exit:
  sgl_val_free(i, sgl);
  return ok;
}

static bool list_insert_imp(struct sgl_fimp *fimp,
                            struct sgl *sgl,
                            struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl), *i = sgl_pop(sgl);
  struct sgl_list_iter *li = sgl_baseof(struct sgl_list_iter, iter, i->as_iter);
  sgl_ls_insert(li->pos, &v->ls);
  sgl_val_free(i, sgl);
  return true;
}

static bool field_put_sym_imp(struct sgl_fimp *fimp,
                              struct sgl *sgl,
                              struct sgl_op **rpc) {
  struct sgl_val *val = sgl_pop(sgl), *id = sgl_pop(sgl), *s = sgl_pop(sgl);
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, s->type);
  struct sgl_struct_type *st = sgl_baseof(struct sgl_struct_type, ref_type, rt);
  struct sgl_sym *fid = id->as_sym;
  struct sgl_field *f = sgl_struct_type_find(st, fid);
  bool ok = false;
  
  if (!f) {
    sgl_error(sgl, sgl_pos(sgl), sgl_sprintf("Unknown field: %s", fid->id));
    sgl_val_free(val, sgl);
    goto exit;
  }

  if (!sgl_derived(val->type, f->type)) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Expected %s, was: %s",
                          sgl_type_id(f->type)->id, sgl_type_id(val->type)->id));
    
    sgl_val_free(val, sgl);
    goto exit;
  }
  
  s->as_struct->fields[f->i] = val;
  ok = true;
 exit:
  sgl_val_free(id, sgl);
  sgl_val_free(s, sgl);
  return ok;
}

static bool say_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_buf buf;
  sgl_buf_init(&buf);
  sgl_val_print(v, sgl, sgl_pos(sgl), &buf);
  sgl_buf_putc(&buf, '\n');
  fputs(sgl_buf_cs(&buf), stdout);
  sgl_buf_deinit(&buf);
  sgl_val_free(v, sgl);
  return true;
}

static char *fopen_io_run_imp(struct sgl *sgl, const char *p, struct sgl_file *out) {
  FILE *f = fopen(p, "r");
  if (!f) { return sgl_sprintf("Failed opening file '%s': %" SGL_INT, p, errno); }
  sgl_file_init(out, f);
  return NULL;
}

static char *fopen_io_run(struct sgl_io *io,
                          struct sgl *sgl,
                          struct sgl_io_thread *thread) {
  struct sgl_val *p = io->args[0];
  return fopen_io_run_imp(sgl, sgl_str_cs(p->as_str), io->args[1]);
}

static void fopen_io_free(struct sgl_io *io, struct sgl *sgl) {
  struct sgl_val *p = io->args[0];
  struct sgl_file *f = io->args[1];
  
  if (io->error) {
    sgl_file_free(f, sgl);    
    sgl_error(sgl, &io->pos, io->error);
  } else {
    sgl_push(sgl, sgl->File)->as_file = f;
  }
  
  sgl_val_free(p, sgl);  
  sgl_io_free(io, sgl);
}

static bool fopen_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *p = sgl_pop(sgl);
  char *ps = sgl_str_cs(p->as_str);
  struct sgl_file *file = sgl_file_malloc(sgl);
  
  if (sgl->ntasks == 1) {
    char *error = fopen_io_run_imp(sgl, ps, file);

    if (error) {
      sgl_file_free(file, sgl);
      sgl_error(sgl, sgl_pos(sgl), error);
    } else {
      sgl_push(sgl, sgl->File)->as_file = file;
    }
    
    sgl_val_free(p, sgl);
    return error ? false : true;
  }

  struct sgl_io *io = sgl_io_new(sgl, -1, p, file);
  io->run = fopen_io_run;
  io->free = fopen_io_free;
  *rpc = sgl_io_yield(io, sgl, sgl_io_thread(sgl))->next;
  return true;
}

static bool file_lines_imp(struct sgl_fimp *fimp,
                           struct sgl *sgl,
                           struct sgl_op **rpc) {
  struct sgl_val *f = sgl_pop(sgl);
  sgl_push(sgl, sgl->Iter)->as_iter = &sgl_file_line_iter_new(sgl, f->as_file)->iter;
  sgl_val_free(f, sgl);
  return true;
}

static bool struct_new_imp(struct sgl_fimp *fimp,
                           struct sgl *sgl,
                           struct sgl_op **rpc) {
  struct sgl_val *tv = sgl_peek(sgl);
  struct sgl_type *t = tv->as_type;
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, t);
  struct sgl_struct_type *st = sgl_baseof(struct sgl_struct_type, ref_type, rt);
  sgl_val_reuse(tv, sgl, t, false)->as_struct = sgl_struct_new(st);
  
  return true;
}

static bool test_imp(struct sgl_fimp *fimp,
                     struct sgl *sgl,
                     struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  bool ok = sgl_val_bool(v);
  sgl_val_free(v, sgl);

  if (!ok) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Test failed"));
    return false;
  }
  
  fputc('.', stdout);
  return true;
}

static bool test_is_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_pop(sgl);

  bool ok = sgl_val_is(lhs, rhs);
  sgl_val_free(lhs, sgl);
  sgl_val_free(rhs, sgl);

  if (!ok) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Test failed"));
    return false;
  }
  
  fputc('.', stdout);
  return true;
}

static bool test_eq_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_pop(sgl);

  bool ok = sgl_val_eq(lhs, sgl, rhs);
  sgl_val_free(lhs, sgl);
  sgl_val_free(rhs, sgl);

  if (!ok) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Test failed"));
    return false;
  }
  
  fputc('.', stdout);
  return true;
}

static bool test_ne_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_pop(sgl);

  bool ok = !sgl_val_eq(lhs, sgl, rhs);
  sgl_val_free(lhs, sgl);
  sgl_val_free(rhs, sgl);

  if (!ok) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Test failed"));
    return false;
  }
  
  fputc('.', stdout);
  return true;
}

static void init_types(struct sgl_lib *l, struct sgl *sgl, struct sgl_pos *pos) {
  sgl->A = sgl_type_new(sgl, pos, l, sgl_sym(sgl, "A"),
                        sgl_types(NULL));

  sgl->Nil = sgl_nil_type_new(sgl, pos, l, sgl_sym(sgl, "Nil"),
                              sgl_types(sgl->A));

  sgl->T = sgl_type_new(sgl, pos, l, sgl_sym(sgl, "T"),
                        sgl_types(sgl->A));

  sgl->Num = sgl_trait_new(sgl, pos, l, sgl_sym(sgl, "Num"),
                           sgl_types(NULL));

  sgl->Seq = sgl_trait_new(sgl, pos, l, sgl_sym(sgl, "Seq"),
                           sgl_types(NULL));

  sgl->Iter = sgl_iter_type_new(sgl, pos, l, sgl_sym(sgl, "Iter"),
                                sgl_types(sgl->T, sgl->Seq));

  sgl->Bool = sgl_bool_type_new(sgl, pos, l, sgl_sym(sgl, "Bool"),
                                sgl_types(sgl->T));

  sgl->Buf = sgl_buf_type_new(sgl, pos, l, sgl_sym(sgl, "Buf"),
                              sgl_types(sgl->T));

  sgl->Char = sgl_char_type_new(sgl, pos, l, sgl_sym(sgl, "Char"),
                                sgl_types(sgl->T));

  sgl->Error = sgl_error_type_new(sgl, pos, l, sgl_sym(sgl, "Error"),
                                  sgl_types(sgl->T));

  sgl->File = sgl_file_type_new(sgl, pos, l, sgl_sym(sgl, "File"),
                                sgl_types(sgl->T));

  sgl->FileLineIter = sgl_ref_type_new(sgl, pos, l,
                                       sgl_sym(sgl, "FileLineIter"),
                                       sgl_types(sgl->Iter),
                                       sizeof(struct sgl_file_line_iter),
                                       offsetof(struct sgl_file_line_iter, iter) +
                                       offsetof(struct sgl_iter, ls));
  
  sgl->FilterIter = sgl_ref_type_new(sgl, pos, l,
                                     sgl_sym(sgl, "FilterIter"),
                                     sgl_types(sgl->Iter),
                                     sizeof(struct sgl_filter_iter),
                                     offsetof(struct sgl_filter_iter, iter) +
                                     offsetof(struct sgl_iter, ls));

  sgl->Fix = sgl_fix_type_new(sgl, pos, l, sgl_sym(sgl, "Fix"),
                              sgl_types(sgl->T, sgl->Num));

  sgl->Func = sgl_func_type_new(sgl, pos, l, sgl_sym(sgl, "Func"),
                                sgl_types(sgl->T));

  sgl->Int = sgl_int_type_new(sgl, pos, l, sgl_sym(sgl, "Int"),
                              sgl_types(sgl->T, sgl->Num, sgl->Seq));

  sgl->IntIter = sgl_ref_type_new(sgl, pos, l,
                                  sgl_sym(sgl, "IntIter"),
                                  sgl_types(sgl->Iter),
                                  sizeof(struct sgl_int_iter),
                                  offsetof(struct sgl_int_iter, iter) +
                                  offsetof(struct sgl_iter, ls));
  
  sgl->Lambda = sgl_lambda_type_new(sgl, pos, l, sgl_sym(sgl, "Lambda"),
                                    sgl_types(sgl->T));

  sgl->List = sgl_list_type_new(sgl, pos, l, sgl_sym(sgl, "List"),
                                sgl_types(sgl->T, sgl->Seq));

  sgl->ListIter = sgl_ref_type_new(sgl, pos, l,
                                   sgl_sym(sgl, "ListIter"),
                                   sgl_types(sgl->Iter),
                                   sizeof(struct sgl_list_iter),
                                   offsetof(struct sgl_list_iter, iter) +
                                   offsetof(struct sgl_iter, ls));

  sgl->Macro = sgl_lambda_type_new(sgl, pos, l, sgl_sym(sgl, "Macro"),
                                   sgl_types(sgl->T));

  sgl->MapIter = sgl_ref_type_new(sgl, pos, l,
                                  sgl_sym(sgl, "MapIter"),
                                  sgl_types(sgl->Iter),
                                  sizeof(struct sgl_map_iter),
                                  offsetof(struct sgl_map_iter, iter) +
                                  offsetof(struct sgl_iter, ls));

  sgl->Pair = sgl_pair_type_new(sgl, pos, l, sgl_sym(sgl, "Pair"),
                                sgl_types(sgl->T));

  sgl->Str = sgl_str_type_new(sgl, pos, l, sgl_sym(sgl, "Str"),
                              sgl_types(sgl->T, sgl->Seq));

  sgl->StrIter = sgl_ref_type_new(sgl, pos, l,
                                  sgl_sym(sgl, "StrIter"),
                                  sgl_types(sgl->Iter),
                                  sizeof(struct sgl_str_iter),
                                  offsetof(struct sgl_str_iter, iter) +
                                  offsetof(struct sgl_iter, ls));

  sgl->Sym = sgl_sym_type_new(sgl, pos, l, sgl_sym(sgl, "Sym"),
                              sgl_types(sgl->T));

  sgl->U = sgl_type_new(sgl, pos, l, sgl_sym(sgl, "U"),
                        sgl_types(sgl->T));

  sgl->Struct = sgl_trait_new(sgl, pos, l, sgl_sym(sgl, "Struct"),
                              sgl_types(sgl->U));
  
  sgl->Meta = sgl_meta_type_new(sgl, pos, l, sgl_sym(sgl, "Meta"),
                                sgl_types(sgl->T));

  sgl->Undef = sgl_undef_type_new(sgl, pos, l, sgl_sym(sgl, "Undef"),
                                  sgl_types(NULL));
}

static void init_vals(struct sgl_lib *l, struct sgl *sgl, struct sgl_pos *pos) {
  sgl_val_init(&sgl->nil, sgl, sgl->Nil, NULL);
  sgl_val_init(&sgl->t, sgl, sgl->Bool, NULL)->as_bool = true;
  sgl_val_init(&sgl->f, sgl, sgl->Bool, NULL)->as_bool = false;
}

static bool lib_init(struct sgl_lib *l, struct sgl *sgl, struct sgl_pos *pos) {
  init_types(l, sgl, pos);
  init_vals(l, sgl, pos);
  
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "::"), 0, pair_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "bench:"), 1, bench_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "debug"), 0, debug_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "drop"), 0, drop_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "drop2"), 0, drop2_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "dup"), 0, dup_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "for:"), 1, for_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "func:"), 3, func_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "idle:"), 1, idle_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "if:"), 2, if_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "let:"), 1, let_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "link:"), 1, link_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "or:"), 1, or_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "pre:"), 1, pre_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "put:"), 1, put_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "recall"), 0, recall_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "recall:"), 1, recall_arg_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "rotl"), 0, rotl_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "rotr"), 0, rotr_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "struct:"), 3, struct_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "swap"), 0, swap_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "task:"), 1, task_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "throw"), 0, throw_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "times:"), 1, times_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "try:"), 1, try_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "type"), 0, type_get_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "type:"), 2, type_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "use:"), 2, use_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "while:"), 1, while_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "yield"), 0, yield_imp);
  
  sgl_lib_add_const(l, sgl, pos, sgl_sym(sgl, "nil"), &sgl->nil);
  sgl_lib_add_const(l, sgl, pos, sgl_sym(sgl, "t"), &sgl->t);
  sgl_lib_add_const(l, sgl, pos, sgl_sym(sgl, "f"), &sgl->f);

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "dump"),
                    dump_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->A)); 
  
  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "type-sub?"),
                    type_sub_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->A), sgl_arg(sgl->Meta)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "sub?"),
                    sub_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->Meta), sgl_arg(sgl->Meta)); 

  sgl->clone_fimp = sgl_lib_add_cfunc(l, sgl, pos, 
                                      sgl_sym(sgl, "clone"),
                                      clone_imp,
                                      sgl_rets(sgl->T),
                                      sgl_arg(sgl->T));
  
  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "clone!"),
                    clone_drop_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->T)); 

  sgl->call_fimp = sgl_lib_add_cfunc(l, sgl, pos, 
                                     sgl_sym(sgl, "call"),
                                     call_imp,
                                     NULL,
                                     sgl_arg(sgl->A));

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "catch"),
                    catch_imp,
                    sgl_rets(sgl->T),
                    sgl_arg(sgl->Error)); 

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "bool"),
                    bool_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "not"),
                    not_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "=="),
                    is_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->A), sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "!=="),
                    nis_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->A), sgl_arg(sgl->A)); 

  sgl->eq_fimp = sgl_lib_add_cfunc(l, sgl, pos, 
                                   sgl_sym(sgl, "="),
                                   eq_imp,
                                   sgl_rets(sgl->Bool),
                                   sgl_arg(sgl->A), sgl_arg(sgl->A));

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "!="),
                    ne_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->A), sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "<"),
                    lt_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->T), sgl_arg(sgl->T)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "<="),
                    lte_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->T), sgl_arg(sgl->T)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, ">"),
                    gt_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->T), sgl_arg(sgl->T)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, ">="),
                    gte_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->T), sgl_arg(sgl->T)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "++"),
                    int_inc_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "--"),
                    int_dec_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "+"),
                    int_add_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "-"),
                    int_sub_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "*"),
                    int_mul_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "div"),
                    int_div_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "mod"),
                    int_mod_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "fix"),
                    int_fix_imp,
                    sgl_rets(sgl->Fix),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "+"),
                    fix_add_imp,
                    sgl_rets(sgl->Fix),
                    sgl_arg(sgl->Fix), sgl_arg(sgl->Fix)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "-"),
                    fix_sub_imp,
                    sgl_rets(sgl->Fix),
                    sgl_arg(sgl->Fix), sgl_arg(sgl->Fix)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "*"),
                    fix_mul_imp,
                    sgl_rets(sgl->Fix),
                    sgl_arg(sgl->Fix), sgl_arg(sgl->Fix)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "*"),
                    fix_int_mul_imp,
                    sgl_rets(sgl->Fix),
                    sgl_arg(sgl->Fix), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "/"),
                    fix_div_imp,
                    sgl_rets(sgl->Fix),
                    sgl_arg(sgl->Fix), sgl_arg(sgl->Fix)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "int"),
                    fix_int_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Fix)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "min"),
                    min_imp,
                    sgl_rets(sgl->T),
                    sgl_arg(sgl->T), sgl_arg(sgl->T)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "max"),
                    max_imp,
                    sgl_rets(sgl->T),
                    sgl_arg(sgl->T), sgl_arg(sgl->T)); 

  sgl->iter_fimp = sgl_lib_add_cfunc(l, sgl, pos,
                                     sgl_sym(sgl, "iter"),
                                     seq_iter_imp,
                                     sgl_rets(sgl->Iter),
                                     sgl_arg(sgl->Seq));
  
  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, ".."),
                    seq_splat_imp,
                    NULL,
                    sgl_arg(sgl->Seq));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "next"),
                    iter_next_imp,
                    sgl_rets(sgl->A),
                    sgl_arg(sgl->Iter));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "next!"),
                    iter_next_drop_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Iter));
  
  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "skip"),
                    iter_skip_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Iter), sgl_arg(sgl->Int));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "map"),
                    iter_map_imp,
                    sgl_rets(sgl->MapIter),
                    sgl_arg(sgl->Seq), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "filter"),
                    iter_filter_imp,
                    sgl_rets(sgl->FilterIter),
                    sgl_arg(sgl->Seq), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "char"),
                    int_char_imp,
                    sgl_rets(sgl->Char),
                    sgl_arg(sgl->Int));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "int"),
                    char_int_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Char));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "str"),
                    str_imp,
                    sgl_rets(sgl->Str),
                    sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "pop"),
                    str_pop_imp,
                    sgl_rets(sgl_type_union(sgl, pos, sgl->Char, sgl->Nil)),
                    sgl_arg(sgl->Str));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "pop!"),
                    str_pop_drop_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Str));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "first"),
                    str_first_imp,
                    sgl_rets(sgl_type_union(sgl, pos, sgl->Char, sgl->Nil)),
                    sgl_arg(sgl->Str));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "last"),
                    str_last_imp,
                    sgl_rets(sgl_type_union(sgl, pos, sgl->Char, sgl->Nil)),
                    sgl_arg(sgl->Str));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "len"),
                    str_len_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Str));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "join"),
                    seq_join_imp,
                    sgl_rets(sgl->Str),
                    sgl_arg(sgl->Seq), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "join"),
                    list_join_imp,
                    sgl_rets(sgl->Str),
                    sgl_arg(sgl->List), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "int"),
                    str_int_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Str));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "int"),
                    str_iter_int_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->StrIter));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "first"),
                    pair_first_imp,
                    sgl_rets(sgl->T),
                    sgl_arg(sgl->Pair));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "last"),
                    pair_last_imp,
                    sgl_rets(sgl->T),
                    sgl_arg(sgl->Pair));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, ".."),
                    pair_splat_imp,
                    sgl_rets(sgl->T, sgl->T),
                    sgl_arg(sgl->Pair));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "list"),
                    list_imp,
                    sgl_rets(sgl->List),
                    sgl_arg(sgl->Seq));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "push"),
                    list_push_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->List), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "peek"),
                    list_peek_imp,
                    sgl_rets(sgl->A),
                    sgl_arg(sgl->List));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "pop!"),
                    list_pop_drop_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->List));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "pop"),
                    list_pop_imp,
                    sgl_rets(sgl->A),
                    sgl_arg(sgl->List));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "len"),
                    list_len_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->List));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "delete"),
                    list_delete_imp,
                    sgl_rets(sgl->A),
                    sgl_arg(sgl->ListIter));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "delete!"),
                    list_delete_drop_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->ListIter));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "insert"),
                    list_insert_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->ListIter), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "put"),
                    field_put_sym_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Struct), sgl_arg(sgl->Sym), sgl_arg(sgl->A));


  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "say"),
                    say_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "fopen"),
                    fopen_imp,
                    sgl_rets(sgl->File),
                    sgl_arg(sgl->Str)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "lines"),
                    file_lines_imp,
                    sgl_rets(sgl->FileLineIter),
                    sgl_arg(sgl->File)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "new"),
                    struct_new_imp,
                    sgl_rets(sgl->Struct),
                    sgl_arg(sgl_type_meta(sgl->Struct, sgl))); 
  
  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "test"),
                    test_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "test=="),
                    test_is_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->A), sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "test="),
                    test_eq_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->A), sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "test!="),
                    test_ne_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->A), sgl_arg(sgl->A)); 

  return sgl->errors.next == &sgl->errors;
}

struct sgl_lib *sgl_abc_lib_new(struct sgl *sgl, struct sgl_pos *pos) {
  struct sgl_lib *l = sgl_lib_new(sgl, pos, sgl_sym(sgl, "abc"));
  l->init = lib_init;
  return l;
}
