#ifndef SNIGL_FORM_H
#define SNIGL_FORM_H

#include "snigl/pos.h"
#include "snigl/ls.h"

struct sgl;
struct sgl_cemit;
struct sgl_form;
struct sgl_val;

struct sgl_form_body {
  struct sgl_ls forms;
};

struct sgl_form_id {
  struct sgl_sym *val;
};

struct sgl_form_index {
  struct sgl_form *target;
  struct sgl_ls vals;
};

struct sgl_form_lit {
  struct sgl_val *val;
};

struct sgl_form_ref {
  struct sgl_form *target;
};

struct sgl_form_type {
  const char *id;

  bool (*cemit_form)(struct sgl_form *,
                     struct sgl *,
                     const char *,
                     struct sgl_cemit *);
  
  void (*deinit_form)(struct sgl_form *, struct sgl *);
};

struct sgl_form_type *sgl_form_type_init(struct sgl_form_type *type, const char *id);

extern struct sgl_form_type SGL_FORM_EXPR, SGL_FORM_ID, SGL_FORM_INDEX,
  SGL_FORM_INTER, SGL_FORM_LIT, SGL_FORM_LIST, SGL_FORM_NOP, SGL_FORM_REF,
  SGL_FORM_SCOPE, SGL_FORM_STR;

struct sgl_form {
  struct sgl_ls ls;
  struct sgl_form_type *type;
  struct sgl_pos pos;
  sgl_int_t cemit_id, nrefs;
  
  struct sgl_ls *(*compile)(struct sgl_form *form,
                              struct sgl *sgl,
                              struct sgl_ls *root);
  union {
    struct sgl_form_body as_body;
    struct sgl_form_id as_id;
    struct sgl_form_index as_index;
    struct sgl_form_lit as_lit;    
    struct sgl_form_ref as_ref;    
  };
};

struct sgl_form *sgl_form_init(struct sgl_form *form,
                               struct sgl_pos *pos,
                               struct sgl_form_type *type,
                               struct sgl_ls *root);

struct sgl_form *sgl_form_deinit(struct sgl_form *form, struct sgl *sgl);

struct sgl_form *sgl_form_new(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_form_type *type,
                              struct sgl_ls *root);

void sgl_form_deref(struct sgl_form *form, struct sgl *sgl);

bool sgl_form_compile(struct sgl_form *f, struct sgl *sgl);
struct sgl_list *sgl_form_eval(struct sgl_form *f, struct sgl *sgl);

sgl_int_t sgl_form_cemit_id(struct sgl_form *f,
                            struct sgl *sgl,
                            struct sgl_cemit *out); 

bool sgl_form_cemit(struct sgl_form *f,
                    struct sgl *sgl,
                    const char *root,
                    struct sgl_cemit *out); 

struct sgl_form *sgl_form_expr_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_ls *root);

struct sgl_form *sgl_form_id_new(struct sgl *sgl,
                                 struct sgl_pos *pos,
                                 struct sgl_ls *root,
                                 struct sgl_sym *id);

struct sgl_form *sgl_form_index_new(struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_ls *root);

struct sgl_form *sgl_form_inter_new(struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_ls *root);

struct sgl_form *sgl_form_lit_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_ls *root,
                                  struct sgl_val *val);

struct sgl_form *sgl_form_list_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_ls *root);

struct sgl_form *sgl_form_nop_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_ls *root);

struct sgl_form *sgl_form_ref_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_ls *root,
                                  struct sgl_form *target);

struct sgl_form *sgl_form_scope_new(struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_ls *root);

struct sgl_form *sgl_form_str_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_ls *root);

struct sgl_ls *sgl_forms_deref(struct sgl_ls *root, struct sgl *sgl);

void sgl_setup_forms();

#endif
