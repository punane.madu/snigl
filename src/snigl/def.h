#ifndef SNIGL_DEF_H
#define SNIGL_DEF_H

#include "snigl/ls.h"
#include "snigl/pos.h"

struct sgl_def;
struct sgl;

struct sgl_def_type {
  const char *id;
  void (*free_def)(struct sgl_def *, struct sgl *sgl);
};

struct sgl_def_type *sgl_def_type_init(struct sgl_def_type *type, const char *id);

struct sgl_def {
  struct sgl_lib *lib;
  struct sgl_def_type *type;
  struct sgl_sym *id;
  struct sgl_pos pos;
  sgl_int_t nrefs;
};

struct sgl_def *sgl_def_init(struct sgl_def *d,
                             struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_lib *lib,
                             struct sgl_def_type *type,
                             struct sgl_sym *id);

void sgl_def_deref(struct sgl_def *d, struct sgl *sgl);

#endif
