#include <math.h>
#include <stdio.h>

#include "snigl/buf.h"
#include "snigl/fix.h"
#include "snigl/util.h"

struct sgl_fix *sgl_fix_init(struct sgl_fix *f,
                             sgl_int_t trunc, sgl_int_t frac,
                             sgl_int_t scale,
                             bool neg) {
  f->scale = sgl_pow(10, scale);
  f->val = trunc * f->scale + frac;
  if (neg) { f->val = -f->val; }
  return f;
}

bool sgl_fix_is(struct sgl_fix *f, struct sgl_fix *rhs) {
  return f->val == rhs->val && f->scale == rhs->scale;
}

bool sgl_fix_eq(struct sgl_fix *f, struct sgl_fix *rhs) {
  return f->val * rhs->scale == rhs->val * f->scale;
}

enum sgl_cmp sgl_fix_cmp(struct sgl_fix *f, struct sgl_fix *rhs) {
  return sgl_int_cmp(f->val * rhs->scale, rhs->val * f->scale);
}

void sgl_fix_add(struct sgl_fix *f, struct sgl_fix *rhs) {
  sgl_int_t s = sgl_min(f->scale, rhs->scale);
  f->val = f->val * s / f->scale + rhs->val * s / rhs->scale;
  f->scale = s;
}

void sgl_fix_sub(struct sgl_fix *f, struct sgl_fix *rhs) {
  sgl_int_t s = sgl_min(f->scale, rhs->scale);
  f->val = f->val * s / f->scale - rhs->val * s / rhs->scale;
  f->scale = s;
}

void sgl_fix_mul(struct sgl_fix *f, struct sgl_fix *rhs) {
  f->val *= rhs->val;
  f->scale *= rhs->scale;
}

void sgl_fix_int_mul(struct sgl_fix *f, sgl_int_t rhs) { f->val *= rhs; }

void sgl_fix_div(struct sgl_fix *f, struct sgl_fix *rhs) {
  sgl_int_t s = sgl_max(1, f->scale / rhs->scale);
  f->val = f->val * s;
  f->val /= rhs->val * f->scale / rhs->scale;
  f->scale = s;
}

sgl_int_t sgl_fix_int(struct sgl_fix *f) { return f->val / f->scale; }

void sgl_fix_dump(struct sgl_fix *f, struct sgl_buf *out) {
  sgl_int_t v = f->val / f->scale;
  if (v || f->scale == 1) { sgl_buf_printf(out, "%" SGL_INT, v); }
  else if (f->val < 0) { sgl_buf_putc(out, '-'); }
  sgl_buf_putc(out, '.');

  if (f->scale > 1) {
    sgl_buf_printf(out, "%" SGL_INT, sgl_abs(f->val % f->scale));
  }
}

void sgl_fix_print(struct sgl_fix *f, struct sgl_buf *out) {
  sgl_buf_printf(out, "%" SGL_INT, f->val / f->scale);
  sgl_buf_putc(out, '.');
  sgl_buf_printf(out, "%" SGL_INT, f->val % f->scale);
}
