#ifndef SNIGL_IO_H
#define SNIGL_IO_H

#include "snigl/config.h"
#include "snigl/int.h"
#include "snigl/ls.h"
#include "snigl/pos.h"

#define sgl_io_new(sgl, depth, ...)                      \
  _sgl_io_new(sgl, depth, (void *[]){__VA_ARGS__, NULL}) \

struct sgl;
struct sgl_io_thread;
struct sgl_pos;
struct sgl_val;

struct sgl_io {
  struct sgl_ls ls;
  struct sgl_pos pos;
  sgl_int_t depth;
  struct sgl_task *task;
  void *args[SGL_MAX_IO_ARGS];
  char *error;
  
  char *(*run)(struct sgl_io *, struct sgl *, struct sgl_io_thread *);
  void (*free)(struct sgl_io *, struct sgl *);
};

struct sgl_io *_sgl_io_new(struct sgl *sgl, sgl_int_t depth, void *args[]);
void sgl_io_free(struct sgl_io *io, struct sgl *sgl);

struct sgl_op *sgl_io_yield(struct sgl_io *io,
                            struct sgl *sgl,
                            struct sgl_io_thread *thread);

#endif
