#ifndef SNIGL_SGL_H
#define SNIGL_SGL_H

#include <pthread.h>
#include <stdio.h>

#include "snigl/int.h"
#include "snigl/lib.h"
#include "snigl/ls.h"
#include "snigl/lset.h"
#include "snigl/op.h"
#include "snigl/pool.h"
#include "snigl/pos.h"
#include "snigl/scope.h"
#include "snigl/task.h"
#include "snigl/type.h"
#include "snigl/val.h"

#define sgl_use(sgl, pos, src, ...)                                 \
  _sgl_use(sgl, pos, src, (struct sgl_sym *[]){__VA_ARGS__, NULL})  \

extern const sgl_int_t SGL_VERSION[3];

struct sgl_io;
struct sgl_val;

struct sgl {
  bool debug;
  sgl_int_t trace;
  
  struct sgl_pool call_pool,
    error_pool,
    fimp_pool, field_pool, form_pool,
    io_pool, io_thread_pool,
    lib_pool,
    macro_pool,
    op_pool,
    ref_type_pool,
    scope_pool, struct_type_pool, sym_pool,
    task_pool, trace_pool, try_pool, type_pool,
    val_pool, var_pool;

  struct sgl_lset libs, syms;
  struct sgl_ls errors, ops, tasks;
  struct sgl_vec links;
  sgl_int_t ntasks;
  
  struct sgl_lib home_lib, *abc_lib;
  struct sgl_task main_task, *task;
  struct sgl_scope main_scope;
  
  sgl_int_t type_tag;
  struct sgl_type *A,
    *Bool, *Buf,
    *Char,
    *Error,
    *File, *FileLineIter, *FilterIter, *Fix, *Func,
    *Int, *IntIter, *Iter,
    *Lambda, *List, *ListIter,
    *Macro, *MapIter, *Meta,
    *Nil, *Num,
    *Pair,
    *Seq, *Str, *StrIter, *Struct, *Sym,
    *T,
    *U, *Undef;

  struct sgl_val nil, t, f;
  struct sgl_fimp *call_fimp, *clone_fimp, *eq_fimp, *iter_fimp;
  
  struct sgl_op *start_pc, *end_pc, *pc;
  struct sgl_fimp *c_fimp;

  sgl_int_t io_depth;
  
  struct sgl_ls io_threads;
  sgl_int_t nio_threads, nio_idle_threads;
  
  struct sgl_ls io_queue;

  pthread_cond_t io_done;
  sgl_int_t nio_done;
  
  pthread_mutex_t io_lock;
};

void sgl_setup();

struct sgl *sgl_init(struct sgl *sgl);
struct sgl *sgl_deinit(struct sgl *sgl);

struct sgl_error *sgl_error(struct sgl *sgl, struct sgl_pos *pos, char *msg);

struct sgl_op *sgl_throw(struct sgl *sgl,
                         struct sgl_pos *pos,
                         char *msg,
                         struct sgl_val *val);

struct sgl_sym *sgl_nsym(struct sgl *sgl, const char *id, sgl_int_t len);
struct sgl_sym *sgl_sym(struct sgl *sgl, const char *id);

bool sgl_add_lib(struct sgl *sgl, struct sgl_pos *pos, struct sgl_lib *lib);

struct sgl_lib *sgl_find_lib(struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_sym *id);

struct sgl_lib *sgl_lib(struct sgl *sgl);
void sgl_beg_lib(struct sgl *sgl, struct sgl_lib *lib);
struct sgl_lib *sgl_end_lib(struct sgl *sgl);

bool _sgl_use(struct sgl *sgl,
              struct sgl_pos *pos,
              struct sgl_lib *src,
              struct sgl_sym *defs[]);

bool sgl_add_const(struct sgl *sgl,
                   struct sgl_pos *pos,
                   struct sgl_sym *id,
                   struct sgl_val *val);

struct sgl_def *sgl_find_def(struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_sym *id);

struct sgl_type *sgl_find_type(struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_sym *id);

struct sgl_type *sgl_get_type(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_sym *id);

struct sgl_macro *sgl_find_macro(struct sgl *sgl,
                                 struct sgl_pos *pos,
                                 struct sgl_sym *id);

struct sgl_func *sgl_find_func(struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_sym *id);

struct sgl_func *sgl_get_func(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_sym *id);

struct sgl_scope *sgl_scope(struct sgl *sgl);
void sgl_beg_scope(struct sgl *sgl, struct sgl_scope *parent);
void sgl_end_scope(struct sgl *sgl);

struct sgl_call *sgl_call(struct sgl *sgl);
struct sgl_io_thread *sgl_io_thread(struct sgl *sgl);
struct sgl_op *sgl_yield(struct sgl *sgl);

struct sgl_ls *sgl_reg_stack(struct sgl *sgl);
struct sgl_val *sgl_push_reg(struct sgl *sgl, struct sgl_type *type);
struct sgl_val *sgl_peek_reg(struct sgl *sgl);
struct sgl_val *sgl_pop_reg(struct sgl *sgl);

struct sgl_ls *sgl_stack(struct sgl *sgl);
struct sgl_val *sgl_push(struct sgl *sgl, struct sgl_type *type);
struct sgl_val *sgl_peek(struct sgl *sgl);
struct sgl_val *sgl_pop(struct sgl *sgl);
void sgl_reset_stack(struct sgl *sgl);

struct sgl_list *sgl_beg_stack(struct sgl *sgl);
struct sgl_list *sgl_end_stack(struct sgl *sgl);
struct sgl_list *sgl_switch_stack(struct sgl *sgl, sgl_int_t n);

struct sgl_val *sgl_get_var(struct sgl *sgl, struct sgl_sym *id);

bool sgl_let_var(struct sgl *sgl,
                 struct sgl_pos *pos,
                 struct sgl_sym *id,
                 struct sgl_val *val);

bool sgl_compile(struct sgl *sgl, struct sgl_ls *in);
void sgl_dump_ops(struct sgl *sgl, FILE *out);
bool sgl_cemit(struct sgl *sgl, struct sgl_op *pc, struct sgl_cemit *out);
bool sgl_load(struct sgl *sgl, struct sgl_pos *pos, const char *path);

#ifdef SGL_USE_DL
bool sgl_link(struct sgl *sgl, struct sgl_pos *pos, const char *path);
#endif

sgl_int_t sgl_trace(struct sgl *sgl, struct sgl_op *pc);
struct sgl_pos *sgl_pos(struct sgl *sgl);

bool sgl_run(struct sgl *sgl, struct sgl_op *pc);

bool sgl_run_task(struct sgl *sgl,
                  struct sgl_task *task,
                  struct sgl_op *pc, struct sgl_op *end_pc);

bool sgl_run_io(struct sgl *sgl, struct sgl_io *io);
void sgl_dump_errors(struct sgl *sgl, FILE *out);

#endif
