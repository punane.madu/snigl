#include <assert.h>
#include <ctype.h>

#include <errno.h>
#include <stdatomic.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "snigl/buf.h"
#include "snigl/call.h"
#include "snigl/cemit.h"
#include "snigl/config.h"
#include "snigl/error.h"
#include "snigl/file.h"
#include "snigl/form.h"
#include "snigl/func.h"
#include "snigl/iters/filter.h"
#include "snigl/iters/int.h"
#include "snigl/iters/map.h"
#include "snigl/io.h"
#include "snigl/io_thread.h"
#include "snigl/libs/abc.h"
#include "snigl/list.h"
#include "snigl/macro.h"
#include "snigl/parse.h"
#include "snigl/pos.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/str.h"
#include "snigl/struct.h"
#include "snigl/sym.h"
#include "snigl/trace.h"
#include "snigl/types/struct.h"
#include "snigl/try.h"
#include "snigl/val.h"
#include "snigl/var.h"
#include "snigl/util.h"
#include "snigl/vec.h"

#ifdef SGL_USE_DL
#include <dlfcn.h>
#endif

const sgl_int_t SGL_VERSION[3] = {0, 8, 5};

void sgl_setup() {
  sgl_setup_pos();
  sgl_setup_forms();
  sgl_setup_ops();
  sgl_setup_macros();
  sgl_setup_types();
  sgl_setup_funcs();
}

static enum sgl_cmp syms_cmp(struct sgl_ls *lhs, const void *rhs, void *data) {
  struct sgl_sym *lsym = sgl_baseof(struct sgl_sym, ls, lhs);
  const char *i = lsym->id, *j = rhs;
  sgl_int_t len = *(sgl_int_t *)data;
  
  while (len && *i) {
    if (*i < *j) { return SGL_LT; }
    if (*i++ > *j++) { return SGL_GT; }
    len--;
  }

  return len ? SGL_LT : (*i ? SGL_GT : SGL_EQ);
}

static enum sgl_cmp libs_cmp(struct sgl_ls *lhs, const void *rhs, void *_) {
  struct sgl_sym *lsym = sgl_baseof(struct sgl_lib, ls, lhs)->id;
  return sgl_sym_cmp(lsym, rhs);
}

struct sgl *sgl_init(struct sgl *sgl) {
  sgl->debug = false;
  sgl->trace = -1;
  
  sgl_pool_init(&sgl->call_pool,
                sizeof(struct sgl_call),
                offsetof(struct sgl_call, ls));

  sgl_pool_init(&sgl->error_pool,
                sizeof(struct sgl_error),
                offsetof(struct sgl_error, ls));

  sgl_pool_init(&sgl->field_pool,
                sizeof(struct sgl_field),
                offsetof(struct sgl_field, ls));

  sgl_pool_init(&sgl->fimp_pool,
                sizeof(struct sgl_fimp),
                offsetof(struct sgl_fimp, ls));

  sgl_pool_init(&sgl->form_pool,
                sizeof(struct sgl_form),
                offsetof(struct sgl_form, ls));

  sgl_pool_init(&sgl->io_pool,
                sizeof(struct sgl_io),
                offsetof(struct sgl_io, ls));

  sgl_pool_init(&sgl->io_thread_pool,
                sizeof(struct sgl_io_thread),
                offsetof(struct sgl_io_thread, ls));

  sgl_pool_init(&sgl->lib_pool,
                sizeof(struct sgl_lib),
                offsetof(struct sgl_lib, ls));

  sgl_pool_init(&sgl->macro_pool,
                sizeof(struct sgl_macro),
                offsetof(struct sgl_macro, ls));

  sgl_pool_init(&sgl->op_pool,
                sizeof(struct sgl_op),
                offsetof(struct sgl_op, ls));

  sgl_pool_init(&sgl->ref_type_pool,
                sizeof(struct sgl_ref_type),
                offsetof(struct sgl_ref_type, type) +
                offsetof(struct sgl_type, ls));

  sgl_pool_init(&sgl->scope_pool,
                sizeof(struct sgl_scope),
                offsetof(struct sgl_scope, ls));

  sgl_pool_init(&sgl->struct_type_pool,
                sizeof(struct sgl_struct_type),
                offsetof(struct sgl_struct_type, ref_type) +
                offsetof(struct sgl_ref_type, type) +
                offsetof(struct sgl_type, ls));

  sgl_pool_init(&sgl->sym_pool,
                sizeof(struct sgl_sym),
                offsetof(struct sgl_sym, ls));

  sgl_pool_init(&sgl->task_pool,
                sizeof(struct sgl_task),
                offsetof(struct sgl_task, ls));

  sgl_pool_init(&sgl->trace_pool,
                sizeof(struct sgl_trace),
                offsetof(struct sgl_trace, ls));

  sgl_pool_init(&sgl->try_pool,
                sizeof(struct sgl_try),
                offsetof(struct sgl_try, ls));

  sgl_pool_init(&sgl->type_pool,
                sizeof(struct sgl_type),
                offsetof(struct sgl_type, ls));

  sgl_pool_init(&sgl->val_pool,
                sizeof(struct sgl_val),
                offsetof(struct sgl_val, ls));

  sgl_pool_init(&sgl->var_pool,
                sizeof(struct sgl_var),
                offsetof(struct sgl_var, ls));

  struct sgl_pos *pos = &SGL_NULL_POS;
  sgl_lset_init(&sgl->syms, syms_cmp);
  
  sgl_ls_init(&sgl->errors);
  sgl_ls_init(&sgl->ops);
  
  sgl->end_pc = sgl_nop_new(sgl, NULL);
  sgl->end_pc->next = sgl->end_pc;
  sgl->start_pc = sgl_nop_new(sgl, NULL);
  sgl->start_pc->prev = sgl->start_pc;
  sgl->pc = NULL;
  sgl->c_fimp = NULL;
  sgl->io_depth = 0;
  sgl->ntasks = 0;
  sgl->type_tag = 0;

  sgl_lset_init(&sgl->libs, &libs_cmp);
  sgl_lib_init(&sgl->home_lib, sgl, pos, sgl_sym(sgl, "home"));
  sgl_add_lib(sgl, pos, &sgl->home_lib);

  sgl_ls_init(&sgl->tasks);

  sgl->task =
    sgl_task_init(&sgl->main_task, sgl, &sgl->home_lib, NULL, NULL, sgl->end_pc);
  
  sgl_scope_init(&sgl->main_scope, NULL);
  sgl->main_scope.nrefs++;
  sgl_ls_push(&sgl->main_task.scopes, &sgl->main_scope.ls);

  sgl->abc_lib = sgl_abc_lib_new(sgl, pos);
  sgl_add_lib(sgl, pos, sgl->abc_lib);

  sgl_vec_init(&sgl->links, sizeof(void *));
  sgl_ls_init(&sgl->io_threads);
  sgl->nio_threads = sgl->nio_idle_threads = 0;
  sgl_ls_init(&sgl->io_queue);
  pthread_cond_init(&sgl->io_done, NULL);
  sgl->nio_done = 0;
  pthread_mutex_init(&sgl->io_lock, NULL);
  return sgl;
}

static void stop_io_threads(struct sgl *sgl) {
  pthread_mutex_lock(&sgl->io_lock);
  
  sgl_ls_do(&sgl->io_threads, struct sgl_io_thread, ls, t) {
    pthread_mutex_unlock(&sgl->io_lock);
    sgl_io_thread_stop(t);
    pthread_mutex_lock(&sgl->io_lock);
    sgl_io_thread_free(t, sgl);
  }
  
  pthread_mutex_unlock(&sgl->io_lock);
}

struct sgl *sgl_deinit(struct sgl *sgl) {
  stop_io_threads(sgl);
  
  sgl_task_deinit(&sgl->main_task, sgl);
  sgl_scope_deinit(&sgl->main_scope, sgl);
  
  sgl_ls_do(&sgl->ops, struct sgl_op, ls, o) {
    sgl_free(&sgl->op_pool, sgl_op_deinit(o, sgl));
  }
  
  sgl_ls_do(&sgl->errors, struct sgl_error, ls, e) { sgl_error_free(e, sgl); }
  sgl_ls_do(&sgl->libs.root, struct sgl_lib, ls, l) { sgl_lib_deinit1(l, sgl); }
  sgl_ls_do(&sgl->libs.root, struct sgl_lib, ls, l) { sgl_lib_deinit2(l, sgl); }
  
  sgl_ls_do(&sgl->syms.root, struct sgl_sym, ls, s) {
    sgl_free(&sgl->sym_pool, sgl_sym_deinit(s));
  }

  sgl_pool_deinit(&sgl->call_pool);
  sgl_pool_deinit(&sgl->error_pool);
  sgl_pool_deinit(&sgl->field_pool);
  sgl_pool_deinit(&sgl->fimp_pool);
  sgl_pool_deinit(&sgl->form_pool);
  sgl_pool_deinit(&sgl->io_pool);
  sgl_pool_deinit(&sgl->io_thread_pool);
  sgl_pool_deinit(&sgl->lib_pool);
  sgl_pool_deinit(&sgl->macro_pool);
  sgl_pool_deinit(&sgl->op_pool);
  sgl_pool_deinit(&sgl->ref_type_pool);
  sgl_pool_deinit(&sgl->scope_pool);
  sgl_pool_deinit(&sgl->struct_type_pool);
  sgl_pool_deinit(&sgl->sym_pool);
  sgl_pool_deinit(&sgl->task_pool);
  sgl_pool_deinit(&sgl->trace_pool);
  sgl_pool_deinit(&sgl->try_pool);
  sgl_pool_deinit(&sgl->type_pool);
  sgl_pool_deinit(&sgl->val_pool);
  sgl_pool_deinit(&sgl->var_pool);

  sgl_vec_do(&sgl->links, void *, h) { dlclose(*h); }
  sgl_vec_deinit(&sgl->links);
  
  pthread_cond_destroy(&sgl->io_done);
  pthread_mutex_destroy(&sgl->io_lock);
  return sgl;
}

struct sgl_error *sgl_error(struct sgl *sgl, struct sgl_pos *pos, char *msg) {
  struct sgl_error *e = sgl_error_init(sgl_malloc(&sgl->error_pool),
                                       pos, msg, NULL, &sgl->errors);

  if (sgl->debug) {
    struct sgl_buf buf;
    sgl_buf_init(&buf);
    sgl_error_dump(e, &buf);
    fputs(sgl_buf_cs(&buf), stderr);
    abort();
  }
    
  return e;
}

struct sgl_op *sgl_throw(struct sgl *sgl,
                         struct sgl_pos *pos,
                         char *msg,
                         struct sgl_val *val) {
  struct sgl_ls *try_found = sgl_ls_peek(&sgl->task->tries);
  
  struct sgl_error *e = sgl_error_init(sgl_malloc(&sgl->error_pool),
                                       pos, msg, val,
                                       try_found ? NULL : &sgl->errors);

  if (try_found) {
    struct sgl_try *try = sgl_baseof(struct sgl_try, ls, try_found);
    sgl_ls_delete(&try->ls);
    sgl_state_restore(&try->state, sgl);
    sgl_state_restore_calls(&try->state, sgl);
    sgl_push(sgl, sgl->Error)->as_error = e;
    struct sgl_op *end_pc = try->end_pc->next;
    sgl_try_free(try, sgl);
    return end_pc;
  }
    
  return sgl->end_pc;
}

struct sgl_sym *sgl_nsym(struct sgl *sgl, const char *id, sgl_int_t len) {
  bool ok = false;
  struct sgl_ls *i = sgl_lset_find(&sgl->syms, id, &len, &ok);
  if (ok) { return sgl_baseof(struct sgl_sym, ls, i); }
  struct sgl_sym *s = sgl_sym_init(sgl_malloc(&sgl->sym_pool), id, len);
  sgl_ls_insert(i, &s->ls);
  return s;
}

struct sgl_sym *sgl_sym(struct sgl *sgl, const char *id) {
  return sgl_nsym(sgl, id, strlen(id));
}

bool sgl_add_lib(struct sgl *sgl, struct sgl_pos *pos, struct sgl_lib *lib) {
  bool ok = false;
  struct sgl_ls *i = sgl_lset_find(&sgl->libs, lib->id, NULL, &ok);
  
  if (ok) {
    sgl_error(sgl, pos, sgl_sprintf("Dup lib: %s", lib->id->id));
    return false;
  }

  sgl_ls_insert(i, &lib->ls);
  return true;
}


struct sgl_lib *sgl_find_lib(struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_sym *id) {
  bool ok = false;
  struct sgl_ls *i = sgl_lset_find(&sgl->libs, id, NULL, &ok);
  return ok ? sgl_baseof(struct sgl_lib, ls, i) : NULL;
}

struct sgl_lib *sgl_lib(struct sgl *sgl) { return sgl->task->lib; }

void sgl_beg_lib(struct sgl *sgl, struct sgl_lib *lib) {
  struct sgl_task *t = sgl->task;
  *(struct sgl_lib **)sgl_vec_push(&t->libs) = lib;
  t->lib = lib;
}
                                                         
struct sgl_lib *sgl_end_lib(struct sgl *sgl) {
  struct sgl_task *t = sgl->task;
  assert(t->libs.len > 1);
  struct sgl_lib **l = sgl_vec_pop(&t->libs);
  t->lib = *(struct sgl_lib **)sgl_vec_peek(&t->libs);
  return *l;
}

bool _sgl_use(struct sgl *sgl,
              struct sgl_pos *pos,
              struct sgl_lib *src,
              struct sgl_sym *defs[]) {
  struct sgl_lib *dst = sgl_lib(sgl);

  if (!src->init_ok) {
    sgl_beg_lib(sgl, src);
    bool ok = src->init(src, sgl, pos);
    assert(sgl_end_lib(sgl) == src);
    if (!ok) { return false; }
    src->init_ok = true;
  }
    
  if (defs[0]) {
    for (struct sgl_sym **did = defs; *did; did++) {
      struct sgl_def *d = sgl_lib_find(src, sgl, pos, *did);

      if (!d) {
        sgl_error(sgl, pos, sgl_sprintf("Unknown def: %s", (*did)->id));
        return false;
      }
      
      if (!sgl_lib_use(dst, sgl, pos, d)) { return false; }
    }
  } else {
    sgl_vset_do(&src->defs, struct sgl_def *, d) {
      if (!sgl_lib_use(dst, sgl, pos, (*d))) { return false; }
    }
  }
  
  return true;
}

bool sgl_add_const(struct sgl *sgl,
                   struct sgl_pos *pos,
                   struct sgl_sym *id,
                   struct sgl_val *val) {
  return sgl_lib_add_const(sgl_lib(sgl), sgl, pos, id, val);
}

struct sgl_def *sgl_find_def(struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_sym *id) {
  return sgl_lib_find(sgl->task->lib, sgl, pos, id);
}

struct sgl_type *sgl_find_type(struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_sym *id) {     
  return sgl_lib_find_type(sgl_lib(sgl), sgl, pos, id);
}

struct sgl_type *sgl_get_type(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_sym *id) {     
  struct sgl_type *t = sgl_find_type(sgl, pos, id);

  if (!t) {
    sgl_error(sgl, pos, sgl_sprintf("Unknown type: %s", id->id));
    return NULL;
  }

  return t;
}

struct sgl_macro *sgl_find_macro(struct sgl *sgl,
                                 struct sgl_pos *pos,
                                 struct sgl_sym *id) {
  struct sgl_def *d = sgl_lib_find(sgl->task->lib, sgl, pos, id);
  if (!d) { return NULL; }
  assert(d->type == &SGL_DEF_MACRO);
  return sgl_baseof(struct sgl_macro, def, d);
}

struct sgl_func *sgl_find_func(struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_sym *id) {
  struct sgl_def *d = sgl_lib_find(sgl->task->lib, sgl, pos, id);
  if (!d) { return NULL; }
  assert(d->type == &SGL_DEF_FUNC);
  return sgl_baseof(struct sgl_func, def, d);
}

struct sgl_func *sgl_get_func(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_sym *id) {
  struct sgl_func *f = sgl_find_func(sgl, pos, id);

  if (!f) {
    sgl_error(sgl, pos, sgl_sprintf("Unknown func: %s", id->id));
    return NULL;
  }

  return f;
}

struct sgl_scope *sgl_scope(struct sgl *sgl) {
  struct sgl_ls *ss = &sgl->task->scopes;
    
  return (ss->next == ss)
    ? &sgl->main_scope
    : sgl_baseof(struct sgl_scope, ls, sgl_ls_peek(&sgl->task->scopes));
}

void sgl_beg_scope(struct sgl *sgl, struct sgl_scope *parent) {
  sgl_ls_push(&sgl->task->scopes,
              &sgl_scope_init(sgl_malloc(&sgl->scope_pool), parent)->ls);
}

void sgl_end_scope(struct sgl *sgl) {
  struct sgl_scope *s =
    sgl_baseof(struct sgl_scope, ls, sgl_ls_pop(&sgl->task->scopes));
  sgl_scope_deref(s, sgl);
}
                  
struct sgl_call *sgl_call(struct sgl *sgl) {
  struct sgl_ls *found = sgl_ls_peek(&sgl->task->calls);
  return found ? sgl_baseof(struct sgl_call, ls, found) : NULL;
}

struct sgl_io_thread *sgl_io_thread(struct sgl *sgl) {
  pthread_mutex_lock(&sgl->io_lock);

  if (sgl->nio_threads == SGL_MAX_IO_THREADS ||
      atomic_load(&sgl->nio_idle_threads)) {
    struct sgl_io_thread *t =
      sgl_baseof(struct sgl_io_thread, ls, sgl_ls_pop(&sgl->io_threads));
    sgl_ls_append(&sgl->io_threads, &t->ls);
    pthread_mutex_unlock(&sgl->io_lock);
    return t;
  }
  
  sgl->nio_threads++;
  struct sgl_io_thread *t = sgl_io_thread_new(sgl);
  pthread_mutex_unlock(&sgl->io_lock);
  sgl_io_thread_start(t);
  return t;
}

static struct sgl_op *yield_io(struct sgl *sgl) {
  sgl_ls_do(&sgl->io_queue, struct sgl_io, ls, io) {
    if (io->depth == -1 || io->depth == sgl->io_depth) {
      sgl_ls_delete(&io->ls);
      pthread_mutex_unlock(&sgl->io_lock);
      atomic_fetch_sub(&sgl->nio_done, 1);
      sgl_ls_append(&sgl->tasks, &io->task->ls);
      struct sgl_task *t = sgl->task = io->task;
      if (io->free) { io->free(io, sgl); }
      return t->pc;
    }
  }

  return NULL;
}

struct sgl_op *sgl_yield(struct sgl *sgl) {
  struct sgl_task *t = sgl->task;
  t->pc = sgl->pc;
  
  struct sgl_ls *nt = (t->ls.next == &sgl->tasks) ? sgl->tasks.next : t->ls.next;
  
  if (nt == &sgl->tasks || nt == &t->ls || atomic_load(&sgl->nio_done)) {
    if (nt == &sgl->tasks || (nt == &t->ls && sgl->ntasks > 1)) {
      pthread_mutex_lock(&sgl->io_lock);

      for (;;) {
        struct sgl_op *pc = yield_io(sgl);
        if (pc) { return pc; } 
        pthread_cond_wait(&sgl->io_done, &sgl->io_lock);
      }
    } else {
      pthread_mutex_lock(&sgl->io_lock);
      struct sgl_op *pc = yield_io(sgl);
      if (pc) { return pc; }
      pthread_mutex_unlock(&sgl->io_lock);
    }
  }
    
  t = sgl->task = sgl_baseof(struct sgl_task, ls, nt);
  return t->pc;
}

struct sgl_ls *sgl_reg_stack(struct sgl *sgl) { return &sgl->task->reg_stack; }

struct sgl_val *sgl_push_reg(struct sgl *sgl, struct sgl_type *type) {
  return sgl_val_new(sgl, type, &sgl->task->reg_stack);
}

struct sgl_val *sgl_peek_reg(struct sgl *sgl) {
  struct sgl_ls *v = sgl_ls_peek(&sgl->task->reg_stack);
  return v ? sgl_baseof(struct sgl_val, ls, v) : NULL;
}

struct sgl_val *sgl_pop_reg(struct sgl *sgl) {
  struct sgl_ls *v = sgl_ls_pop(&sgl->task->reg_stack);
  
  if (!v) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Reg stack is empty"));
    return NULL;
  }

  return sgl_baseof(struct sgl_val, ls, v);
}

struct sgl_ls *sgl_stack(struct sgl *sgl) { return &sgl->task->stack->root; }

struct sgl_val *sgl_push(struct sgl *sgl, struct sgl_type *type) {
  return sgl_val_new(sgl, type, sgl_stack(sgl));
}

struct sgl_val *sgl_peek(struct sgl *sgl) {
  struct sgl_ls *v = sgl_ls_peek(sgl_stack(sgl));
  return v ? sgl_baseof(struct sgl_val, ls, v) : NULL;
}

struct sgl_val *sgl_pop(struct sgl *sgl) {
  struct sgl_ls *v = sgl_ls_pop(sgl_stack(sgl));
  
  if (!v) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Stack is empty"));
    return NULL;
  }

  return sgl_baseof(struct sgl_val, ls, v);
}

void sgl_reset_stack(struct sgl *sgl) {
  struct sgl_ls *s = sgl_stack(sgl);
  sgl_ls_do(s, struct sgl_val, ls, v) { sgl_val_free(v, sgl); }
  sgl_ls_init(s);
}

struct sgl_list *sgl_beg_stack(struct sgl *sgl) {
  struct sgl_task *t = sgl->task;
  struct sgl_list *ps = t->stack, *s = t->stack = sgl_list_new(sgl);
  sgl_ls_append(&ps->ls, &s->ls);
  return s;
}

struct sgl_list *sgl_end_stack(struct sgl *sgl) {
  struct sgl_task *t = sgl->task;
  struct sgl_list *s = t->stack;
  struct sgl_ls *next = s->ls.next;
  sgl_ls_delete(&s->ls);
  if (next == &t->stacks) { next = t->stacks.prev; }
  assert(next != &t->stacks);
  t->stack = sgl_baseof(struct sgl_list, ls, next);
  return s;
}

struct sgl_list *sgl_switch_stack(struct sgl *sgl, sgl_int_t n) {
  struct sgl_task *t = sgl->task;
  struct sgl_ls *i = &t->stack->ls;

  if (n > 0) {
    for (; n && i != &t->stacks; i = i->next, n--);
  } else {
    for (; n && i != &t->stacks; i = i->prev, n++);
  }

  if (n) {
    sgl_error(sgl, sgl_pos(sgl), sgl_sprintf("Failed switching stack: %" SGL_INT, n));
    return false;
  }

  t->stack = sgl_baseof(struct sgl_list, ls, i);
  return t->stack;
}

static struct sgl_val *get_var(struct sgl_scope *scope, struct sgl_sym *id) {
  bool ok=false;
  struct sgl_ls *i = sgl_lset_find(&scope->vars, id, NULL, &ok);
  if (ok) { return sgl_baseof(struct sgl_var, ls, i)->val; }
  return scope->parent ? get_var(scope->parent, id) : NULL;
}

struct sgl_val *sgl_get_var(struct sgl *sgl, struct sgl_sym *id) {
  return get_var(sgl_scope(sgl), id);
}

bool sgl_let_var(struct sgl *sgl,
                 struct sgl_pos *pos,
                 struct sgl_sym *id,
                 struct sgl_val *val) {
  bool ok=false;
  struct sgl_scope *scope = sgl_scope(sgl);
  struct sgl_ls *i = sgl_lset_find(&scope->vars, id, NULL, &ok);
  
  if (ok) {
    sgl_error(sgl, pos, sgl_sprintf("Dup var: %s", id->id));
    return false;
  }

  struct sgl_var *v = sgl_var_new(sgl, id, val);
  sgl_ls_insert(i, &v->ls);
  return true;
}

bool sgl_compile(struct sgl *sgl, struct sgl_ls *in) {
  struct sgl_ls *i = in->next;
  
  while (i && i != in) {
    struct sgl_form *f = sgl_baseof(struct sgl_form, ls, i);
    i = f->compile(f, sgl, in);
  }
  
  return sgl->errors.next == &sgl->errors;
}

void sgl_dump_ops(struct sgl *sgl, FILE *out) {
  struct sgl_buf buf;
  sgl_buf_init(&buf);
  sgl_ls_do(&sgl->ops, struct sgl_op, ls, op) { sgl_op_dump(op, &buf); }
  fputs(sgl_buf_cs(&buf), out);
  sgl_buf_deinit(&buf);
}

bool sgl_cemit(struct sgl *sgl, struct sgl_op *pc, struct sgl_cemit *out) {
  sgl_cemit_sincl(out, "stddef.h");
  sgl_cemit_sincl(out, "stdio.h");
  sgl_cemit_line(out, NULL);

  sgl_cemit_incl(out, "snigl/form.h");
  sgl_cemit_incl(out, "snigl/ls.h");
  sgl_cemit_incl(out, "snigl/pos.h");
  sgl_cemit_incl(out, "snigl/sgl.h");
  sgl_cemit_incl(out, "snigl/val.h");
  sgl_cemit_line(out, NULL);
  
  sgl_cemit_line(out, "static struct sgl_ls forms;");
  sgl_cemit_line(out, NULL);
  
  sgl_cemit_scope(out, "static bool init(struct sgl *sgl)") {
    sgl_cemit_line(out, "static bool init_ok = false;");
    sgl_cemit_line(out, "if (init_ok) { return false; }");
    sgl_cemit_line(out, "init_ok = true;\n");
    sgl_cemit_line(out, "sgl_ls_init(&forms);");
    sgl_cemit_line(out, "struct sgl_lib *lib = sgl_lib(sgl);");
    sgl_cemit_line(out, "struct sgl_pos p = SGL_NULL_POS;");
    sgl_cemit_line(out, "struct sgl_sym *id = NULL;");
    sgl_cemit_line(out, "struct sgl_val *v = NULL;");
    sgl_cemit_line(out, NULL);
    
    for (struct sgl_op *ipc = pc;
         ipc != sgl->end_pc;
         ipc = sgl_op_cinit(ipc, sgl, out));

    if (sgl->errors.next != &sgl->errors) { return false; }
    sgl_cemit_line(out, "return sgl->errors.next == &sgl->errors;");
  }

  sgl_cemit_line(out, NULL);
  
  sgl_cemit_scope(out, "static bool eval(struct sgl *sgl)") {
    sgl_cemit_line(out, "if (!sgl_run(sgl, sgl->start_pc->next)) { return false; }");
    while (pc != sgl->end_pc) { pc = sgl_op_cemit(pc, sgl, out); }
    if (sgl->errors.next != &sgl->errors) { return false; }
    sgl_cemit_line(out, "return true;");
  }

  sgl_cemit_line(out, NULL);

  sgl_cemit_scope(out, "int main(int argc, char *argv[])") {
    sgl_cemit_line(out, "sgl_setup();");
    sgl_cemit_line(out, "struct sgl sgl;");
    sgl_cemit_line(out, "sgl_init(&sgl);");
    if (sgl->debug) { sgl_cemit_line(out, "sgl.debug = true;"); }
    sgl_cemit_line(out, NULL);
      
    sgl_cemit_scope(out, "if (!sgl_use(&sgl, &SGL_NULL_POS, sgl.abc_lib, NULL))") {
      sgl_cemit_line(out, "sgl_dump_errors(&sgl, stdout);");
      sgl_cemit_line(out, "return -1;");
    }

    sgl_cemit_line(out, NULL);

    sgl_cemit_scope(out, "if (!init(&sgl))") {
      sgl_cemit_line(out, "sgl_dump_errors(&sgl, stdout);");
      sgl_cemit_line(out, "return -1;");
    }
  
    //sgl_cemit_line(out, "sgl_dump_ops(&sgl, stdout);");
    sgl_cemit_line(out, NULL);
    
    sgl_cemit_scope(out, "if (!eval(&sgl))") {
      sgl_cemit_line(out, "sgl_dump_errors(&sgl, stdout);");
      sgl_cemit_line(out, "return -1;");
    }

    sgl_cemit_line(out, NULL);
    sgl_cemit_line(out, "sgl_deinit(&sgl);");
    sgl_cemit_line(out, "return 0;");
  }
  
  return sgl->errors.next == &sgl->errors;
}

bool sgl_load(struct sgl *sgl, struct sgl_pos *pos, const char *path) {
  FILE *f = fopen(path, "r");

  if (!f) {
    sgl_error(sgl, pos, sgl_sprintf("Failed opening file: %s", path));
    return false;
  }
  
  fseek(f, 0, SEEK_END);
  sgl_int_t len = ftell(f);
  fseek(f, 0, SEEK_SET);
  char buf[len+1];
  buf[len] = 0;
  sgl_int_t rlen = fread(buf, 1, len, f);
  fclose(f);
  
  if (rlen != len) { return false; }

  struct sgl_ls fs;
  bool ok = false;
  if (!sgl_parse(buf, sgl, sgl_ls_init(&fs))) { goto exit; }
  if (!sgl_compile(sgl, &fs)) { goto exit; }
  sgl_forms_deref(&fs, sgl);
  ok = true;
 exit:
  return ok;
}

#ifdef SGL_USE_DL

bool sgl_link(struct sgl *sgl, struct sgl_pos *pos, const char *path) {
  dlerror();
  void *h = dlopen(path, RTLD_LAZY | RTLD_GLOBAL);

  if (!h) {
    sgl_error(sgl, pos, sgl_sprintf("Failed linking: %s\n", dlerror()));
    return false;
  }

  void *found = dlsym(h, "sgl_plugin_init");
  bool ok = true;
  
  if (found) {
    bool (*init)(struct sgl *, struct sgl_pos *);
    *(void **) (&init) = found;
    ok = init(sgl, pos);
  }

  *(void **)sgl_vec_push(&sgl->links) = h;
  return ok;
}

#endif

sgl_int_t sgl_trace(struct sgl *sgl, struct sgl_op *pc) {
  struct sgl_trace *t = sgl_trace_new(sgl);
  struct sgl_op *start_pc = pc;
  bool changed = false;
  sgl_int_t nrounds = 0;

  for (pc = start_pc;
       pc != sgl->end_pc &&
         sgl->errors.next == &sgl->errors &&
         (sgl->trace == -1 || nrounds < sgl->trace);
       pc = pc->next) {
    changed |= sgl_op_trace(pc, sgl, t);
    
    if (changed && !t->len) {
      sgl_trace_clear(t, sgl);
      pc = start_pc;
      changed = false;
      nrounds++;
    }
  }

  sgl_trace_free(t, sgl);
  return (sgl->errors.next == &sgl->errors) ? nrounds : -1;
}

struct sgl_pos *sgl_pos(struct sgl *sgl) { return &sgl->pc->form->pos; }

bool sgl_run(struct sgl *sgl, struct sgl_op *pc) {
  while ((pc = pc->run((sgl->pc = pc), sgl)) != sgl->end_pc);
  return sgl->errors.next == &sgl->errors;
}

bool sgl_run_task(struct sgl *sgl,
                  struct sgl_task *task,
                  struct sgl_op *pc, struct sgl_op *end_pc) {
  while ((sgl->task != task || pc != end_pc) && pc != sgl->end_pc) {
    pc = pc->run((sgl->pc = pc), sgl);
  }

  return sgl->errors.next == &sgl->errors;
}

bool sgl_run_io(struct sgl *sgl, struct sgl_io *io) {
  sgl->io_depth++;
  
  struct sgl_task *task = sgl->task;
  struct sgl_op *pc = sgl_io_yield(io, sgl, sgl_io_thread(sgl))->next;

  while (sgl->task != task && pc != sgl->end_pc) {
    pc = pc->run((sgl->pc = pc), sgl);
  }

  sgl->io_depth--;
  return sgl->errors.next == &sgl->errors;
}

void sgl_dump_errors(struct sgl *sgl, FILE *out) {
  struct sgl_buf buf;
  sgl_buf_init(&buf);
  
  sgl_ls_do(&sgl->errors, struct sgl_error, ls, e) {
    if (buf.len) { sgl_buf_putc(&buf, '\n'); }
    sgl_error_dump(e, &buf);
    sgl_buf_putc(&buf, '\n');
    fputs(sgl_buf_cs(&buf), out);
    buf.len = 0;
    sgl_error_deref(e, sgl);
  }

  sgl_buf_deinit(&buf);
}
