#include <assert.h>

#include "snigl/buf.h"
#include "snigl/file.h"
#include "snigl/io.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/str.h"
#include "snigl/util.h"

struct sgl_file *sgl_file_new(struct sgl *sgl, FILE *imp) {
  return sgl_file_init(sgl_file_malloc(sgl), imp);
}

void sgl_file_deref(struct sgl_file *f, struct sgl *sgl) {
  assert(f->nrefs > 0);
  
  if (!--f->nrefs) {
    fclose(f->imp);
    sgl_file_free(f, sgl);
  }
}

struct sgl_file *sgl_file_malloc(struct sgl *sgl) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->File);
  return sgl_malloc(&rt->pool);
}

void sgl_file_free(struct sgl_file *f, struct sgl *sgl) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->File);
  sgl_free(&rt->pool, f); 
}

struct sgl_file *sgl_file_init(struct sgl_file *f, FILE *imp) {
  f->imp = imp;
  f->nrefs = 1;
  return f;
}

static char *lines_next_imp(struct sgl *sgl,
                            FILE *in,
                            struct sgl_buf *buf,
                            struct sgl_str *out,
                            bool *ok) {  
  char c = 0;
  out->len = 0;
  char *os = out->as_short;

  while ((c = fgetc(in)) && c != EOF && c != '\n') {
    out->len++;
    
    if (out->len < SGL_MAX_STR_LEN) {
      os[out->len-1] = c;
      os[out->len] = 0;
    } else {
      if (out->len == SGL_MAX_STR_LEN) {
        sgl_buf_grow(buf, SGL_MAX_STR_LEN * 2);
        sgl_buf_nputcs(buf, os, SGL_MAX_STR_LEN);
      }

      sgl_buf_putc(buf, c);
    }
  }
  
  if (out->len || c == '\n') {
    if (buf->len) {
      sgl_str_init(out, sgl_buf_cs(buf), buf->len);
      buf->len = 0;
    } else {
      out->cap = out->len;
    }
        
    *ok = true;
    return NULL;
  }
  
  return NULL;
}

static char *lines_next_io(struct sgl_io *io,
                          struct sgl *sgl,
                          struct sgl_io_thread *thread) {
  struct sgl_file_line_iter *i = io->args[0];
  return lines_next_imp(sgl, i->in->imp, &i->buf, io->args[1], io->args[2]);
}

static struct sgl_val *lines_next_val(struct sgl_iter *i,
                                      struct sgl *sgl,
                                      struct sgl_pos *pos,
                                      struct sgl_ls *root) {
  struct sgl_file_line_iter *fi = sgl_baseof(struct sgl_file_line_iter, iter, i);
  struct sgl_str *out = sgl_str_new(sgl, NULL, 0);
  bool ok = false;
  
  if (sgl->ntasks == 1) {
    lines_next_imp(sgl, fi->in->imp, &fi->buf, out, &ok);
  } else {
    struct sgl_io *io = sgl_io_new(sgl, sgl->io_depth + 1, fi, out, &ok);
    io->run = lines_next_io;
    if (!sgl_run_io(sgl, io)) { ok = false; }
    sgl_io_free(io, sgl);
  }

  if (!ok) {
    sgl_str_free(out, sgl);
    return NULL;
  }
  
  struct sgl_val *v = sgl_val_new(sgl, sgl->Str, root);
  v->as_str = out;
  return v;
}

static bool lines_skip_vals(struct sgl_iter *i,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            sgl_int_t nvals) {
  return false;
}

static void lines_free(struct sgl_iter *i, struct sgl *sgl) {
  struct sgl_file_line_iter *fi = sgl_baseof(struct sgl_file_line_iter, iter, i);
  sgl_file_deref(fi->in, sgl);
  sgl_buf_deinit(&fi->buf);
  
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->FileLineIter);
  sgl_free(&rt->pool, fi);
}

struct sgl_file_line_iter *sgl_file_line_iter_new(struct sgl *sgl,
                                                  struct sgl_file *in) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->FileLineIter);
  struct sgl_file_line_iter *i = sgl_malloc(&rt->pool);
  sgl_iter_init(&i->iter);
  i->iter.next_val = lines_next_val;
  i->iter.skip_vals = lines_skip_vals;
  i->iter.free = lines_free;
  i->in = in;
  in->nrefs++;
  sgl_buf_init(&i->buf);
  return i;
}

