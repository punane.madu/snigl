#ifndef SNIGL_REPL_H
#define SNIGL_REPL_H

#include <stdio.h>

struct sgl;
struct sgl_ls;

void sgl_repl(struct sgl *sgl, FILE *in, FILE *out);

#endif
