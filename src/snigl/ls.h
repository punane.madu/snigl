#ifndef SNIGL_LS_H
#define SNIGL_LS_H

#include <stdbool.h>

#include "snigl/cmp.h"

#define _sgl_ls_do(mroot, mtype, mfield, mvar, mls, mnext)      \
  struct sgl_ls                                                 \
  *mls = mroot->next,                                           \
    *mnext = (mroot->next == mroot) ? NULL : mroot->next->next; \
                                                                \
  for (mtype *mvar = sgl_baseof(mtype, mfield, mls);            \
       mls != mroot;                                            \
       mls = mnext,                                             \
         mnext = mls->next,                                     \
         mvar = sgl_baseof(mtype, mfield, mls))                 \

#define sgl_ls_do(mroot, mtype, mfield, mvar)   \
  _sgl_ls_do((mroot), mtype, mfield, mvar,      \
             SGL_CUID(mls), SGL_CUID(mnext))    \

#define sgl_ls_push sgl_ls_insert

struct sgl_ls {
  struct sgl_ls *prev, *next;
};

struct sgl_ls *sgl_ls_init(struct sgl_ls *ls);
bool sgl_ls_null(struct sgl_ls *ls);

void sgl_ls_append(struct sgl_ls *ls, struct sgl_ls *next);
void sgl_ls_insert(struct sgl_ls *ls, struct sgl_ls *prev);
void sgl_ls_delete(struct sgl_ls *ls);

struct sgl_ls *sgl_ls_peek(struct sgl_ls *root);
struct sgl_ls *sgl_ls_pop(struct sgl_ls *root);

#endif
