#include "snigl/buf.h"
#include "snigl/func.h"
#include "snigl/lib.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/types/func.h"
#include "snigl/val.h"

static struct sgl_op *call_val(struct sgl_val *val,
                               struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_op *return_pc,
                               bool now) {
  struct sgl_func *fn = val->as_func;
  struct sgl_fimp *fi = sgl_func_dispatch(fn, sgl, sgl_stack(sgl));

  if (!fi) {
    sgl_error(sgl, pos, sgl_sprintf("Func not applicable: %s", fn->def.id->id));
    return sgl->end_pc;
  }

  struct sgl_imp *i = &fi->imp;
  if (!fi->cimp && i->end_pc == i->start_pc) { return return_pc->next; }

  if (now) {
    struct sgl_op *pc = sgl_fimp_call(fi, sgl, pos, return_pc);
    if (fi->cimp) { return pc; }
    struct sgl_op *end_pc = i->end_pc->next;
    return sgl_run_task(sgl, sgl->task, pc, end_pc) ? return_pc->next : sgl->end_pc;
  }

  return sgl_fimp_call(fi, sgl, pos, return_pc);
}

static enum sgl_cmp cmp_val(struct sgl_val *val, struct sgl_val *rhs) {
  return sgl_ptr_cmp(val->as_func, rhs->as_func);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_buf_printf(out, "(Func %s)", val->as_func->def.id->id);
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  out->as_func = val->as_func;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_func == rhs->as_func;
}

struct sgl_type *sgl_func_type_new(struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_lib *lib,
                                    struct sgl_sym *id,
                                    struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_ref_type_new(sgl, pos, lib, id, parents,
                                        sizeof(struct sgl_func),
                                        offsetof(struct sgl_func, ls));

  t->call_val = call_val;
  t->cmp_val = cmp_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->is_val = is_val;
  return t;
}
