#include "snigl/cemit.h"
#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/sym.h"
#include "snigl/val.h"

static bool bool_val(struct sgl_val *val) { return val->as_sym->id[0]; }

static bool cemit_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      const char *id,
                      const char *root,
                      struct sgl_cemit *out) {
  sgl_cemit_line(out,
                 "%s = sgl_val_new(sgl, sgl->Sym, %s);",
                 id, root ? root : "NULL");
  
  sgl_cemit_line(out, "%s->as_sym = sgl_sym(sgl, \"%s\");", id, val->as_sym->id);
  return true;
}

static enum sgl_cmp cmp_val(struct sgl_val *val, struct sgl_val *rhs) {
  return sgl_sym_cmp(val->as_sym, rhs->as_sym);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_buf_printf(out, "'%s", val->as_sym->id);
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  out->as_sym = val->as_sym;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_sym == rhs->as_sym;
}

static void print_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_buf *out) {
  sgl_buf_putcs(out, val->as_sym->id);
}

struct sgl_type *sgl_sym_type_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_lib *lib,
                                  struct sgl_sym *id,
                                  struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, parents);
  t->bool_val = bool_val;
  t->cemit_val = cemit_val;
  t->cmp_val = cmp_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->is_val = is_val;
  t->print_val = print_val;
  return t;
}
