#include <stdio.h>

#include "snigl/buf.h"
#include "snigl/file.h"
#include "snigl/lib.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/types/file.h"
#include "snigl/val.h"

static enum sgl_cmp cmp_val(struct sgl_val *val, struct sgl_val *rhs) {
  return sgl_ptr_cmp(val->as_file, rhs->as_file);
}

static void deinit_val(struct sgl_val *val, struct sgl *sgl) {
  sgl_file_deref(val->as_file, sgl);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_buf_printf(out, "(File %p)", (void *)val->as_file->imp);
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  struct sgl_file *f = out->as_file = val->as_file;
  f->nrefs++;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_file == rhs->as_file;
}

struct sgl_type *sgl_file_type_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_lib *lib,
                                   struct sgl_sym *id,
                                   struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_ref_type_new(sgl, pos, lib, id, parents,
                                        sizeof(struct sgl_file),
                                        offsetof(struct sgl_file, ls));
  t->cmp_val = cmp_val;
  t->deinit_val = deinit_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->is_val = is_val;
  return t;
}
