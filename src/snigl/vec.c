#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "snigl/config.h"
#include "snigl/vec.h"
#include "snigl/util.h"

struct sgl_vec *sgl_vec_init(struct sgl_vec *v, sgl_int_t val_size) {
  v->val_size = val_size;
  v->len = v->cap = 0;
  v->vals = NULL;
  return v;
}

struct sgl_vec *sgl_vec_deinit(struct sgl_vec *v) {
  if (v->vals) { free(v->vals); }
  return v;
}

void sgl_vec_grow(struct sgl_vec *v, sgl_int_t len) {
  if (v->cap < len) {
    if (v->cap) {
      while (v->cap < len) { v->cap *= 2; }
    } else {
      v->cap = sgl_max(len, SGL_MIN_VEC_LEN);
    }
    
    v->vals = realloc(v->vals, v->cap*v->val_size);
  }
}

void *sgl_vec_beg(struct sgl_vec *v) { return v->vals; }                    

void *sgl_vec_get(struct sgl_vec *v, sgl_int_t i) {
  return v->vals + i * v->val_size;
}

void *sgl_vec_push(struct sgl_vec *v) {
  sgl_vec_grow(v, ++v->len);
  return sgl_vec_get(v, v->len-1);
}

void *sgl_vec_peek(struct sgl_vec *v) {
  return v->len ? sgl_vec_get(v, v->len-1) : NULL;
}

void *sgl_vec_pop(struct sgl_vec *v) { return sgl_vec_get(v, --v->len); }

void *sgl_vec_insert(struct sgl_vec *v, sgl_int_t i) {
 assert(i <= v->len);
 if (v->cap == v->len) { sgl_vec_grow(v, v->cap + 1); }
 void *p = sgl_vec_get(v, i);
 if (i < v->len) { memmove(sgl_vec_get(v, i + 1), p, (v->len-i) * v->val_size); }
 v->len++;
 return p;
}

void sgl_vec_delete(struct sgl_vec *v, sgl_int_t i) {
  assert(i < v->len);
  
  if (i < v->len-1) {
    memmove(sgl_vec_get(v, i),
            sgl_vec_get(v, i + 1),
            (v->len - i - 1) * v->val_size);
  }

  v->len--;
}
