#ifndef SNIGL_TRY_H
#define SNIGL_TRY_H

#include "snigl/ls.h"
#include "snigl/state.h"

struct sgl;

struct sgl_try {
  struct sgl_ls ls;
  struct sgl_state state;
  struct sgl_op *end_pc;
};

struct sgl_try *sgl_try_new(struct sgl *sgl, struct sgl_op *end_pc);
void sgl_try_free(struct sgl_try *try, struct sgl *sgl);

#endif
