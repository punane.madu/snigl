#include <stddef.h>

#include "snigl/vset.h"

struct sgl_vset *sgl_vset_init(struct sgl_vset *s,
                               sgl_int_t val_size,
                               sgl_vec_cmp_t cmp,
                               sgl_int_t key_offs) {
  s->cmp = cmp;
  s->key_offs = key_offs;
  s->key_fn = NULL;
  sgl_vec_init(&s->vals, val_size);
  return s;
}

struct sgl_vset *sgl_vset_deinit(struct sgl_vset *s) {
  sgl_vec_deinit(&s->vals);
  return s;
}

sgl_int_t sgl_vset_find(struct sgl_vset *s, const void *key, void *data, bool *ok) {
  struct sgl_vec *vs = &s->vals;
  sgl_int_t min = 0, max = vs->len;

  while (min < max) {
    sgl_int_t i = (min + max) / 2;
    void *v = sgl_vec_get(vs, i);
    void *k = (unsigned char *)v + s->key_offs;
    
    switch (s->cmp(s->key_fn ? s->key_fn(k) : k, key, data)) {
    case SGL_LT:
      min = i+1;
      continue;
    case SGL_GT:
      max = i;
      continue;
    default:
      if (ok) { *ok = true; }
      return i;
    }
  }

  return min;
}
