#include <stddef.h>

#include "snigl/form.h"
#include "snigl/imp.h"

struct sgl_imp *sgl_imp_init(struct sgl_imp *imp, struct sgl_form *form) {
  imp->form = form;
  form->nrefs++;
  imp->start_pc = imp->end_pc = NULL;
  imp->recalls = false;
  return imp;
}

struct sgl_imp *sgl_imp_deinit(struct sgl_imp *imp, struct sgl *sgl) {
  sgl_form_deref(imp->form, sgl);
  return imp;
}
