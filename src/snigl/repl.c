#include "snigl/buf.h"
#include "snigl/error.h"
#include "snigl/repl.h"
#include "snigl/parse.h"
#include "snigl/sgl.h"
#include "snigl/stack.h"
#include "snigl/val.h"

void sgl_repl(struct sgl *sgl, FILE *in, FILE *out) {
  printf("Snigl v%" SGL_INT ".%" SGL_INT ".%" SGL_INT "\n\n",
         SGL_VERSION[0], SGL_VERSION[1], SGL_VERSION[2]);

  printf("Press Return in empty row to eval.\n\n  ");
  
  struct sgl_buf buf;
  sgl_buf_init(&buf);
  char c = 0, pc = 0;

  while ((c = fgetc(in))) {
    if (c == '\n' && pc == '\n') {
      if (buf.len) {
        struct sgl_op *pc = sgl->end_pc->prev;
        struct sgl_ls fs;
      
        if (sgl_parse(sgl_buf_cs(&buf), sgl, sgl_ls_init(&fs)) &&
            sgl_compile(sgl, &fs)) {
          struct sgl_op *start_pc = pc->next;
          
          if (sgl_run(sgl, start_pc)) {
            sgl_stack_dump(sgl_stack(sgl), out);
            fputc('\n', out);
          }
        }
        
        sgl_dump_errors(sgl, out);
        buf.len = 0;
      } else {
        sgl_reset_stack(sgl);
        fputs("[]\n", out);
      }
      
      fputs("\n  ", out);
      pc = 0;
    } else {
      if (c == '\n') { fputs("  ", out); }
      sgl_buf_putc(&buf, c);
    }

    pc = c;
  }

  sgl_buf_deinit(&buf);
}
