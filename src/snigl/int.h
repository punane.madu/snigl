#ifndef SNIGL_INT_H
#define SNIGL_INT_H

#include <inttypes.h>
#include <stdint.h>

#include "snigl/cmp.h"

#define SGL_INT PRIdFAST64
#define SGL_INT_MAX INT_FAST64_MAX
#define SGL_INT_MAX_LEN 22

typedef int_fast64_t sgl_int_t;

enum sgl_cmp sgl_int_cmp(sgl_int_t lhs, sgl_int_t rhs);
char* sgl_int_str(sgl_int_t val, char* out, int base);

#endif
