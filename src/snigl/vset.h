#ifndef SNIGL_VSET_H
#define SNIGL_VSET_H
  
#include <stdbool.h>

#include "snigl/cmp.h"
#include "snigl/vec.h"

#define sgl_vset_do(mset, mtype, mvar)          \
  sgl_vec_do(&(mset)->vals, mtype, mvar)        \

typedef enum sgl_cmp (*sgl_vec_cmp_t)(const void *lhs, const void *rhs, void *data);

struct sgl_vset {
  sgl_vec_cmp_t cmp;
  sgl_int_t key_offs;
  const void *(*key_fn)(void *val);
  struct sgl_vec vals;
};

struct sgl_vset *sgl_vset_init(struct sgl_vset *s,
                               sgl_int_t val_size,
                               sgl_vec_cmp_t cmp,
                               sgl_int_t key_offs);

struct sgl_vset *sgl_vset_deinit(struct sgl_vset *s);
sgl_int_t sgl_vset_find(struct sgl_vset *s, const void *key, void *data, bool *ok);

#endif
