#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "snigl/buf.h"
#include "snigl/cemit.h"
#include "snigl/list.h"
#include "snigl/repl.h"
#include "snigl/sgl.h"
#include "snigl/str.h"
#include "snigl/timer.h"

enum sgl_mode { SGL_MODE_COMPILE,
                SGL_MODE_DEFAULT,
                SGL_MODE_REPL,
                SGL_MODE_RUN };
  
int main(int argc, const char *argv[]) {
  sgl_setup();
  struct sgl sgl;
  sgl_init(&sgl);
  
  if (!sgl_use(&sgl, &SGL_NULL_POS, sgl.abc_lib, NULL)) {
    sgl_dump_errors(&sgl, stderr);
    return -1;
  }
          
  enum sgl_mode mode = SGL_MODE_DEFAULT;
  if (argc == 1) { goto exit; }
  
  sgl_int_t i = 1;
  const char *m = argv[i];
  
  if (strcmp(m, "compile") == 0) {
    mode = SGL_MODE_COMPILE;
    i++;
  } else if (strcmp(m, "repl") == 0) {
    mode = SGL_MODE_REPL;
    i++;
  } else if (strcmp(m, "run") == 0) {
    mode = SGL_MODE_RUN;
    i++;
  }

  if (mode == SGL_MODE_DEFAULT) { mode = SGL_MODE_RUN; } 
  bool cemit = false;
  
  while (*argv[i] == '-') {
    const char *a = argv[i++];
    
    if (strcmp(a, "--trace") == 0) {
      sgl.trace = strtoimax(argv[i++], NULL, 10);
    } else if (strcmp(a, "--cemit") == 0) {
      cemit = true;
    } else {
      printf("Error: Invalid flag: %s\n", a);
      return -1;
    }
  }
  
  if (i == argc) {
    puts("Error: Missing file");
    return -1;
  }

  struct sgl_val *v = sgl_val_new(&sgl, sgl.List, NULL);
  v->as_list = sgl_list_new(&sgl);
  sgl_add_const(&sgl, &SGL_NULL_POS, sgl_sym(&sgl, "argv"), v);

  for (sgl_int_t j = i+1; j < argc; j++) {
    const char *a = argv[j];
    
    sgl_val_new(&sgl, sgl.Str, &v->as_list->root)->as_str =
      sgl_str_new(&sgl, a, strlen(a));
  }

  if (!sgl_load(&sgl, &SGL_NULL_POS, argv[i])) {
    sgl_dump_errors(&sgl, stderr);
    return -1;
  }

  sgl_int_t trace_rounds = 0, trace_ms = 0;
  
  if (sgl.trace) {
    struct sgl_timer t;
    sgl_timer_init(&t);
    trace_rounds = sgl_trace(&sgl, sgl.start_pc->next);
    trace_ms = sgl_timer_ms(&t);

    if (trace_rounds == -1) {
      sgl_dump_errors(&sgl, stderr);
      return -1;
    }
  }
  
  if (mode == SGL_MODE_RUN) {
    if (!sgl_run(&sgl, sgl.start_pc->next)) { sgl_dump_errors(&sgl, stderr); }
  } else if (mode == SGL_MODE_COMPILE) {
    if (cemit) {
      struct sgl_cemit out;
      sgl_cemit_init(&out);
      sgl_cemit(&sgl, sgl.start_pc->next, &out);
      fwrite(sgl_buf_cs(&out.buf), 1, out.buf.len, stdout);
      sgl_cemit_deinit(&out);    
      sgl_dump_errors(&sgl, stderr);
    } else {
      sgl_dump_ops(&sgl, stdout);
      sgl_int_t nops = 0, nfimp_call = 0, ndispatch = 0;
      
      sgl_ls_do(&sgl.ops, struct sgl_op, ls, op) {
        if (op->type == &SGL_NOP) {
          nops++;
        } else if (op->type == &SGL_OP_FIMP_CALL) {
          nfimp_call++;
        } else if (op->type == &SGL_OP_DISPATCH) {
          ndispatch++;
        }
      }
    
      nops -= 2;
      
      if (sgl.trace) {
        printf("Traced %" SGL_INT " rounds in %" SGL_INT "ms\n\n",
               trace_rounds, trace_ms);
        
        printf("nop: %" SGL_INT " (%" SGL_INT "%%)\n",
               nops, (sgl_int_t)((nops / (double)sgl_max(1, sgl.end_pc->pc)) * 100));
      }
    
      printf("fimp-call/dispatch: %" SGL_INT " / %" SGL_INT " (%" SGL_INT "%%)\n\n",
             nfimp_call, ndispatch,
             (sgl_int_t)((nfimp_call /
                          (double)sgl_max(1, (nfimp_call + ndispatch))) * 100));
    }
  }
 exit:
  if (mode == SGL_MODE_DEFAULT || mode == SGL_MODE_REPL) {
    sgl_repl(&sgl, stdin, stdout);
  }
  
  sgl_deinit(&sgl);
  return 0;
}
