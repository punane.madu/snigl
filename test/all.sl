debug

1 2 + 3 test=
1, 2 + 3 test=
(1 2) + 3 test=
{1 2} + 3 test=

1 3 min 1 test=
3 1 min 1 test=
1 3 max 3 test=
3 1 max 3 test=

.5 .50 test=
.5 .49 > test

.25 .5 + .7 test=
.25 .50 + .75 test=
.25 .5 - -.3 test=
.25 .50 - -.25 test=

.50 .5 * .250 test=
1.00 4.0 / .2 test=
.500 .125 / 4. test=
.5 4 * int 2 test=
42 2 fix .25 + 42.25 test=

1 2 &+ call 3 test=
&(42) call 42 test=
&{42} call 42 test=

'foo type Sym test=

42 Int|Sym type-sub? test
'a Int|Sym type-sub? test
nil Int|Sym type-sub? not test
nil Int|Nil type-sub? test
Int|Fix Num sub? test

type: IntList List
func: sum (IntList) (0 swap for: +)
[1 2 3] int-list sum 6 test=

1 2 dup + 4 test=
1 2 3 drop + 3 test=

0 7 for: + 21 test=
0 3 times: ++ 3 test=
0 while: (++ dup 42 <) 42 test=

t if: 1 2 1 test=
f if: 1 2 2 test=

0 if: 1 2 2 test=
1 if: 2 3 2 test=

{1 2 3 let: ('a 'b 'c) @a @b + @c -} 0 test=
{42 let: 'foo &{@foo}} call 42 test=
{2 let: ('foo :: 1 'bar 'baz :: 3) [@foo @bar @baz]} [1 2 3] test=

func: thirtyfive () 35
7 thirtyfive + 42 test=

func: add-thirtyfive (Int) (35 +)
7 add-thirtyfive 42 test=

func: my-add (Int Int) +
7 35 my-add 42 test=

func: is-42? (42) t
func: is-42? (T) f
41 is-42? not test
42 is-42? test

func: is-3? (1 2 +) t
func: is-3? (3 type) f
1 is-3? not test
3 is-3? test

func: fib-rec (0) _
func: fib-rec (1) _
func: fib-rec (Int) (-- dup fib-rec swap -- fib-rec +)
20 fib-rec 6765 test=

func: fib-tail (0 A A) (rotr drop2)
func: fib-tail (1 A A) (rotl drop2)
func: fib-tail (A A A) (rotr -- rotl dup rotl + recall: fib-tail)
20 0 1 fib-tail 6765 test=

func: iff (A A A) (drop2 call)
func: iff (A A f) (rotr drop2 call)
func: if (A A A) (rotr bool recall: iff)
t 1 2 if 1 test=
f 1 &(2) if 2 test=

{42 let: 'foo func: closure () {@foo}}
closure 42 test=

{&let: 'bar 42 swap call @bar} 42 test=
{42 let: 'y &{or: @y}} f swap call 42 test=

{
  &(35 7 +) let: 'y
  &{or:, @y call}
} f swap call 42 test=

{
  &(35 @z +) let: 'y
  &{7 let: 'z or:, @y call}
} f swap call 42 test=

try: _ nil test=
try: (42 throw) catch 42 test=

[1 2 3] [1 2 2] test!=
[1 2 3] [1 2 3] test=

[1 2] [1 2] < not test
[1 2] [1 2] <= test
[1 2 3] [1 2 2] > test

1::2 first 1 test=
'foo 'bar :: _ last 'bar test=
42::"foo" clone test=
42 :: #a 42 :: #b < test
1::2.. + 3 test=
[1::'foo..] len 2 test=

[] peek nil test=
[] dup 42 push pop 42 test=

[1 2] dup pop swap pop + 3 test=
[1 2] dup peek swap peek + 4 test=
[1 2 3] clone pop! len 3 test=
[1 2 3] dup list pop! len 3 test=
7 list len 7 test=
[1 2 3] dup iter dup next! dup next! dup delete! 42 insert [1 42 3] test=
41 ['foo %(++) 'bar] ['foo 42 'bar] test=
42 ['foo [%_ 'bar %%_].. 'baz] ['foo 'bar 42 'baz] test=

\n type Char test=
#a type Char test=

"foo" len 3 test=
"foo" dup pop! len 2 test=
"foobar" dup pop! dup last #a test=
["foo" 42 'bar] #, join "foo,42,bar" test=
"foo" list dup "bar" push str "foobar" test=
"42abc" iter dup next! dup int 2 test= next #a test= 
"abc" iter dup next! str "bc" test=

"abcabcabc" &(int ++ char) map str "bcdbcdbcd" test=
"abcabcabc" &(#b !=) filter str "acacac" test=
"abcabcabc" &(dup #b = if: (drop nil) _) map str "acacac" test=

["abc"..] [#a #b #c] test=

{'bar 42 let: 'foo "foo %@foo %_ baz"} "foo 42 bar baz" test=

(pre:, 1 2 +) 3 test=

struct: Basic _ ('id Str) {
  Basic new dup put: ('.id :: "foo") let: 'b1
  @b1 call @b1 test==

  Basic new dup put: ('.id :: "foo") let: 'b2
  @b1 @b2 test=

  @b2 put: ('.id :: "bar")
  @b1 @b2 test!=
  @b1 @b2 > test

  @b2 clone .id pop!
  @b2 .id "bar" test=
}

struct: Custom _ ('id Str) {
  Custom new dup put: ('.id :: "foo") let: 'c1
  func: call (Custom) 42
  @c1 call 42 test=

  Custom new dup put: ('.id :: "bar") let: 'c2

  func: = (Custom Custom) 42
  @c1 @c2 = 42 test=

  func: clone (Custom) 42
  @c1 clone 42 test=
}

struct: Foobar Seq _
func: iter Foobar (drop "foobar" iter)
Foobar new #, join "f,o,o,b,a,r" test=

nil say